<?php

// ***********************************************************
// This method can be used to ADD record 
/******************************************************/ 
if(!function_exists('addRecord')){
	function addRecord($tableName, $postedDataArr=array(), $creationInfo=true, $recordStatus=true){
		$homzz =  & get_instance();
            if($recordStatus == 'true'){
                $postedDataArr['status'] = 1;
            }else{
                $postedDataArr['status'] = $recordStatus;
            }
        
        if($creationInfo == 'true'){
        	$user_id = getSessionData('user_id');
            $creationInfoArr = creationInfo($user_id);
            $postedDataArr = array_merge($postedDataArr, $creationInfoArr);
        }
		//pr($postedDataArr); exit;
        $homzz->db->insert($tableName, $postedDataArr);
			
        return ($homzz->db->insert_id() > 0) ? true : false;
	}
}

// ***********************************************************
// This method can be used to UPDATE record 
/******************************************************/ 
if(!function_exists('updateRecord')){
	function updateRecord($tableName, $conditionDataArr=array(), $postedDataArr=array(), $modificationInfo=true){
		$homzz =  & get_instance();
        foreach($conditionDataArr as $key => $value){
		    $homzz->db->where($key, $value);		
        }

        if($modificationInfo == 'true'){
        	$user_id = getSessionData('user_id');
            $modificationInfoArr = modificationInfo($user_id);
            $postedDataArr = array_merge($postedDataArr, $modificationInfoArr);
        }

        $homzz->db->update($tableName, $postedDataArr);		
        return ($homzz->db->trans_status() > 0) ? true : false;	
	}
}

// ***********************************************************
// This method can be used to DELETE the record
/******************************************************/ 
if(!function_exists('deleteRecord')){
	function deleteRecord($tableName, $conditionDataArr=array()){
		$homzz =  & get_instance();
		foreach($conditionDataArr as $key => $value){
		    $homzz->db->where($key, $value);		
        }
		$homzz->db->delete($tableName);
		return ($homzz->db->affected_rows() > 0) ? true: false;
	}
}

// ***********************************************************
// This method can be used to GET the record
/******************************************************/ 
if(!function_exists('getRecord')){
	function getRecord($tableName, $conditionDataArr=array(), $orderDataArr=array()){
		$homzz =  & get_instance();
		
        if(!is_array($conditionDataArr)){
            $conditionDataArr=array();
        }

        if(!is_array($orderDataArr)){
            $orderDataArr=array();
        }

		foreach($conditionDataArr as $key => $value){
		    $homzz->db->where($key, $value);		
        }
				
        foreach($orderDataArr as $k => $v){
		    $homzz->db->order_by($k, $v);
		}    
        return $homzz->db->get($tableName)->result(); 
	}
}


// ***********************************************************
// This method can be used to GET the record by ID (Primary Key)
/******************************************************/ 
if(!function_exists('getRecordById')){
	function getRecordById($tableName, $conditionDataArr=array()){
		$homzz =  & get_instance();
		foreach($conditionDataArr as $key => $value){
		    $homzz->db->where($key, $value);		
        }
		return $homzz->db->get($tableName)->row();
	}
}


// ***********************************************************
// This method can be used to Activate/De-Activate Record 
/******************************************************/ 
if(!function_exists('updateRecordStatus')){
	function updateRecordStatus($tableName, $postedDataArr=array()){
		$homzz =  & get_instance();
        $record_id          = $postedDataArr['record_id'];
        $current_status     = $postedDataArr['status'];
        if($current_status == 1) {
            $homzz->db->where('id ', $record_id);
            $homzz->db->update($tableName, array('status'=>0));
            return 'inactive';
        } else {
            $homzz->db->where('id ', $record_id);
            $homzz->db->update($tableName, array('status'=>1));
            return 'active';
        }
	}
}


if(!function_exists('createUserIpDetail')){
	function createUserIpDetail($user_id=false){
		if($user_id){
			$mahindra =  & get_instance();
			$ip_address = $mahindra->input->ip_address();


			if ($mahindra->agent->is_browser())
			{
				$agent = $mahindra->agent->browser().' '.$mahindra->agent->version();
			}
			elseif ($mahindra->agent->is_robot())
			{
				$agent = $mahindra->agent->robot();
			}
			elseif ($mahindra->agent->is_mobile())
			{
				$agent = $mahindra->agent->mobile();
			}
			else
			{
				$agent = '';
			}

			$create_user = array();
			$create_user['created_date'] = date('Y-m-d H:i:s');
			$create_user['created_by'] = $user_id;
			$create_user['create_browser'] = $agent;
			$create_user['created_ip'] = $ip_address;
			return $create_user;
		}
	}
}

if(!function_exists('modifyUserIpDetail')){
	function modifyUserIpDetail($user_id=false){
		if($user_id){
			$mahindra =  & get_instance();
			$ip_address = $mahindra->input->ip_address();
			if ($mahindra->agent->is_browser())
			{
				$agent = $mahindra->agent->browser().' '.$mahindra->agent->version();
			}
			elseif ($mahindra->agent->is_robot())
			{
				$agent = $mahindra->agent->robot();
			}
			elseif ($mahindra->agent->is_mobile())
			{
				$agent = $mahindra->agent->mobile();
			}
			else
			{
				$agent = '';
			}

			$modifyed_user = array();
			$modifyed_user['modified_date'] = date('Y-m-d H:i:s');
			$modifyed_user['modified_by'] = $user_id;
			$modifyed_user['modified_browser'] = $agent;
			$modifyed_user['modified_ip'] = $ip_address;
			return $modifyed_user;
		}
	}
}

