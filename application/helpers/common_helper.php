<?php
/***************************************************************************
// Print Array
/***************************************************************************/

if(!function_exists('pr')){
	function pr($data=array()){
		if($data){
			echo "<pre>"; print_r($data); echo "</pre>";
		}
	}
}

/***************************************************************************
// ERROR Flash message as per the user action response
/***************************************************************************/
if(!function_exists('setErrorFlashData')){
	function setErrorFlashData($message){
		if($message != ''){
			$homzz = & get_instance();
	        return $homzz->session->set_flashdata('message', '<div class="alert alert-danger alert-dismissable"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Error ! &nbsp;&nbsp;</strong>'.$message.'</div>');
        }else{		
			return '';
		}
	}
}

/***************************************************************************
// WARNING Flash message as per the user action response
/***************************************************************************/
if(!function_exists('setWarningFlashData')){
	function setWarningFlashData($message){
		if($message != ''){
			$homzz = & get_instance();
	        return $homzz->session->set_flashdata('message', '<div class="alert alert-warning alert-dismissable"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Warning ! &nbsp;&nbsp;</strong>'.$message.'</div>');
        }else{		
			return '';
		}
	}
}

/***************************************************************************
// SUCCESS Flash message as per the user action response
/***************************************************************************/
if(!function_exists('setSuccessFlashData')){
	function setSuccessFlashData($message){
		if($message != ''){
			$homzz = & get_instance();
	        return $homzz->session->set_flashdata('message', '<div class="alert alert-success alert-dismissable"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Success ! &nbsp;&nbsp;</strong>'.$message.'</div>');
        }else{		
			return '';
		}
	}
}

// ***********************************************************
// Check if session is active than go ahead else redirect to login page
/****************************************************************************/
if(!function_exists('checkSessionData')){
	function checkSessionData(){
		$homzz = & get_instance();
		if( $homzz->session->userdata('homzz_user_id') != '' && $homzz->session->userdata('homzz_user_fname') != '' ) {			
			
			$session = $homzz->session->all_userdata();
			if($session['homzz_user_id'] != '' && $session['homzz_user_fname'] != ''
			){
				return true;
			}else{
				return false;
			}
		}else{
			return false;
		}	
	}
}

// ***********************************************************
// Get session data by session name 
/******************************************************/ 
if(!function_exists('getSessionData')){
	function getSessionData($sessionName){
		$homzz = & get_instance();
        if( $sessionName != '') {
			$sessionName = 'homzz_'.$sessionName;
			if( $homzz->session->userdata($sessionName) )  {	
				return $homzz->session->userdata($sessionName);
			}else{
				return false;
			}
		}else{
			return false;
		}	
	}
}


// ***********************************************************
// Table record Modification info 
/******************************************************/ 

if(!function_exists('modificationInfo')){
	function modificationInfo($user_id=''){
		$homzz =  & get_instance();
		if ($homzz->agent->is_browser())
		{
			$agent = $homzz->agent->browser().' '.$homzz->agent->version();
		}
		elseif ($homzz->agent->is_robot())
		{
			$agent = $homzz->agent->robot();
		}
		elseif ($homzz->agent->is_mobile())
		{
			$agent = $homzz->agent->mobile();
		}
		else
		{
			$agent = '';
		}

		$infoData = array();
		$infoData['modified_on'] = date('Y-m-d H:i:s');
		$infoData['modified_by'] = $user_id;
		$infoData['modified_by_ip'] = $homzz->input->ip_address();
		$infoData['modified_by_browser'] = $agent;
		if($user_id != ''){
			$infoData['modified_by'] = $user_id;
		}
		return $infoData;
	}
}

// ***********************************************************
// Table record Creation info 
/******************************************************/
if(!function_exists('creationInfo')){
	function creationInfo($user_id= ''){
		$homzz =  & get_instance();
		if ($homzz->agent->is_browser())
		{
			$agent = $homzz->agent->browser().' '.$homzz->agent->version();
		}
		elseif ($homzz->agent->is_robot())
		{
			$agent = $homzz->agent->robot();
		}
		elseif ($homzz->agent->is_mobile())
		{
			$agent = $homzz->agent->mobile();
		}
		else
		{
			$agent = '';
		}
		
		$infoData = array();
		$infoData['created_on'] = date('Y-m-d H:i:s');
		$infoData['created_by_ip'] = $homzz->input->ip_address();
		$infoData['created_by_browser'] = $agent;
		if($user_id != ''){
			$infoData['created_by'] = $user_id;
		}
		return $infoData;
	}
	
	
if(!function_exists('decode_id_url')){
	function decode_id_url($decodeid = false){
		if($decodeid){
			return explode('|', decodeurl($decodeid));
		}else{
			return false;
		}
	}
}
	
	
	if(!function_exists('encode_id_url')){
	function encode_id_url($id=false){
		if($id){
			$session_id = session_id();
			$new_id = $session_id.'|'.$id;
			return $encode_id = encodeurl($new_id);
		}else{
			return false; 
		}
		
	}
}

if(!function_exists('encodeurl')){
	function encodeurl($string, $key = "", $url_safe = TRUE) {
		if ($key == null || $key == "") {
			$key = "tyz_mydefaulturlencryption";
		}
		$CI = & get_instance();
		$ret = $CI->encrypt->encode($string, $key);
		if ($url_safe) {
			$ret = strtr(
					$ret, array(
				'+' => '.',
				'=' => '-',
				'/' => '~'
					)
			);
		}

		return $ret;
	}
}

if(!function_exists('decodeurl')){
	function decodeurl($string, $key = "") {
		if ($key == null || $key == "") {
			$key = "tyz_mydefaulturlencryption";
		}
		$CI = & get_instance();
		$string = strtr(
				$string, array(
			'.' => '+',
			'-' => '=',
			'~' => '/'
				)
		);

		return $CI->encrypt->decode($string, $key);
	}
}

	
}
?>