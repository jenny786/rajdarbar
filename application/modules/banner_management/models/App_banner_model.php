<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class App_banner_model extends CI_Model{
	
	function __construct() {
        parent::__construct();
        
    }
	
	public function getBanner() {		
        $this->db->select("tbl_banner.*","tbl_media.image");
        $this->db->from('tbl_banner');
        $this->db->join('tbl_media', 'tbl_banner.media_id = tbl_media.id');
        $this->db->order_by('id', 'DESC');
        return $this->db->get()->result();     
    }
	

}
