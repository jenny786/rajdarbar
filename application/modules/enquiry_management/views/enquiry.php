
<div class="content-wrapper">
<?php //$this->load->view('admin/breadcrumbs'); ?>


<section class="content">  
    <div class="box box-primary">		
	<div class="box-body">
		<?php 
			if($this->session->flashdata('message'))
			{ 
				echo $this->session->flashdata('message'); 
	        } 
	    ?>
        <div class="pull-right" style="margin-bottom:5px">            
			     
        </div>
            		
	    <div class="clearfix"></div>
	    
	        <table id="example1" class="table table-bordered table-striped table-hover">
	            <thead>
	                <tr class="alert-warning">
					    <th> &nbsp; </th>
	                    <th>Name</th>                           
	                    <th>Phone</th>                           
	                    <th>Email</th>                           
	                    <th>Comment</th>
	                    <th>Project</th>
						<th style="width:70px">Action</th>
						</tr>
	            </thead>
	            <tbody>
	                <?php  
	                if (!empty($contact)) {
						$sr=1;
	                foreach ($contact as $data) { 
					$uid = encode_id_url($data->id);
	                ?> 
	                    <tr>
						   <td> <?php echo $sr; ?></td>
							<td><?php echo $data->name; ?></td>
							<td><?php echo $data->phone; ?></td>
							<td><?php echo $data->email; ?></td>
							<td><?php echo $data->comment; ?></td>
							<td><?php $Catid = $data->equcat;
                          $contact ="SELECT * from tbl_Category where id='$Catid'";
						 $bannerresult = $this->db->query($contact);
						 echo $text1 = @$bannerresult->result()[0]->cate_name; 

							?></td>
							<td>
							<a class="btn btn-danger"  onclick="return confirm('Are you sure want to delete this record ?')"  data-toggle="tooltip" data-placement="top" title="Delete" data-csrfs="<?php echo $this->security->get_csrf_hash(); ?>"  href="<?php echo base_url('enquiry_management/delete_record_by_id_home_enquiry/')?><?php echo $uid; ?>" id="<?php echo $uid; ?>" style="padding: 0px 4px;"> <i class="fa fa-trash-o"></i> </a>
							</td>

							
	                    </tr>
	                <?php 
					 $sr++;
					} } ?> 
	            </tbody>
	        </table>               
        
    </div>
    </div>
</section>
</div>

<script>

/****************************************************
* Script to filter table Record data
/**********************************************************/
$(function () {
   $("#example1").DataTable( {
		aaSorting: [],
		pageLength: 25,
		paging: true,
		bFilter: true,
		bInfo: false,
		bSortable: true,
		bRetrieve: true,
		aoColumnDefs: [
                {"aTargets": [0], "bSortable": true},
                {"aTargets": [1], "bSortable": true},
                {"aTargets": [2], "bSortable": true},
                {"aTargets": [3], "bSortable": true},
                {"aTargets": [4], "bSortable": true},
                {"aTargets": [5], "bSortable": false}
            ]
	
	});
  });
</script>
