<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Subscribe_newsletter extends CI_Controller {
	
	function __construct() {
        parent::__construct();
		
        if(checkSessionData() == false)
	    { 
	      redirect('admin'); 
	    }	
    }
	public function index()
	{
		$this->load->model('subscribe_newsletter/App_subscribe_newsletter_model');
		$data['career_list'] = $this->App_subscribe_newsletter_model->getCareerEnquiry();
		$data['main_content'] = 'subscribe_newsletter/subscribe_newsletter';
		$this->load->view('admin/layout',$data);
	}
	public function ajax_careerstatus() {
	$return = array();
	$return['csrfvalue'] = $this->security->get_csrf_hash(); 	
	
	$post_value = $this->input->post();
    $result = $this->App_subscribe_newsletter_model->status_active_inacive($post_value);
    $html = '';
		if($result['status']==1){
		$title = "Active";
		$text = "Active";
		$class = "label-success statuslabel label label-default activeDeactive Changecsrf";
		$status = "1";
			 
		}else{
		$title = "De-Active";
		$text = "De-Active";
		$class = "label-danger statuslabel label label-default activeDeactive Changecsrf";	
		$status = "0";	
		}
		$html ='<a title="'.$title.'"  href="javascript:void(0);" record_id="'.$_POST['record_id'].'" record_status="'.$status.'" data-csrf="'.$this->security->get_csrf_hash().'" class="'.$class.'">'.$text.' </a>';
		$return['html'] = $html;
		echo json_encode($return);    

    
    }
	
	public function editCareer($id=false){
		 
		$this->load->model('subscribe_newsletter/App_subscribe_newsletter_model');
		$user_id = $this->session->userdata('homzz_user_id');
		$this->form_validation->set_error_delimiters('<div class="error">', '</div>');
		 if($this->input->post()) {
				    
					$static_pageData2 =modifyUserIpDetail($user_id);
					$static_pageData1 = $this->input->post();
					$static_pageData = array_merge($static_pageData1, $static_pageData2);
					$updateStatus = $this->App_subscribe_newsletter_model->updatecareer_page($static_pageData,$id);
					if ($updateStatus) {
			$this->session->set_flashdata('success_message','updated sucessfully');
						redirect('subscribe_newsletter');
						exit();
					} else {
						$this->session->set_flashdata('error_message','error_message');
						redirect('subscribe_newsletter');
					}
		}  else {
					
					$result_module = $this->App_subscribe_newsletter_model->fetchEditCareer($id);
					if($result_module){
						//pr($result_module); exit;
						$data['fetchmodule'] = $result_module;
					}else{
						$this->session->set_flashdata('error_message','error_message');
						redirect('subscribe_newsletter');
					}
					
					$data['main_content'] = 'subscribe_newsletter/editCareer';
					$this->load->view('admin/layout', $data);
				}
	}
	
	
	
	public function deleteCareerenquiry($id)
	{
		            $this->load->model('subscribe_newsletter/App_subscribe_newsletter_model');
					$this->App_subscribe_newsletter_model->deletecareerenquiry($id);
					$this->session->set_flashdata('success_message','deleted sucessfully');
					redirect('subscribe_newsletter');
					exit();
			
	}
	
	public function export_excel_sheet(){
		$this->load->model('subscribe_newsletter/App_subscribe_newsletter_model');
			$config = array(
					array(
							'field' => 'fromdate',
							'label' => 'From Date',
							'rules' => 'required|trim'
					),
					array(
							'field' => 'todate',
							'label' => 'To Date',
							'rules' => 'required|trim'
							
					)
			);

			$this->form_validation->set_rules($config);
			
			if ($this->form_validation->run() == FALSE)
			{
				$this->session->set_flashdata('error_message', 'Something went wrong.. ' );
				redirect('home');
			}
			else
			{
				
				$this->load->helper('php-excel');
				$fields = (
					$field_array[] = array (
								"ID",
								"Email",
								"Date"
					)
				);
				$query = $this->App_subscribe_newsletter_model->getUsersData();
				//print_r($query); die; 
				$i=1;	
				foreach ($query['resultSet'] as $row) {
					//$date = date_create($row->created_date); 
					$yrdata= strtotime($row->created_date); 
					$date = date('M-Y-d', $yrdata); 	
					$data_array[] = array(
								$row->id,
								$row->email,
								$date
						); 
						$i++;
				}
				$xls = new Excel_XML;
				$xls->addArray ($field_array);
				$xls->addArray ($data_array);
			//	$xls->addArray ($data_plans);
				$xls->generateXML ("ExportToExcel");
			}
					
	}
	
	
	/*=== for unique Slug name ===================== */
	
	
}
