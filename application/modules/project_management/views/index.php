<style>
.bgnone {
	background-color: transparent;
}
#example1 td span:last-child {
    display: none;
}
</style>
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
	<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
	<script>
	$( function() {
	//$( ".datepicker" ).datepicker();
		$( "#fromdate" ).datepicker({
			dateFormat: 'yy-mm-dd',
			changeMonth: true,
			changeYear: true,
			yearRange: "-80:+0",
			maxDate: 0
			
		});
		$( "#todate" ).datepicker({
			dateFormat: 'yy-mm-dd',
			changeMonth: true,
			changeYear: true,
			yearRange: "-80:+0",
			maxDate: 0
		});
	} );
	</script>
<div class="content-wrapper">


    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
       <?php echo $page_title?>
      </h1>
	 </section>
      
    

    <!-- Main content -->
    <section class="content">

              <?php if($this->session->flashdata('success_message')){ ?>
  <?php echo $this->session->flashdata('success_message'); ?>
<?php }else if($this->session->flashdata('error_message')){ ?>
   <?php echo $this->session->flashdata('error_message'); ?>
<?php } ?>
      <div class="row">
        <div class="col-xs-12">
            <div class="box">
            <div class="box-header" >
			<ol class="breadcrumb bgnone">
					<li><a href="<?php echo base_url('dashboard');?>"><i class="fa fa-dashboard"></i> <?php echo 'home' ;?></a></li>
					<li class="active"> <?php echo $page_title?></li>
				
					
				</ol>
				
				   <div class="pull-right">            
			<a href="<?php echo base_url() . "project_management/gallery/create" ?>" class="btn btn-block btn-primary"></i>Add Project Gallery</a>  
        </div>

            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                   <th> Project Name </th>
                  <th> Action </th>
                </tr>
                </thead>
                <tbody>
				<?php if(!empty($recordAll)){ 
					$i=1;
					foreach($recordAll as $value){?>
					<tr>
            <td><?php echo $project[$value->project_id];?></td>
						<td>
						
							<a class="btn btn-info" data-toggle="tooltip" data-placement="top" title="Edit" href="<?php echo base_url()."project_management/gallery/update/" .$value->id; ?>" style="padding: 0px 4px;"><i class="fa fa-edit"></i></a>     

					<a class="btn btn-danger" data-toggle="tooltip" data-placement="top" title="" href="<?php echo base_url() . "project_management/gallery/delete/" . $value->id; ?>" style="padding: 0px 4px;" data-original-title="Delete Subscribe Newsletter" onclick="return confirm('Are you sure? you want to delete.')"><i class="fa fa-trash"></i></a> 
						
												
						</td>
					</tr>
                <?php $i++; } }?>
                </tbody>
               
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->

         
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <script>
  $(document).ready(function() {
        $(document).on('click', '.activeDeactive', function() {
			record_id = $(this).attr('record_id');
            status = $(this).attr('record_status');
			var cval = $(this).data('csrf');
			if (status == '1') {
                var r = confirm("Are you sure you want to De-activate?");
            } else {
                var r = confirm("Are you sure you want to Activate?");
            }
			
            if (r == true) {
				$.post('<?php echo base_url() ?>subscribe_newsletter/ajax_careerstatus/', {'record_id': record_id, 'status': status, '<?php echo $this->security->get_csrf_token_name(); ?>':cval}, function(data) {
                        var obj = jQuery.parseJSON(data);
						var csrfvalue = obj.csrfvalue;
						$('.recordDataDIV' + record_id).html(obj.html);
						$('.Changecsrf').attr('data-csrf', csrfvalue);
				});
            }
        });
    });
  

  $(function () {
   $("#example1").DataTable( {
		aaSorting: [],
		pageLength: 25,
		paging: true,
		bFilter: true,
		bInfo: false,
		bSortable: true,
		bRetrieve: true,
		aoColumnDefs: [
                {"aTargets": [0], "bSortable": true},
                {"aTargets": [1], "bSortable": false},
                
            ]
	
	});
  });

  function export_check()
    {
        var start_date = $('#fromdate').val();
        var end_date = $('#todate').val();

        var startDate = new Date($('#fromdate').val().replace(/-/g,"/"));
        var endDate = new Date($('#todate').val().replace(/-/g,"/"));
		
        if(start_date != '' && end_date != '')
        {   
            if(startDate != 'Invalid Date' && endDate != 'Invalid Date') {
                if (startDate >= endDate){
                    alert("Start Date Time Should be less than End Date Time", function() {
                    });                
                    $("#extract-excel").attr('disabled','disabled');
                    return false;
                }
                else{
                    $("#extract-excel").removeAttr('disabled');
                }
            }
        }
        else
        {
            $("#extract-excel").attr('disabled','disabled');
            return false;
        }
    }
</script>
