<?php
    $description  		  = (isset($rtrnData)) ? $rtrnData->description : ''; 
	//$image  		  = (isset($rtrnData)) ? $rtrnData->image : ''; 

    //$record_id 	  = $this->strencrypt->decode($this->uri->segment(3));
?>
<div class="content-wrapper">

<section class="content">  
	 <?php if($this->session->flashdata('success_message')){ ?>
  <?php echo $this->session->flashdata('success_message'); ?>
<?php }else if($this->session->flashdata('error_message')){ ?>
   <?php echo $this->session->flashdata('error_message'); ?>
<?php } ?>

	  <div class="box-header" >
			<ol class="breadcrumb bgnone">
					<li><a href="<?php echo base_url('dashboard');?>"><i class="fa fa-dashboard"></i> <?php echo 'home' ;?></a></li>
					<li class="active"> <?php echo $page_title?></li>
				
					
				</ol>
				
				
            </div>
            
    <div class="box box-primary">       
        <div class="box-body">

                
				<?php $attributes = array('id' => 'frmProject', 'name'=>'frmProject');
				echo form_open_multipart('', $attributes); ?>
                   
				<?php if($this->session->flashdata('message')){ echo $this->session->flashdata('message'); } ?>
				<?php if($this->session->flashdata('error_image')){ ?>
	<div class="alert alert-danger alert-dismissable">
		<a href="#" class="close" data-dismiss="alert" aria-label="close">x</a>
		<strong>Oops!</strong> <?php echo $this->session->flashdata('error_image'); ?>
	</div>
<?php } ?>   
                    
                    <div class="col-md-12">
					
				
					   
						<div class="row">
                            <div class="col-md-12 ">
                                <div class="form-group">
                                    <label>Description <span style="color:#f00">*</span></label>
                                    <textarea class="form-control medium_rtb ckeditor" id="description"  name="description" placeholder="Enter Description"><?php echo set_value('description', $description);?></textarea> 
									<?php echo form_error('description'); ?>
                                </div>
                            </div>
                        </div>
						
						
						
						<div class="box-footer" style="padding:1px">
                            <button type="submit" name="submitBTN" class="btn btn-primary"><?php echo ($this->uri->segment(2) == 'update') ? 'Update' : 'Save' ?></button>
                            &nbsp; &nbsp; 
                            <a href="<?php echo base_url() . 'project_management/leasing'; ?>" class="btn btn-danger">Cancel</a>
                        </div>                       
                    </div>                       
                </form>
            </div>
        </div>  
    </section>
</div>
<script>
 /****************************************************************************
* Submit the form data 
******************************************************************************/  	
	$.validator.addMethod("lettersonly", function(value, element) {
	return this.optional(element) || /^[a-zA-Z\s]*$/.test(value);
	}, "Letters only please");
	
	jQuery.validator.addMethod("alpha_dash", function(value, element) {
        return this.optional(element) || /^[a-z0-9_ \-]+$/i.test(value); 
    }, "Alphanumerics, spaces, underscores & dashes only.");
	
	$.validator.addMethod('filesize', function (value, element, param) {
    return this.optional(element) || (element.files[0].size <= param)
}, 'File size must be less than {0}');
	
	$("#frmProject").validate({
		errorClass: "validationError",
        rules: {
               p_id:"required",
               name:{
				  required:true
				},
				dated:{
				  required:true
				},
				mainimage:{
				  extension: "jpg|jpeg|png|gif",
				  filesize: 500000
				},
				img:{
				  extension: "jpg|jpeg|png|gif",
				  filesize: 500000
				},
				meta_title:"required",
				meta_keyword:"required"
            },
            messages: {
                name: {
					required: "Page Name is required",
					lettersonly: "Enter Valid Name ",
				},
				mainimage:{
				  extension: "Please upload .jpg or .png or file of notice.",
				  filesize: "file size must be less than 500 KB"
				},
				img:{
				  extension: "Please upload .jpg or .png or file of notice.",
				  filesize: "file size must be less than 500 KB"
				},
				meta_title:"Meta title is required",
				meta_keyword:"Meta keyword is required"
            },
		submitHandler: function(form) {
			form.submit();		
		}
	});
	
</script>
<script src="<?php echo base_url() . "template/plugins/datepicker/bootstrap-datepicker.js" ?>"></script>
<link rel="stylesheet" href="<?php echo base_url() . "template/plugins/datepicker/datepicker3.css" ?>">
<script>
$('#datepicker').datepicker({
        //other option
        autoclose: true,
		format: 'yyyy-mm-dd'
    });
</script>