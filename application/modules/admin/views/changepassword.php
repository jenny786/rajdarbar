<div class="content-wrapper">
<?php //$this->load->view('admin/breadcrumbs'); ?>
<section class="content">  
    <div class="box box-primary">       
        <div class="box-body">
                <?php $attributes = array('method'=>'post','id' => 'changepasswordform', 'name'=>'changepasswordform', 'onsubmit'=>'return checkStrength();');
			echo form_open_multipart('admin/changepassword', $attributes); ?>
                   
				<?php if($this->session->flashdata('message')){ echo $this->session->flashdata('message'); } ?>
                    
                    <div class="col-md-12">
 						
                       	<div class="row">
                            <div class="col-md-12 ">
                                <div class="form-group">
                                    <label>Old Password <span style="color:#f00">*</span></label>
                                    <input type="text" class="form-control" id="old_password"  name="old_password" placeholder="Old Password" maxlength="150"> 
									<?php echo form_error('old_password'); ?>
                                </div>
                            </div>
                        </div>
						
						<div class="row">
                            <div class="col-md-12 ">
                                <div class="form-group">
                                    <label>New Password <span style="color:#f00">*</span></label>
                                    <input type="text" maxlength="20" minlength="8" class="form-control" id="password"  name="password" placeholder="Enter Password" >
									<span id="result" style="color: red;font-family: 'Source Sans Pro','Helvetica Neue',Helvetica,Arial,sans-serif;font-size: 12px;font-weight: 700"></span>									
									<?php echo form_error('password'); ?>
                                </div>
                            </div>
                        </div>
						
						<div class="row">
                            <div class="col-md-12 ">
                                <div class="form-group">
                                    <label>Confirm Password <span style="color:#f00">*</span></label>
                                    <input type="text" class="form-control" id="cnfrm_password"  name="cnfrm_password" placeholder="Confirm Password"> 
									<?php echo form_error('cnfrm_password'); ?>
                                </div>
                            </div>
                        </div>
						<div class="box-footer" style="padding:1px">
                            <button type="submit" name="submitBTN" class="btn btn-primary"><?php echo ($this->uri->segment(2) == 'update') ? 'Update' : 'Save' ?></button>
                            &nbsp; &nbsp; 
                            <a href="<?php echo base_url() . 'cms'; ?>" class="btn btn-danger">Cancel</a>
                        </div>                       
                    </div>                       
                </form>
            </div>
        </div>  
    </section>
</div>
<script >	
  function checkStrength(inputtxt){  
	var inputtxt = $('#password').val();
	if(inputtxt.length >=8){
		var decimal=  /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[^a-zA-Z0-9])(?!.*\s).{8,15}$/;  
		if(inputtxt.match(decimal)){   
			return true;
		} else{   
			$('#result').text('your password contain at least one lowercase letter, one uppercase letter, one numeric digit, and one special character');
			return false;
		}  
	}
}
$(document).ready(function(){
	$("#changepasswordform").validate({
		errorClass: "validationError",
		rules: {
			password:
			{
				required: true,
				minlength: 8,
			},
			cnfrm_password:
			{
				required: true,
				equalTo: password,
				minlength: 8
			}				
		}
	});
});
</script>
