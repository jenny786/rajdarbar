<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>ACE Ltd | Log in</title>
	<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
	<link rel="stylesheet" href="<?php echo base_url();?>public/admin/css/bootstrap.min.css">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
	<link rel="stylesheet" href="<?php echo base_url();?>public/admin/css/AdminLTE.min.css">
	<link rel="stylesheet" href="<?php echo base_url();?>public/admin/css/blue.css">
	<style>
	.error{
		color: red;
	}
	</style>
	
	
	
</head>
<body class="hold-transition login-page" >
<div class="login-box">
  <div class="login-logo">
    <a href="<?php echo base_url('dashboard');?>"><b>Ace</b>Ltd</a>
  </div>
  <!-- /.login-logo -->
  <div class="login-box-body">
    <p class="login-box-msg">Change Password</p>
	<?php if($this->session->flashdata('error_message')){ ?>
	<div class="alert alert-danger alert-dismissable text-center">
		<a href="#" class="close" data-dismiss="alert" aria-label="close">x</a>
		<strong>Oops!</strong> <?php echo $this->session->flashdata('error_message'); ?>
	</div>
<?php } ?>
    <!--<form id="commentForm"  action="<?php //echo base_url('admin/authAuthentication');?>" method="post">-->
	
	<?php $attributes = array('method'=>'post','id' => 'changepasswordform', 'name'=>'changepasswordform',  'onsubmit'=>'return checkStrength();');
			echo form_open_multipart('', $attributes); ?>
				<div class="box-body">
					<div class="form-group">
					
					
							<div class="form-group">				
								<label for="exampleInputEmail1">New Password <span class = "error">*</span></label>
								<br>
								<input type="password" id="password" name="password" class="form-control required" placeholder="New Password" maxlength="20" minlength="8"/>
								<span id="result" style="color: red;font-family: 'Source Sans Pro','Helvetica Neue',Helvetica,Arial,sans-serif;font-size: 12px;font-weight: 700"></span>
								<span class="error" ><?php echo form_error('password');?></span>
								 
							</div>	
							<div class="form-group">	
								<label for="exampleInputEmail1">Confirm Password <span class = "error">*</span></label>
								<br>
								<input type="password" id="cnfrm_password" name="cnfrm_password" class="form-control required" placeholder="Confirm Password"  />
								<span class="error"><?php echo form_error('cnfrm_password');?></span> 
								 
							</div>		
							
					
					

	
					</div>
              <!-- /.box-body -->

              <div class="box-footer">
                <button type="submit" class="btn btn-primary">Submit</button>
				<a href="<?php echo base_url('admin');?>" class="btn btn-danger">Cancel</a>
              </div>
            </form>
          </div>
  </div>
</div>
<style>
.captcha {
    padding: 0px !important;
}
</style>

</body>
<script src="<?php echo base_url();?>public/admin/js/jquery-2.2.3.min.js"></script>
<!-- Bootstrap 3.3.6 -->
<script src="<?php echo base_url();?>public/admin/js/bootstrap.min.js"></script>
<!-- iCheck -->
<script src="<?php echo base_url();?>public/admin/js/icheck.min.js"></script>
<script src="<?php echo base_url();?>public/admin/js/jquery.validate.js"></script>
<script>
	$(document).ready(function() {
		$("#admin_login").validate({
			rules: {
				username: "required",
				password: "required",
				captcha_code: "required"				
			},
			messages: {
				username: "Please enter your username",
				password: "Please enter your password",
				captcha_code: "Please enter captcha code"
			},
			
			submitHandler: function(form) {
			  form.submit();
			}
			
		});
	});
	 /* $.validator.setDefaults({
		submitHandler: function() {
			form.submit();
		}
	}); */ 
	
	
	</script>

<script>	
  function checkStrength(inputtxt){  
	var inputtxt = $('#password').val();
	if(inputtxt.length >=8){
		var decimal=  /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[^a-zA-Z0-9])(?!.*\s).{8,15}$/;  
		if(inputtxt.match(decimal)){   
			return true;
		} else{   
			$('#result').text('your password contain at least one lowercase letter, one uppercase letter, one numeric digit, and one special character');
			return false;
		}  
	}
}
$(document).ready(function(){
		$("#changepasswordform").validate({
			rules: {
				password:
                {
                    required: true,
                    minlength: 8,
                },
                cnfrm_password:
                {
                    required: true,
                    equalTo: password,
                    minlength: 8
                },
			
			}
			
			

	});
});


</script>
</html>
