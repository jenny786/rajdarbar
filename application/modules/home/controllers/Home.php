<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Home extends CI_Controller {
	//Admin@rajdar123
	function __construct() {
        parent::__construct();
		
		$this->load->helper('cookie');
		$this->load->model('home/App_home_model');
	}


	
	public function index()
	{
		$data['meta_title'] = '';
		$data['meta_description'] = '';
		$data['meta_keywords'] = '';
		$this->load->view('front/header',$data);
		$data['main_content'] = 'home/index';
		$this->load->view('front/layout', $data);
	}
	
	public function about()
	{
		$data['meta_title'] = '';
		$data['meta_description'] = '';
		$data['meta_keywords'] = '';
		$this->load->view('front/header',$data);
		$data['main_content'] = 'home/about';
		$this->load->view('front/layout', $data);
	}
	public function business()
	{
		$data['meta_title'] = '';
		$data['meta_description'] = '';
		$data['meta_keywords'] = '';
		$this->load->view('front/header',$data);
		$data['main_content'] = 'home/business';
		$this->load->view('front/layout', $data);
	}
	public function csr()
	{
		$data['meta_title'] = '';
		$data['meta_description'] = '';
		$data['meta_keywords'] = '';
		$this->load->view('front/header',$data);
		$data['main_content'] = 'home/csr';
		$this->load->view('front/layout', $data);
	}
	public function media()
	{
		$data['meta_title'] = '';
		$data['meta_description'] = '';
		$data['meta_keywords'] = '';

		$data['recordAll'] = $this->App_home_model->getMedia();

		$this->load->view('front/header',$data);
		$data['main_content'] = 'home/media';
		$this->load->view('front/layout', $data);
	}
	public function career()
	{
		$data['meta_title'] = '';
		$data['meta_description'] = '';
		$data['meta_keywords'] = '';
		$this->load->view('front/header',$data);
		$data['main_content'] = 'home/career';
		$this->load->view('front/layout', $data);
	}


	

	 function apply_now_submit()
	{
		
		$this->load->library('form_validation');
		$this->form_validation->set_rules('name', 'Enter Cat Name', 'required|alpha_dash|encode_php_tags|xss_clean');
		$this->form_validation->set_rules('email', 'Enter Email Id', 'required|encode_php_tags|xss_clean');
		$this->form_validation->set_error_delimiters('<div class="error">', '</div>');
		if ($this->form_validation->run() == TRUE){		
		$post_value = $this->input->post();
		
		if(!empty($_FILES['cv']['name'])){
                $config['upload_path'] = 'images/cv/';
                $config['allowed_types'] = 'pdf|doc|docs|gif|docx';
				$config['file_name'] = time() . date('Ymd');				
				$this->load->library('upload');
				$this->upload->initialize($config);				
                $this->load->library('upload');
                $this->upload->initialize($config);
                if($this->upload->do_upload('cv')){
                    $uploadData = $this->upload->data();
					$picture = $uploadData['file_name'];
					$this->load->library('image_lib');
					//$this->image_lib->clear();
                }else{ 
				echo $this->upload->display_errors(); 
				   $error = array('error' => $this->upload->display_errors());
					$this->session->set_flashdata('error',$error['error']);
                redirect('home/career','refresh');				
                    $picture = '';
                } 
            }else{
                $picture = '';
            }
			$postValue['name'] = $post_value['name'];
			$postValue['email'] = $post_value['email'];
			$postValue['mobile'] = $post_value['mobile'];
			$postValue['dob'] = $post_value['dob'];
			$postValue['gender'] = $post_value['gender'];
			$postValue['subject'] = $post_value['subject'];
			$postValue['position'] = $post_value['position'];
			$postValue['comment'] = $post_value['comment'];
			$postValue['cv'] = $picture;
			
			$inserted = $this->App_home_model->insertCareerEnquiry($postValue);
			if($inserted['status']=='true'){
            $bodyData = '<h3>Career enquiry from globalreality.com :</h3><table border="0" cellpadding="10">
				<tr><th>Name </th><th>: </th><td>'.$post_value['name'].'</td></tr>
				<tr><th>Email </th><th>: </th><td>'.$post_value['email'].'</td></tr>
				<tr><th>Mobile </th><th>: </th><td>'.$post_value['mobile'].'</td></tr>			
				<tr><th>CV </th><th>: </th><td>'.base_url().'images/cv/'.$picture.'</td></tr>
				</table>';
				//echo $bodyData;die;

				$this->load->library('email');
				$this->email->from($post_value['email'], $post_value['name']); 
				$this->email->to("info@rajdarbarrealty.com");
				$this->email->subject('Enquiry - Careers');
				$this->email->set_mailtype('html');
				$this->email->message($bodyData);
				if($this->email->send()){ 	
				  $this->session->set_flashdata('success_message', 'Thanks, We Will Contact You Soon!!');
				  redirect('home/career');
				}
				$this->session->set_flashdata('error_message', 'Oops!!! Email could not be sent');
				 redirect('home/career');
			}else{
				$this->session->set_flashdata('error_message', 'There are some error ');
				redirect('home/career');
			}
		}
	}
	public function contact_us()
	{
		$data['meta_title'] = '';
		$data['meta_description'] = '';
		$data['meta_keywords'] = '';
		$this->load->view('front/header',$data);
		$data['main_content'] = 'home/contact-us';
		$this->load->view('front/layout', $data);
	}
	function contactsubmit()
	{
		$post_value = $this->input->post();
		unset($post_value['CaptchaInput']);
		$rtrn = addRecord('tbl_contact', $post_value, 'false');
				$bodyData = '<h3>Contact us enquiry from globalreality.com :</h3><table border="0" cellpadding="10">
				<tr><th>Name </th><th>: </th><td>'.$post_value['name'].'</td></tr>
				<tr><th>Email </th><th>: </th><td>'.$post_value['email'].'</td></tr>
				<tr><th>Contact </th><th>: </th><td>'.$post_value['phone'].'</td></tr>
				<tr><th>Project </th><th>: </th><td>'.$post_value['project'].'</td></tr>
				<tr><th>Message </th><th>: </th><td>'.$post_value['message'].'</td></tr>
				</table>';
				//echo $bodyData;die;
				$this->load->library('email');
				$this->email->from($post_value['email'], $post_value['name']); 
				//$this->email->to("info@rajdarbarrealty.com");
				//$this->email->bcc("sumit.makan@csipl.net");
				$this->email->subject('Enquiry - Contact Us');
				$this->email->set_mailtype('html');
				$this->email->message($bodyData);
				$this->email->send();
				if($this->email->send()){ 	
					$this->session->set_flashdata('alert', 'thankyou');
					redirect('home/thankyou');
				}else
				  {
					setErrorFlashData('Something goes wrong, Please try again');
					redirect('home/contact_us');
				  }						
	}
	public function thankyou()
	{
		$error = $this->session->flashdata('alert');
		if($error=='thankyou') {
		$data['meta_title'] = '';
		$data['meta_description'] = '';
		$data['meta_keywords'] = '';
		$this->load->view('front/header',$data);
		$data['main_content'] = 'home/thankyou';
		$this->load->view('front/layout', $data);
			
		}else{
			redirect('home');
		}	
	}
	public function mission_vision()
	{
		$data['meta_title'] = '';
		$data['meta_description'] = '';
		$data['meta_keywords'] = '';
		$this->load->view('front/header',$data);
		$data['main_content'] = 'home/mission-vision';
		$this->load->view('front/layout', $data);
	}
	public function chairman_message()
	{
		$data['meta_title'] = '';
		$data['meta_description'] = '';
		$data['meta_keywords'] = '';
		$this->load->view('front/header',$data);
		$data['main_content'] = 'home/chairmen-message';
		$this->load->view('front/layout', $data);
	}
	public function online_payment()
	{
		$data['meta_title'] = '';
		$data['meta_description'] = '';
		$data['meta_keywords'] = '';
		$this->load->view('front/header',$data);
		$data['main_content'] = 'home/online-payment';
		$this->load->view('front/layout', $data);
	}
	public function global_foyer_gurgaon()
	{
		$data['meta_title'] = 'Global Foyer Golf Course Road';
		$data['meta_description'] = 'Global Foyer is Gurgaon&acute;s first luxury mall. It offers premium space on Gurgaon&acute;s most sought after locale - the Golf Course Road.';
		$data['meta_keywords'] = 'Global Foyer , Golf Course Road Gurgaon, shopping experience,Premium space available for retail,luxury spa,Retail & Commercial space, Fine Dining in gurugram';

        $data['gallery'] = $this->App_home_model->getProjectGallery(1);
		$this->load->view('front/header',$data);
		$data['main_content'] = 'home/global-foyer-gurgaon';
		$this->load->view('front/layout', $data);
	}
	public function global_foyer_palam_vihar()
	{
		$data['meta_title'] = 'Global Foyer Palam Vihar : The Perfect Address for Global ';
		$data['meta_description'] = 'Global Foyer Palam Vihar, well developed in every aspect, this buzzing feature has been bereft of a global standard commercial landmark of its own. But, not for long! After the success of Global Foyer, Gurgaon, we are now ready to give Palam Vihar its very own Global Foyer.';
		$data['meta_keywords'] = 'Global Foyer,retail, commercial , serviced apartments,Palam Vihar';

		$data['gallery'] = $this->App_home_model->getProjectGallery(2);
		
		$this->load->view('front/header',$data);
		$data['main_content'] = 'home/global-foyer-palam-vihar';
		$this->load->view('front/layout', $data);
	}

	public function global_spaces_hisar()
	{
		$data['meta_title'] = 'Rajdarbar Spaces Hisar: modern habitats for modern india';
		$data['meta_description'] = 'Rajdarbar Spaces is the best township in Hisar, conveniently located near Jindal Chowk right on Delhi-Hisar-Sirsa bypass road, approachable from both Raipur and Mirzapur roads. It is the first modern township situated in one of the best residential sectors in Hisar, adjacent to HUDA plots of Sector 1 and Sector 3.';
		$data['meta_keywords'] = 'Hisar township ,Sector 1 and Sector 3 hisar, hisar International Airport, Delhi-Hisar-Sirsa bypass road';
		$this->load->view('front/header',$data);
		$data['gallery'] = $this->App_home_model->getProjectGallery(3);
		$data['main_content'] = 'home/global-spaces-hisar';
		$this->load->view('front/layout', $data);
	}


	public function global_spaces_sirsa()
	{
		$data['meta_title'] = 'Rajdarbar Spaces  Sirsa: Royal Heritage, Modern Lifestyle';
		$data['meta_description'] = 'Rajdarbar Spaces, offers a new, regal way of life to the residents of Sirsa.';
		$data['meta_keywords'] = 'Sirsa township , plots in sirsa,plots in sirsa haryana';
		$data['gallery'] = $this->App_home_model->getProjectGallery(4);
		$this->load->view('front/header',$data);
		$data['main_content'] = 'home/global-spaces-sirsa';
		$this->load->view('front/layout', $data);
	}

	public function global_spaces_agra()
	{
		$data['meta_title'] = 'Rajdarbar Village agra: luxurious independent row houses ';
		$data['meta_description'] = 'Rajdarbar Village gives its residents luxurious independent row houses just a little away from the bustle of the busy main city. The thoughtful township has all amenities at walking distance. ';
		$data['meta_keywords'] = 'independent row houses, Rajdarbar Limited, commercial space';
		$this->load->view('front/header',$data);
		$data['gallery'] = $this->App_home_model->getProjectGallery(5);
		$data['main_content'] = 'home/global-spaces-agra';
		$this->load->view('front/layout', $data);
	}
	
	public function global_spaces_karnal_phase1()
	{
		$data['meta_title'] = '';
		$data['meta_description'] = '';
		$data['meta_keywords'] = '';
		$this->load->view('front/header',$data);
		$data['gallery'] = $this->App_home_model->getProjectGallery(6);
		$data['main_content'] = 'home/global-spaces-karnal-phase1';
		$this->load->view('front/layout', $data);
	}
	public function global_spaces_karnal_phase2()
	{
		$data['meta_title'] = '';
		$data['meta_description'] = '';
		$data['meta_keywords'] = '';
		$this->load->view('front/header',$data);
		$data['gallery'] = $this->App_home_model->getProjectGallery(7);
		$data['main_content'] = 'home/global-spaces-karnal-phase2';
		$this->load->view('front/layout', $data);
	}
	public function commercial_leasing()
	{
		$data['meta_title'] = '';
		$data['meta_description'] = '';
		$data['meta_keywords'] = '';

		 $data['rtrnData'] = getRecordById('tbl_leasing', array('id'=>1) );
		$this->load->view('front/header',$data);
		$data['main_content'] = 'home/commercial_leasing';
		$this->load->view('front/layout', $data);
	}
	public function newsletter()
	{
		$post_value = $this->input->post();
		$csrfName = $this->security->get_csrf_hash();
		$postValue['email'] = $post_value['email'];
		$postValue['status'] = 0;
		$inserted = $this->db->insert('subscribe_newsletter', $post_value);
		if($inserted)//data inserted 
		{
			$response['msg'] = 'Thank you for Subscribe Newsletter.';
			$response['status'] = 'Success';                          
			$response['csrfName'] = $csrfName;                          
			echo json_encode($response);
		}
		else
		{
			$response['msg'] = 'Something went wrong, Please try again!!!!';
			$response['status'] = 'Failed';
			echo json_encode($response);
			
		}
	} 
	
	
	
}
