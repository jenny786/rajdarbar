<div class="banner inner-b wow fadeInDown">
	<div class="banner-part1" style="background-image: url(<?php echo base_url();?>public/front/images/banner_about_us.jpg);">
		<img style="display: none;" src="<?php echo base_url();?>public/front/images/banner_about_us.jpg" alt=""/>
	</div>
</div>
<div class="banner-text inner wow fadeInDown" data-wow-delay="100ms">
	<div class="container">
		<div class="col-lg-12 col-md-12 col-sm-12 banner-text-main">
			<div class="col-lg-3 col-md-3 col-sm-3 banner-text-left">
				<h3 class="wow fadeInDown" data-wow-delay="200ms">About us</h3>
			</div>
			<div class="col-lg-9 col-md-9 col-sm-9 banner-text-right">
				<div class="bre">
				<ul class="wow fadeInDown" data-wow-delay="300ms">
					<li><a href="<?php echo base_url()?>">home</a></li> 
					<li><a href="javascript:;">corporate</a></li>         
					<li><span>about us</span></li> 
				</ul>
				</div>
			</div>
		</div>
	</div>
</div>


<div class="about-area">
	<div class="container">
		<div class="row">
			<div class="col-md-4 col-sm-5">
				<div class="about-bottom">
					<div class="row">
						<div class="col-md-12 col-sm-12">
							<div class="box left">
								<h2>Chairman's Message</h2>
								<p><span><img src="<?php echo base_url();?>public/front/images/quote-icon1.png" alt="Quote Icon1"></span> Infrastructure spells the biggest challenge, creating a template of immense possibilities. India is predicted to become the world’s second largest economy by 2020, with the largest intellectual talent pool contributing to every possible vertical in finance, banking, hi-tech, aerospace and the life sciences that spells mega entrepreneurial matrices.<span2><img src="<?php echo base_url();?>public/front/images/quote-icon2.png" alt="Quote Icon2"></span2></p>
								<div class="more"><a href="<?php echo base_url()?>home/chairmen_message">Read More</a></div>
							</div>
						</div>
						<div class="col-md-12 col-sm-12">
							<div class="box right">
								<h2>Vision &amp; Mission</h2>
								<p><span><img src="<?php echo base_url();?>public/front/images/quote-icon1.png" alt="Quote Icon1"></span> To position <strong>Rajdarbar Limited</strong> We exist to make a positive difference in the way India lives, earns &amp; learns. Be it dream homes, work spaces, retail destinations or buildings for those in the hospitality industry, educational institutions, financial or power sector <span2><img src="<?php echo base_url();?>public/front/images/quote-icon2.png" alt="Quote Icon2"></span2></p>

								<div class="more"><a href="<?php echo base_url()?>home/mission_vision">Read More</a></div>
							</div>
						</div>
					</div>
				</div>
			</div>

			<div class="col-md-8 col-sm-7">
				<div class="top-content">
					<div class="box">
						<h2>Bringing Regal Standards to Indian Real Estate</h2>
						<p>Rajdarbar Limited (Formerly known as Global Realty) is a leading real estate company with commercial and residential projects like Global Foyer Gurgaon, Global Foyer Palam Vihar; Rajdarbar Spaces in Hisar, Agra , Sirsa; and Global Spaces in Karnal. Transforming challenges into opportunities has been the hallmark of the Rajdarbar Group, ever since its inception. The Group is a diversified conglomerate with business interests in Real Estate, Hospitality, Finance, Dairy, Trading and IT Service Sector.</p>
					</div>
				</div>

				<div class="about-content">
					<div class="box left">
						<p>Spreading Royalty to the tier 2 and even tier 3 cities. We are India’s first real estate developer that took its commitment to bringing global standards to the tier 2 &amp; tier 3 cities of India. While all the big players were busy planning their projects in the major metros, Rajdarbar Limited understood that even people in the tier 2 &amp; tier 3 cities dream about well planned townships, commercial spaces, group housing and entertainment zones. Today, our projects Global Foyer and Rajdarbar Spaces are proud landmarks in New Delhi, Gurgaon, Palam Vihar, Hisar, Sirsa, Agra, Karnal, Vrindavan, Bangalore &amp; Jaipur and have redefined the quality and lifestyle benchmarks of the Indian real estate. In recent years our realty business has grown many folds and we have emerged as one of the most progressive and multi-faceted real estate and construction entity in the country. The future for us is even bigger. Its global!</p>
						<p>For detailed information about our various projects, please visit our <a href="<?php echo base_url()?>home/business">project pages</a>.</p>
					</div>
					<div class="box right">
						<a href="javascript:;"><img src="<?php echo base_url();?>public/front/images/about-desc.png" alt="logo" /></a>
					</div>
				</div>
			</div>
		</div>
	
	</div>
</div>


	