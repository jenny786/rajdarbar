
<!--popup box-->
<div class="black11"></div>
<div class="popbox"><div class="popclose">X</div>
	<!--<h2>Global Foyer Golf Course Road: Key Highlights</h2>-->
	<ul>
		<li>45 acre integrated township</li>
		<li>Ultramodern facilities</li>
		<li>Easy accessibility from city landmarks</li>
		<li>Round-the-clock security</li>
		<li>Entry from 45 mtrs. Wide sector road</li>
		<li>Main entrance with round-the-clock security</li>
		<li>health care centers</li>
		<li>primary &amp; nursery schools </li>
		<li>24 hr taxi stand</li>
		<li>Well developed parks</li>
		<li>Children play area</li>
		<li>Amphitheater</li>
	</ul>
</div>
<!--end popup-->

<div class="banner inner wow fadeInDown">
	
	<div class="banner-slider owl-carousel owl-theme">
		<div class="item">
			<div class="banner-part" style="background-image: url(<?php echo base_url();?>public/front/images/s-slider-1.jpg);">
				<img src="<?php echo base_url();?>public/front/images/s-slider-1.jpg" alt="inner banner" style="display: none;" />
			</div>
		</div>
		<div class="item">
			<div class="banner-part" style="background-image: url(<?php echo base_url();?>public/front/images/s-slider-2.jpg);">
				<img src="<?php echo base_url();?>public/front/images/s-slider-2.jpg" alt="inner banner" style="display: none;" />
			</div>
		</div>
		<div class="item">
			<div class="banner-part" style="background-image: url(<?php echo base_url();?>public/front/images/s-slider-3.jpg);">
				<img src="<?php echo base_url();?>public/front/images/s-slider-3.jpg" alt="inner banner" style="display: none;" />
			</div>
		</div>
	</div>
	
	<div class="rotate-part">
		<div class="container">
			<div class="banner-bottom">
				<div class="spaces pull-left banner-bottom-left wow fadeInLeft" data-wow-delay="100ms">
					<img src="<?php echo base_url();?>public/front/images/residential-spaces.jpg" alt=""/>
				</div>
				<div class="spaces pull-right banner-bottom-right">
						<a class="wow fadeInDown enquiry" data-wow-delay="100ms" href="<?php base_url()?>home/contact_us"><img src="<?php echo base_url();?>public/front/images/icon-enquiry1.png" alt=""/> Enquiry</a>
					<a class="wow fadeInDown" data-wow-delay="200ms" target="_blank" href="<?php echo base_url();?>public/front/images/pdf/Global-Spaces-Sirsa.pdf"><img src="<?php echo base_url();?>public/front/images/eb.png" alt=""/> E-Brochure</a>
				</div>
			</div>
		</div>
	</div>
</div>

	<div class="banner-text inner bre-slider">
		<div class="container">
			<div class="col-lg-12 col-md-12 col-sm-12 banner-text-main">
				<div class="col-lg-4 col-md-4 col-sm-4 banner-text-left">
					<h3 class="wow fadeInDown" data-wow-delay="100ms">Sirsa</h3>
				</div>
				<div class="col-lg-8 col-md-8 col-sm-8 banner-text-right">
					<div class="bre">
					<ul class="wow fadeInLeft" data-wow-delay="100ms">
						<li><a href="<?php echo base_url();?>">home</a></li>     
						<li>businesses</li>      
						<li>residential</li>      
						<li><span>sirsa</span></li> 
					</ul>
					</div>
				</div>
			</div>
		</div>
	</div>
	
	
	<section class="overview residential">
		<div class="container">
			<div class="col-lg-12 col-md-12 col-sm-12 overview-main">
				<h2 class="wow fadeInDown" data-wow-delay="100ms">Overview</h2>
				<h3 class="wow fadeInDown" data-wow-delay="100ms">Sirsa: Royal Heritage, Modern Lifestyle</h3>
				
				<div class="float_img" style="">
							<img src="<?php echo base_url();?>public/front/images/global-spaces-sirsa-th.jpg" alt="Global Spaces Hisar" />
				</div>
				
				<p class="wow fadeInDown" data-wow-delay="200ms">Sirsa can trace its history all the way back to the Mahabharata. A city as ancient as Sirsa, brings about its own sweet challenges at modernisation. At Rajdarbar Limited, we like challenges. And today, with our township we can proudly say that we have added a few more illustrious pages to Sirsa's history!</p>
				<p class="wow fadeInDown" data-wow-delay="100ms">Rajdarbar Spaces, offers a new, regal way of life to the residents of Sirsa. </p>
				<h4 class="wow fadeInDown" data-wow-delay="100ms">A life that blends in the green environment, parks, walkways and fountains with rugged and beautifully crafted concrete to give the residents fresh memories everyday. </h4>
			</div>
		</div>
	</section>
	
	<section class="plan-feature">
		<div class="container">
			<div class="col-lg-12 col-md-12 col-sm-12 plan-feature-main fix-h">
				<h2 class="wow fadeInDown" data-wow-delay="100ms">Plans &amp; Features</h2>
				<div class="tab-system">

					<button type="button" class="navbar-toggle open">
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span> 
					</button>

				  <!-- Nav tabs -->
				  <ul class="nav nav-tabs nav-tabs1 wow fadeInDown" data-wow-delay="100ms" id="product-tabs-area" role="tablist">
				  	<li role="presentation" class="active"><a href="#tab1" role="tab" data-toggle="tab">Elevation</a></li>
					<li role="presentation"><a href="#tab2" role="tab" data-toggle="tab">Key Highlight</a></li>
					<li role="presentation"><a href="#tab3" role="tab" data-toggle="tab">Floor Plans</a></li>
					<li role="presentation"><a href="#tab4" aria-controls="home" role="tab" data-toggle="tab">Specifications</a></li>
				 <?php if(!empty($gallery)){ ?>
					<li role="presentation"><a href="#tab5" role="tab" data-toggle="tab">Photo Gallery</a></li>
				<?php } ?>
					<li role="presentation"><a href="#tab6" role="tab" data-toggle="tab">Site Plan</a></li>
					<li role="presentation"><a href="#tab8" role="tab" data-toggle="tab">Location Map</a></li>
				  </ul>

				  <!-- Tab panes -->
				  <div class="tab-content">
					<div class="tab-pane wow fadeInDown active" data-wow-delay="300ms" id="tab1">
						<div class="elev-slider owl-carousel owl-theme">
							<div class="item wow fadeInDown" data-wow-delay="130ms">
								<div class="shops-text">
									<img src="<?php echo base_url();?>public/front/images/s-slider-1.jpg" alt=""/>
								</div>
							</div>
							<div class="item wow fadeInDown" data-wow-delay="130ms">
								<div class="shops-text">
									<img src="<?php echo base_url();?>public/front/images/s-slider-2.jpg" alt=""/>
								</div>
							</div>
						</div>	
					</div>
					<div class="tab-pane wow fadeInDown" data-wow-delay="300ms" id="tab2">
						<div class="key-heighlight">
							<ul class="key-list">
								<li>
									<div class="img">
										<a href="javascript:;">
											<img class="img1" src="<?php echo base_url();?>public/front/images/icon/icon-acred-township.png" alt="keyhilight" />
											<img class="img2" src="<?php echo base_url();?>public/front/images/icon/iconw-acred-township.png" alt="keyhilight" />
										</a>
									</div>
									<div class="desc">
										<p>45 acre integrated  <br>township</p>
									</div>
								</li>
								<li>
									<div class="img">
										<a href="javascript:;">
											<img class="img1" src="<?php echo base_url();?>public/front/images/icon/icon-modern-home.png" alt="keyhilight" />
											<img class="img2" src="<?php echo base_url();?>public/front/images/icon/iconw-modern-home.png" alt="keyhilight" />
										</a>
									</div>
									<div class="desc">
										<p>Ultramodern <br>facilities</p>
									</div>
								</li>
								<li>
									<div class="img">
										<a href="javascript:;">
											<img class="img1" src="<?php echo base_url();?>public/front/images/icon/icon-landscape.png" alt="keyhilight" />
											<img class="img2" src="<?php echo base_url();?>public/front/images/icon/iconw-landscape.png" alt="keyhilight" />
										</a>
									</div>
									<div class="desc">
										<p>Easy accessibility from <br>city landmarks</p>
									</div>
								</li>
								<li>
									<div class="img">
										<a href="javascript:;">
											<img class="img1" src="<?php echo base_url();?>public/front/images/icon/icon-road.png" alt="keyhilight" />
											<img class="img2" src="<?php echo base_url();?>public/front/images/icon/iconw-road.png" alt="keyhilight" />
										</a>
									</div>
									<div class="desc">
										<p>Entry from 45 mtrs. <br>Wide sector road</p>
									</div>
								</li>
								<li>
									<div class="img">
										<a href="javascript:;">
											<img class="img1" src="<?php echo base_url();?>public/front/images/icon/icon-key-highlight-7.png" alt="keyhilight" />
											<img class="img2" src="<?php echo base_url();?>public/front/images/icon/iconw-key-highlight-7.png" alt="keyhilight" />
										</a>
									</div>
									<div class="desc">
										<p>Main entrance with <br>round-the-clock security</p>
									</div>
								</li>
								<li>
									<div class="img">
										<a href="javascript:;">
											<img class="img1" src="<?php echo base_url();?>public/front/images/icon/icon-yoga.png" alt="keyhilight" />
											<img class="img2" src="<?php echo base_url();?>public/front/images/icon/iconw-yoga.png" alt="keyhilight" />
										</a>
									</div>
									<div class="desc">
										<p>health care centers</p>
									</div>
								</li>
								<li>
									<div class="img">
										<a href="javascript:;">
											<img class="img1" src="<?php echo base_url();?>public/front/images/icon/icon-scool.png" alt="keyhilight" />
											<img class="img2" src="<?php echo base_url();?>public/front/images/icon/iconw-scool.png" alt="keyhilight" />
										</a>
									</div>
									<div class="desc">
										<p>primary &amp; nursery <br>schools</p>
									</div>
								</li>
								<li>
									<div class="img">
										<a href="javascript:;">
											<img class="img1" src="<?php echo base_url();?>public/front/images/icon/icon-taxi-stand.png" alt="keyhilight" />
											<img class="img2" src="<?php echo base_url();?>public/front/images/icon/iconw-taxi-stand.png" alt="keyhilight" />
										</a>
									</div>
									<div class="desc">
										<p>24 hr taxi stand</p>
									</div>
								</li>
								<li>
									<div class="img">
										<a href="javascript:;">
											<img class="img1" src="<?php echo base_url();?>public/front/images/icon/icon-park.png" alt="keyhilight" />
											<img class="img2" src="<?php echo base_url();?>public/front/images/icon/iconw-park.png" alt="keyhilight" />
										</a>
									</div>
									<div class="desc">
										<p>Well developed parks</p>
									</div>
								</li>
								<li>
									<div class="img">
										<a href="javascript:;">
											<img class="img1" src="<?php echo base_url();?>public/front/images/icon/icon-kids.png" alt="keyhilight" />
											<img class="img2" src="<?php echo base_url();?>public/front/images/icon/iconw-kids.png" alt="keyhilight" />
										</a>
									</div>
									<div class="desc">
										<p>Children play area</p>
									</div>
								</li>
								<li>
									<div class="img">
										<a href="javascript:;">
											<img class="img1" src="<?php echo base_url();?>public/front/images/icon/icon-amphitheater-&-club.png" alt="keyhilight" />
											<img class="img2" src="<?php echo base_url();?>public/front/images/icon/iconw-amphitheater-&-club.png" alt="keyhilight" />
										</a>
									</div>
									<div class="desc">
										<p>Amphitheater</p>
									</div>
								</li>
							</ul>
							<p class="more-text">For Detailed Key Highlights Please <a href="javascript:;" class="more-vv">Click Here</a></p>
						</div>
					</div>
					<div class="tab-pane wow fadeInDown" data-wow-delay="300ms" id="tab3">
					
						<div class="tab-pane wow fadeInDown" data-wow-delay="300ms" id="tab3">
									<div class="key-heighlight min-height">
												<p>On Request</p>
									</div>
						</div>
						
								
					</div>
					<div class="tab-pane wow fadeInDown" data-wow-delay="300ms" id="tab4">
						
						<div class="tab-pane wow fadeInDown" data-wow-delay="300ms" id="tab3">
									<div class="key-heighlight min-height">
												<p>On Request</p>
									</div>
						</div>
						
						
					</div>
					<div class="tab-pane wow fadeInDown" data-wow-delay="300ms" id="tab5">
						<div class="elev-slider owl-carousel owl-theme wow fadeInUp">
										
					 <?php if(!empty($gallery)) { foreach ($gallery as $key => $value) { ?>
							<div class="item">
								<div class="shops-text">
								<img src="<?php echo base_url('images/project_gallery/'.$value->image);?>" alt="gurgaon" />	
							</div>
							</div>
					<?php } } ?>
						</div>
					</div>
					<div class="tab-pane wow fadeInDown docs-pictures" data-wow-delay="300ms" id="tab6">
						<div class="Walkthrough">
							<span><img data-original="<?php echo base_url();?>public/front/images/sp-sirsa-bg.jpg" src="<?php echo base_url();?>public/front/images/sp-sirsa-th.jpg" alt="Location" /></span>
						</div>	
					</div>
					<div class="tab-pane1" id="tab8">
						<iframe src="https://www.google.co.in/maps/d/embed?mid=1l3Jg87TU3FeWPvjuQQKAKo-YgynenrH9" width="600" height="450" style="border:0"></iframe>
					</div>
				  </div>
				</div>
			</div>
		</div>
	</section>
	
	<section class="downloads">
		<div class="container">
			<div class="col-lg-12 col-md-12 col-sm-12 downloads-main">
				<h2 class="wow fadeInDown" data-wow-delay="100ms">Downloads</h2>
				<div class="downloads-icon">
					<div class="downloads-icon-text">
						<a class="one-a" target="_blank" href="<?php echo base_url();?>public/front/images/pdf/Global-Spaces-Sirsa.pdf"><p class="wow fadeInLeft" data-wow-delay="100ms">E-Brochure</p></a>					
					</div>
					<div class="downloads-icon-text">
						<a class="one-b" target="_blank" href="<?php echo base_url();?>public/front/images/pdf/Application-form_Sirsa.pdf"><p class="wow fadeInLeft" data-wow-delay="200ms">Application Form</p></a>				
					</div>
					<div class="downloads-icon-text">
						<a class="one-c one-e" target="_blank" href="javascript:;"><p class="wow fadeInLeft" data-wow-delay="300ms">Payment Plan</p></a>				
					</div>
					<div class="downloads-icon-text">
						<a href="javascript:;" class="one-d one-e"><p data-wow-delay="400ms" class="wow fadeInLeft">Price List</p></a>				
					</div>
				</div>
			</div>
		</div>
	</section>
	
	<section class="loans">
		<div class="container">
			<div class="col-lg-12 col-md-12 col-sm-12 loans-main">
				<h2 class="wow fadeInDown" data-wow-delay="100ms">Home Loans Available By</h2>
				<div class="loans-slider owl-carousel owl-theme wow fadeInDown" data-wow-delay="300ms">
					<div class="item">
						<div class="loans-text">
							<img src="<?php echo base_url();?>public/front/images/bank1.jpg" alt=""/>
						</div>
					</div>
					<div class="item">
						<div class="loans-text">
							<img src="<?php echo base_url();?>public/front/images/bank-lic.jpg" alt=""/>
						</div>
					</div>
					<div class="item">
						<div class="loans-text">
							<img src="<?php echo base_url();?>public/front/images/bank-pnb.jpg" alt=""/>
						</div>
					</div>
					<div class="item">
						<div class="loans-text">
							<img src="<?php echo base_url();?>public/front/images/bank4.jpg" alt=""/>
						</div>
					</div>
					<div class="item">
						<div class="loans-text">
							<img src="<?php echo base_url();?>public/front/images/bank2.jpg" alt=""/>
						</div>
					</div>
					<div class="item">
						<div class="loans-text">
							<img src="<?php echo base_url();?>public/front/images/bank5.jpg" alt=""/>
						</div>
					</div>
					<div class="item">
						<div class="loans-text">
							<img src="<?php echo base_url();?>public/front/images/bank7.jpg" alt=""/>
						</div>
					</div>
				</div>	
			</div>
		</div>
	</section>
	
	
	<section class="location-map">
		<div class="container">
			<div class="map-area">
				<h2 class="wow fadeInDown" data-wow-delay="100ms">Location</h2>
				<ul class="location-map-main">
					<li>
						<h3 class="wow fadeInDown" data-wow-delay="200ms">Corporate Office</h3>
						<p class="wow fadeInDown" data-wow-delay="300ms">303, 3rd Floor, Global Foyer, Golf Course Road, <br>Sector-43, Gurgaon -122002, (Haryana) India</p>
						<p class="wow fadeInDown" data-wow-delay="100ms">
							<span>Email :</span> 
							<a href="mailto:info@globalrealty.com">info@rajdarbarrealty.com</a>
							<a href="mailto:customercare@globalrealty.com">customercare@rajdarbarrealty.com</a>
							<a href="mailto:sales@globalrealty.com">sales@rajdarbarrealty.com</a>
						</p>
						<p class="wow fadeInDown" data-wow-delay="400ms"><span></span> <a href="javascript:;"> </a> <a href="javascript:;"></a></p>
					</li>
					<li>
						<h3 class="wow fadeInDown" data-wow-delay="200ms">Site Office</h3>
						<p class="wow fadeInDown" data-wow-delay="300ms">Sector - 1, Opp Sector 20 Part - III HUDA, Sirsa - 125055 (Haryana)</p>
						<p class="wow fadeInDown" data-wow-delay="100ms">
							<span></span> 
							<a href="javascript:;"></a>
							<a href="javascript:;"></a>
							<a href="javascript:;"></a>
						</p>
						<p class="wow fadeInDown" data-wow-delay="400ms"><span>Tel :</span> <a href="callto:919991888940">+91 9991888940 </a> <a href="javascript:;"></a></p>
					</li>
				</ul>
				<div class="map-location wow fadeInDown google-map-wrap" >
					<div class="map">
						<iframe class="g-map" src="https://www.google.com/maps/d/embed?mid=1wcLmJzu5etrXpkSCOoaHb89jwvmZxK5r" width="640" height="480"></iframe>
					</div>
				</div>
			</div>
		</div>
		
	</section>