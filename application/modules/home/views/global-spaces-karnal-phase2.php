<!--popup box-->
<div class="black11"></div>
<div class="popbox"><div class="popclose">X</div>
	<!--<h2>Global Foyer Golf Course Road: Key Highlights</h2>-->
	<ul>
		<li>A green environment with landscaped parks and fountains</li>
		<li>Wide, tarred internal roads, lined with trees</li>
		<li>Underground electrical wiring</li>
		<li>Underground sewage system</li>
		<li>Excellent drainage and rain-water harvesting system</li>
		<li>Manned gate with 24-hour security</li>
		<li>Driveway with ample parking space</li>
		<li>Nursery and primary school</li>
		<li>Clinic and convenience store</li>
	</ul>
</div>
<!--end popup-->

<div class="banner inner wow fadeInDown">
	<!--<div class="banner-part1" style="background-image: url(images/gs-karnal2-1.jpg);">
		<img style="display: none;" src="images/gs-karnal2-1.jpg" alt=""/>
	</div>-->
	
	<div class="banner-slider owl-carousel owl-theme">
		<div class="item">
			<div class="banner-part" style="background-image: url(<?php echo base_url();?>public/front/images/gs-karnal2-1.jpg);">
				<img src="<?php echo base_url();?>public/front/images/gs-karnal2-1.jpg" alt="inner banner" style="display: none;" />
			</div>
		</div>
		<div class="item">
			<div class="banner-part" style="background-image: url(<?php echo base_url();?>public/front/images/gs-karnal2-2.jpg);">
				<img src="<?php echo base_url();?>public/front/images/gs-karnal2-2.jpg" alt="inner banner" style="display: none;" />
			</div>
		</div>
		<div class="item">
			<div class="banner-part" style="background-image: url(<?php echo base_url();?>public/front/images/gs-karnal2-3.jpg);">
				<img src="<?php echo base_url();?>public/front/images/gs-karnal2-3.jpg" alt="inner banner" style="display: none;" />
			</div>
		</div>
	</div>
	
	<div class="rotate-part">
		<div class="container">
			<div class="banner-bottom">
				<div class="pull-left banner-bottom-left wow fadeInLeft" data-wow-delay="100ms">
					<img src="<?php echo base_url();?>public/front/images/text-logo1.png" alt=""/>
				</div>
				<div class="pull-right banner-bottom-right">
					<!--<a class="wow fadeInDown" data-wow-delay="100ms" href="javascript:;"><img src="images/360.png" alt=""/> Virtual Tour</a>-->
					<a class="wow fadeInDown enquiry" data-wow-delay="100ms" href="<?php base_url()?>home/contact_us"><img src="<?php echo base_url();?>public/front/images/icon-enquiry1.png" alt=""/> Enquiry</a>
					<a class="wow fadeInDown" data-wow-delay="200ms" href="<?php echo base_url();?>public/front/images/pdf/brochure-karnal-Phase-II.pdf" target="_blank"><img src="<?php echo base_url();?>public/front/images/eb.png" alt=""/> E-Brochure</a>
				</div>
			</div>
		</div>
	</div>
</div>

	<div class="banner-text inner bre-slider">
		<div class="container">
			<div class="col-lg-12 col-md-12 col-sm-12 banner-text-main">
				<div class="col-lg-4 col-md-4 col-sm-4 banner-text-left">
					<h3 class="wow fadeInDown" data-wow-delay="100ms">Karnal Phase II <span>Delivered</span></h3>
				</div>
				<div class="col-lg-8 col-md-8 col-sm-8 banner-text-right">
					<div class="bre">
					<ul class="wow fadeInLeft" data-wow-delay="100ms">
						<li><a href="<?php echo base_url()?>">home</a></li>     
						<li>businesses</li>      
						<li>commercial</li>      
						<li><span>karnal phase II</span></li> 
					</ul>
					</div>
				</div>
			</div>
		</div>
	</div>
	
	
	<section class="overview residential">
		<div class="container">
			<div class="col-lg-12 col-md-12 col-sm-12 overview-main">
			<h2 class="wow fadeInDown marB" data-wow-delay="100ms">Overview</h2>
				<div class="float_img">
							<img src="<?php echo base_url();?>public/front/images/global-spaces-karnal2-th.jpg" alt="Global Spaces karnal" />
				</div>
				<p class="wow fadeInDown" data-wow-delay="200ms">Global Spaces, Karnal Phase 2 is a 26-acre township at sector 33, Karnal. It offers plots of various sizes. This project is an evidence of success of Global Spaces, Karnal Phase 1. It is an integrated township with ultra modern amenities, facilities, landscaped surroundings and security.</p>
			</div>
		</div>
	</section>
	
	<section class="plan-feature">
		<div class="container">
			<div class="col-lg-12 col-md-12 col-sm-12 plan-feature-main">
				<h2 class="wow fadeInDown" data-wow-delay="100ms">Plans &amp; Features</h2>
				<div class="tab-system">

					<button type="button" class="navbar-toggle open">
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span> 
					</button>

				  <!-- Nav tabs -->
				  <ul class="nav nav-tabs nav-tabs1 wow fadeInDown" data-wow-delay="100ms" id="product-tabs-area" role="tablist">
				  	<li role="presentation" class="active"><a href="#tab1" role="tab" data-toggle="tab">Elevation</a></li>
					<li role="presentation"><a href="#tab2" role="tab" data-toggle="tab">Key Highlight</a></li>
					<li role="presentation"><a href="#tab3" role="tab" data-toggle="tab">Floor Plans</a></li>
					<li role="presentation"><a href="#tab4" aria-controls="home" role="tab" data-toggle="tab">Specifications</a></li>
				 <?php if(!empty($gallery)){ ?>
					<li role="presentation"><a href="#tab5" role="tab" data-toggle="tab">Photo Gallery</a></li>
				<?php } ?>
					<li role="presentation"><a href="#tab6" role="tab" data-toggle="tab">Site Plan</a></li>
					<li role="presentation"><a href="#tab7" role="tab" data-toggle="tab">Location Map</a></li>
				  </ul>

				  <!-- Tab panes -->
				  <div class="tab-content">
					<div class="tab-pane wow fadeInDown active" data-wow-delay="300ms" id="tab1">
						<div class="elev-slider owl-carousel owl-theme">
							<div class="item wow fadeInDown" data-wow-delay="130ms">
								<div class="shops-text">
									<img src="<?php echo base_url();?>public/front/images/gs-karnal2-1.jpg" alt=""/>
								</div>
							</div>
							<div class="item wow fadeInDown" data-wow-delay="130ms">
								<div class="shops-text">
									<img src="<?php echo base_url();?>public/front/images/gs-karnal2-gallery-5.jpg" alt=""/>
								</div>
							</div>
							<div class="item wow fadeInDown" data-wow-delay="130ms">
								<div class="shops-text">
									<img src="<?php echo base_url();?>public/front/images/gs-karnal2-2.jpg" alt=""/>
								</div>
							</div>
						</div>	
					</div>
					<div class="tab-pane wow fadeInDown" data-wow-delay="300ms" id="tab2">
						<div class="key-heighlight">
							<ul class="key-list">
								<li>
									<div class="img">
										<a href="javascript:;">
											<img class="img1" src="<?php echo base_url();?>public/front/images/icon/icon-park.png" alt="keyhilight" />
											<img class="img2" src="<?php echo base_url();?>public/front/images/icon/iconw-park.png" alt="keyhilight" />
										</a>
									</div>
									<div class="desc">
										<p>A green Environment with Landscaped Parks and Fountains</p>
									</div>
								</li>
								<li>
									<div class="img">
										<a href="javascript:;">
											<img class="img1" src="<?php echo base_url();?>public/front/images/icon/icon-environment.png" alt="keyhilight" />
											<img class="img2" src="<?php echo base_url();?>public/front/images/icon/iconw-environment.png" alt="keyhilight" />
										</a>
									</div>
									<div class="desc">
										<p>Wide, Tarred Internal Roads, Lined with Trees</p>
									</div>
								</li>
								<li>
									<div class="img">
										<a href="javascript:;">
											<img class="img1" src="<?php echo base_url();?>public/front/images/icon/icon-key-highlight-10.png" alt="keyhilight" />
											<img class="img2" src="<?php echo base_url();?>public/front/images/icon/iconw-key-highlight-10.png" alt="keyhilight" />
										</a>
									</div>
									<div class="desc">
										<p>Underground Electrical Wiring</p>
									</div>
								</li>
								<li>
									<div class="img">
										<a href="javascript:;">
											<img class="img1" src="<?php echo base_url();?>public/front/images/icon/icon-key-highlight-4.png" alt="keyhilight" />
											<img class="img2" src="<?php echo base_url();?>public/front/images/icon/iconw-key-highlight-4.png" alt="keyhilight" />
										</a>
									</div>
									<div class="desc">
										<p>Underground Sewage System</p>
									</div>
								</li>
								<li>
									<div class="img">
										<a href="javascript:;">
											<img class="img1" src="<?php echo base_url();?>public/front/images/icon/icon-rain-water.png" alt="keyhilight" />
											<img class="img2" src="<?php echo base_url();?>public/front/images/icon/iconw-rain-water.png" alt="keyhilight" />
										</a>
									</div>
									<div class="desc">
										<p>Excellent Drainage and Rain-Water Harvesting System</p>
									</div>
								</li>
								<li>
									<div class="img">
										<a href="javascript:;">
											<img class="img1" src="<?php echo base_url();?>public/front/images/icon/icon-key-highlight-7.png" alt="keyhilight" />
											<img class="img2" src="<?php echo base_url();?>public/front/images/icon/iconw-key-highlight-7.png" alt="keyhilight" />
										</a>
									</div>
									<div class="desc">
										<p>Manned Gate with 24-hour Security</p>
									</div>
								</li>
								<li>
									<div class="img">
										<a href="javascript:;">
											<img class="img1" src="<?php echo base_url();?>public/front/images/icon/icon-parking.png" alt="keyhilight" />
											<img class="img2" src="<?php echo base_url();?>public/front/images/icon/iconw-parking.png" alt="keyhilight" />
										</a>
									</div>
									<div class="desc">
										<p>Driveway with ample Parking Space</p>
									</div>
								</li>
								<li>
									<div class="img">
										<a href="javascript:;">
											<img class="img1" src="<?php echo base_url();?>public/front/images/icon/icon-scool.png" alt="keyhilight" />
											<img class="img2" src="<?php echo base_url();?>public/front/images/icon/iconw-scool.png" alt="keyhilight" />
										</a>
									</div>
									<div class="desc">
										<p>Nursery and Primary School</p>
									</div>
								</li>
								<li>
									<div class="img">
										<a href="javascript:;">
											<img class="img1" src="<?php echo base_url();?>public/front/images/icon/icon-key-highlight-8.png" alt="keyhilight" />
											<img class="img2" src="<?php echo base_url();?>public/front/images/icon/iconw-key-highlight-8.png" alt="keyhilight" />
										</a>
									</div>
									<div class="desc">
										<p>Clinic and Convenience Store</p>
									</div>
								</li>
								
							</ul>
							<p class="more-text">For Detailed Key Highlights Please <a href="javascript:;" class="more-vv">Click Here</a></p>
						</div>
					</div>
					<div class="tab-pane wow fadeInDown" data-wow-delay="300ms" id="tab3">
						<div class="key-heighlight min-height">
										<p>On Request</p>
							</div>			
					</div>
					<div class="tab-pane wow fadeInDown" data-wow-delay="300ms" id="tab4">
						<div class="key-heighlight min-height">
										<p>On Request</p>
							</div>
					</div>
					<div class="tab-pane wow fadeInDown" data-wow-delay="300ms" id="tab5">
						<!--<p style="text-align: center; color:#fff; font-size: 20px; margin-top: 40px;">No Images Available</p>-->
						<div class="elev-slider owl-carousel owl-theme wow fadeInUp">
										
					 <?php if(!empty($gallery)) { foreach ($gallery as $key => $value) { ?>
							<div class="item">
								<div class="shops-text">
								<img src="<?php echo base_url('images/project_gallery/'.$value->image);?>" alt="gurgaon" />	
							</div>
							</div>
					<?php } } ?>
						</div>
					</div>
					<div class="tab-pane wow fadeInDown docs-pictures" data-wow-delay="300ms" id="tab6">
						<div class="Walkthrough">
							<span><img data-original="<?php echo base_url();?>public/front/images/sp-karnal2-bg.jpg" src="<?php echo base_url();?>public/front/images/sp-karnal2-th.jpg" alt="Location" /></span>
						</div>
					</div>
					<div class="tab-pane wow fadeInDown" data-wow-delay="300ms" id="tab7">
						<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3483.932820093233!2d75.76985921451731!3d29.166651866457656!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3912324df5c550b3%3A0x43a9171bdc1cb008!2sGlobal+Spaces!5e0!3m2!1sen!2sin!4v1532589993326" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
					</div>
				  </div>
				</div>
			</div>
		</div>
	</section>
	
	<section class="downloads">
		<div class="container">
			<div class="col-lg-12 col-md-12 col-sm-12 downloads-main">
				<h2 class="wow fadeInDown" data-wow-delay="100ms">Downloads</h2>
				<div class="downloads-icon">
					<div class="downloads-icon-text">
						<a class="one-a" href="<?php echo base_url();?>public/front/images/pdf/brochure-karnal-Phase-II.pdf" target="_blank"><p class="wow fadeInLeft" data-wow-delay="100ms">E-Brochure</p></a>					
					</div>
					<div class="downloads-icon-text">
						<a class="one-b" href="<?php echo base_url();?>public/front/images/pdf/application-form-karnal-ph2.pdf" target="_blank"><p class="wow fadeInLeft" data-wow-delay="200ms">Application Form</p></a>				
					</div>
					<div class="downloads-icon-text">
						<a class="one-c one-e" href="javascript:;"><p class="wow fadeInLeft" data-wow-delay="300ms">Payment Plan</p></a>				
					</div>
					<div class="downloads-icon-text">
						<a class="one-d one-e" href="javascript:;"><p class="wow fadeInLeft" data-wow-delay="400ms">Price List</p></a>				
					</div>
				</div>
			</div>
		</div>
	</section>
	
	<section class="loans">
		<div class="container">
			<div class="col-lg-12 col-md-12 col-sm-12 loans-main">
				<h2 class="wow fadeInDown" data-wow-delay="100ms">Home Loans Available By</h2>
				<div class="loans-slider owl-carousel owl-theme wow fadeInDown" data-wow-delay="300ms">
					<div class="item">
						<div class="loans-text">
							<img src="<?php echo base_url();?>public/front/images/bank1.jpg" alt=""/>
						</div>
					</div>
					<div class="item">
						<div class="loans-text">
							<img src="<?php echo base_url();?>public/front/images/bank-lic.jpg" alt=""/>
						</div>
					</div>
					<div class="item">
						<div class="loans-text">
							<img src="<?php echo base_url();?>public/front/images/bank-pnb.jpg" alt=""/>
						</div>
					</div>
					<div class="item">
						<div class="loans-text">
							<img src="<?php echo base_url();?>public/front/images/bank4.jpg" alt=""/>
						</div>
					</div>
				</div>	
			</div>
		</div>
	</section>
	
	
	<section class="location-map">
		<div class="container">
			<div class="map-area">
				<h2 class="wow fadeInDown" data-wow-delay="100ms">Location</h2>
				<ul class="location-map-main">
					<li>
						<h3 class="wow fadeInDown" data-wow-delay="200ms">Corporate Office</h3>
						<p class="wow fadeInDown" data-wow-delay="300ms">303, 3rd Floor, Global Foyer, Golf Course Road, <br>Sector-43, Gurgaon -122002, (Haryana) India</p>
						<p class="wow fadeInDown" data-wow-delay="100ms">
							<span>Email :</span> 
							<a href="mailto:info@globalrealty.com">info@globalrealty.com</a>
							<a href="mailto:customercare@globalrealty.com">customercare@globalrealty.com</a>
							<a href="mailto:sales@globalrealty.com">sales@globalrealty.com</a>
						</p>
						<p class="wow fadeInDown" data-wow-delay="400ms"><span></span> <a href="javascript:;"> </a> <a href="javascript:;"></a></p>
					</li>
					<li>
						<h3 class="wow fadeInDown" data-wow-delay="200ms">Site Office</h3>
						<p class="wow fadeInDown" data-wow-delay="300ms">Global Spaces, Sector 33, Behind Sector 7, <br>Karnal - 132001 (Haryana)</p>
						<p class="wow fadeInDown" data-wow-delay="100ms">
							<span></span> 
							<a href="javascript:;"></a>
							<a href="javascript:;"></a>
							<a href="javascript:;"></a>
						</p>
						<p class="wow fadeInDown" data-wow-delay="400ms"><span>Tel :</span> <a href="callto:919991888948">+91 9991888948 </a> <a href="javascript:;"></a></p>
					</li>
				</ul>
				<div class="map-location wow fadeInDown google-map-wrap" >
					<div class="map">
						<iframe class="g-map" src="https://www.google.com/maps/d/embed?mid=1wcLmJzu5etrXpkSCOoaHb89jwvmZxK5r" width="640" height="480"></iframe>
					</div>
				</div>
			</div>
		</div>
		
	</section>