

<div class="banner home wow fadeInDown">
		<div class="banner-slider owl-carousel owl-theme">
			<div class="item">
				<div class="banner-part" style="background-image: url(<?php echo base_url();?>public/front/images/home-banner1.jpg);">
					<img src="<?php echo base_url();?>public/front/images/home-banner1.jpg" style="display: none;" alt="home banner" />
					<div class="container">
						<div class="banner-text-new wow fadeInLeft">
							<h2>Little Princesses have <br>Global Royal Aspirations</h2>
							<p>A Royal Love Affair With Nature <br>Revisiting Regal India of Tomorrow</p>
						</div>
					</div>
				</div>
			</div>
			<div class="item">
				<div class="banner-part" style="background-image: url(<?php echo base_url();?>public/front/images/home-banner2.jpg);">
					<img src="<?php echo base_url();?>public/front/images/home-banner2.jpg" style="display: none;" alt="home banner" />
					<div class="container">
						<div class="banner-text-new wow fadeInLeft">
							<h2>Creating environment <br>friendly home</h2>
							<p>Holistic Living Experiences</p>
						</div>
					</div>
				</div>
			</div>
			<div class="item">
				<div class="banner-part" style="background-image: url(<?php echo base_url();?>public/front/images/home-banner3.jpg);">
					<img src="<?php echo base_url();?>public/front/images/home-banner3.jpg" style="display: none;" alt="home banner" />
					<div class="container">
						<div class="banner-text-new">
							<h2>Endeavouring <br>to Develop</h2>
							<p>A Progressive and Promising India</p>
						</div>
					</div>
				</div>
			</div>
			<div class="item">
				<div class="banner-part" style="background-image: url(<?php echo base_url();?>public/front/images/home-banner4.jpg);">
					<img src="<?php echo base_url();?>public/front/images/home-banner4.jpg" style="display: none;" alt="home banner" />
					<div class="container">
						<div class="banner-text-new"> 
							<h2>Completed 4 <br>Projects</h2>
							<p>Developing Many More Across India</p>
						</div>
					</div>
				</div>
			</div>
			<div class="item">
				<div class="banner-part" style="background-image: url(<?php echo base_url();?>public/front/images/home-banner5.jpg);">
					<img src="<?php echo base_url();?>public/front/images/home-banner5.jpg" style="display: none;" alt="home banner" />
					<div class="container">
						<div class="banner-text-new wow fadeInLeft">
							<h2>Over 6 million <br>square feet sold</h2>
							<p>By the Group</p>
						</div>
					</div>
				</div>
			</div>
			<div class="item">
				<div class="banner-part" style="background-image: url(<?php echo base_url();?>public/front/images/home-banner6.jpg);">
					<img src="<?php echo base_url();?>public/front/images/home-banner6.jpg" style="display: none;" alt="home banner" />
					<div class="container">
						<div class="banner-text-new wow fadeInLeft">
							<h2>Over 20 years <br>of real estate</h2>
							<p>development experience</p> 
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="banner-text front">
		<div class="container">
			<div class="col-lg-12 col-md-12 col-sm-12 banner-text-main">
				<div class="col-lg-3 col-md-3 col-sm-3 banner-text-left">
					<h2>Rajdarbar Limited Latest Update</h2>
				</div>
				<div class="col-lg-9 col-md-9 col-sm-9 banner-text-right">
					<marquee behavior="scroll" direction="left"
           onmouseover="this.stop();"
           onmouseout="this.start();"><p>PRIME  DEVELOPED PLOTS ....IN HISSAR....SIRSA.... &amp; KARNAL. <span>&nbsp;|&nbsp;</span> FULLY LOADED, MODERN  &amp;  ENVIRONMENTALLY LANDSCAPED. <span>&nbsp;|&nbsp;</span> ROYAL GEMS, HIGHLY INVESTOR- FRIENDLY &amp; HAPPY ON POCKETS. <span>&nbsp;|&nbsp;</span> JOYFUL LIFESTYLES FOR NEW INDIANS. </p></marquee>
				</div>
			</div>
		</div>
	</div>
	
	
	<div class="global-realty">
		<div class="container">
			<div class="col-lg-12 col-md-12 col-sm-12 global-realty-main">
				<img class="wow fadeInDown" src="<?php echo base_url();?>public/front/images/img1.png" alt=""/>
				<h2 class="orange wow fadeInDown">Welcome to The Regal Splendor of Raj Darbar</h2>
				<h2 class="wow fadeInDown">The Royalty of Indian Real Estate</h2>
				<p class="wow fadeInDown">Rajdarbar Limited (Formerly known as Global Realty) is one of the largest and only integrated real estate players in the country. Since inception, the Company has strived for world-class quality, a customer centric approach, robust engineering, in-house research, uncompromising business ethics, timeless values and transparency in all spheres of business a trusted real estate brand in India</p>
				<!--<h3 class="wow fadeInDown">We are among <span>India&rsquo;s front runners in Real Estate Development</span>  that took its commitment to bringing global standards to the <span>tier 2 &amp; tier 3</span> cities of India. </h3>-->
				<a class="wow fadeInDown" href="<?php echo base_url()?>home/about">Discover More About Us</a>
			</div>
		</div>
	</div>		
	
	
	<div class="feature-projects">
		<div class="container">
			<div class="col-lg-12 col-md-12 col-sm-12 feature-projects-main">
				<div class="pull-left">
					<h2 class="wow fadeInDown">Feature</h2> 
					<h3 class="wow fadeInDown">Projects</h3>
				</div>
				<div class="pull-right wow fadeInDown">
					<a href="<?php echo base_url()?>home/business">All Projects</a>
				</div>
				<div class="clearfix"></div>
				<div class="featured-slider owl-carousel owl-theme wow fadeInDown">
					<div class="item">
						<div class="feature-img">
							<a href="<?php echo base_url()?>home/global_foyer_gurgaon"><img src="<?php echo base_url();?>public/front/images/pic1.jpg" alt=""/></a>
						</div>
						<div class="feature-text">
							
							<a href="<?php echo base_url()?>home/global_foyer_gurgaon"><p>Global Foyer Gurgaon</p><img src="<?php echo base_url();?>public/front/images/img2.png" alt=""/></a>
						</div>
					</div>
					<div class="item">
						<div class="feature-img">
							<a href="<?php echo base_url()?>home/global_foyer_palam_vihar"><img src="<?php echo base_url();?>public/front/images/pic2.jpg" alt=""/></a>
						</div>
						<div class="feature-text">
							
							<a href="<?php echo base_url()?>home/global_foyer_palam_vihar"><p>Global Foyer Palam Vihar</p><img src="<?php echo base_url();?>public/front/images/img2.png" alt=""/></a>
						</div>
					</div>
					<div class="item">
						<div class="feature-img">
							<a href="<?php echo base_url()?>home/global_spaces_hisar"><img src="<?php echo base_url();?>public/front/images/h-home-project-th.jpg" alt=""/></a>
						</div>
						<div class="feature-text">
							
							<a href="<?php echo base_url()?>home/global_spaces_hisar"><p>Global Spaces Hisar</p><img src="<?php echo base_url();?>public/front/images/img2.png" alt=""/></a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	

	<div class="businesses">
		<h2 class="wow fadeInDown"><span>BUSINESSES</span></h2>
		<div class="container">
			<div class="col-lg-12 col-md-12 col-sm-12 businesses-main">
				<div class="businesses-box wow fadeInDown">
					<div class="businesses-img">
						<img src="<?php echo base_url();?>public/front/images/pic4.jpg" alt=""/>
						<div class="overlay">
							<div class="text addClass">Residential Townships</div>
							<div class="text1"><a href="<?php echo base_url()?>home/business#residential" class="addClass">VIEW</a></div>
						</div>
					</div>
				</div>
				<div class="businesses-box wow fadeInDown">
					<div class="businesses-img">
						<img src="<?php echo base_url();?>public/front/images/pic5.jpg" alt=""/>
						<div class="overlay">
							<div class="text">Commercial</div>
							<div class="text1"><a href="<?php echo base_url()?>home/business#commercial" class="addClass">VIEW</a></div>
						</div>
					</div>
				</div>
				<div class="businesses-box">
					<div class="businesses-img wow fadeInDown">
						<div class="businesses-top">
							<img src="<?php echo base_url();?>public/front/images/home-csr-view.jpg" alt=""/>
							<div class="overlay">
								<div class="text">CSR</div>
								<div class="text2"><a target="_blank" href="http://www.indiaeyeihro.com/">VIEW</a></div>
							</div>
						</div>
					</div>
					<div class="businesses-img wow fadeInDown">
						<div class="businesses-bottom">
							<img src="<?php echo base_url();?>public/front/images/pic7.jpg" alt=""/>
							<div class="overlay">
								<div class="text">Commodity</div>
								<div class="text2"><a target="_blank" href="http://www.rajdarbarcommodities.co.in/">VIEW</a></div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>	
	
	
	
	<div class="brands">
		<div class="container">
			<div class="col-lg-12 col-md-12 col-sm-12 brands-main">
				<h2 class="wow fadeInDown"><span>Leasing attractive spaces to nation and international brands</span></h2>
				<div class="col-lg-2 col-md-2 col-sm-3 brands-left wow fadeInDown">
					<h3>To Join as Certified Channel Partner..</h3>
					<a href="<?php echo base_url()?>home/contact_us">Click here</a>
				</div>
				<div class="col-lg-10 col-md-10 col-sm-9 brands-right">
					<div class="brands-slider owl-carousel owl-theme wow fadeInDown">
						<div class="item">
							<img src="<?php echo base_url();?>public/front/images/c12.jpg" alt=""/>
						</div>
						<div class="item">
							<img src="<?php echo base_url();?>public/front/images/c1.jpg" alt=""/>
						</div>
						<div class="item">
							<img src="<?php echo base_url();?>public/front/images/c5.jpg" alt=""/>
						</div>
						<div class="item">
							<img src="<?php echo base_url();?>public/front/images/c6.jpg" alt=""/>
						</div>
						<div class="item">
							<img src="<?php echo base_url();?>public/front/images/c7.jpg" alt=""/>
						</div>
						<div class="item">
							<img src="<?php echo base_url();?>public/front/images/c8.jpg" alt=""/>
						</div>
						<div class="item">
							<img src="<?php echo base_url();?>public/front/images/c9.jpg" alt=""/>
						</div>
						<div class="item">
							<img src="<?php echo base_url();?>public/front/images/c10.jpg" alt=""/>
						</div>
						<div class="item">
							<img src="<?php echo base_url();?>public/front/images/c11.jpg" alt=""/>
						</div>
						<div class="item">
							<img src="<?php echo base_url();?>public/front/images/c13.jpg" alt=""/>
						</div>
						<div class="item">
							<img src="<?php echo base_url();?>public/front/images/c14.jpg" alt=""/>
						</div>
						<div class="item">
							<img src="<?php echo base_url();?>public/front/images/c15.jpg" alt=""/>
						</div>
						<div class="item">
							<img src="<?php echo base_url();?>public/front/images/c16.jpg" alt=""/>
						</div>
					</div>	
				</div>
			</div>
		</div>
	</div>
	
	
	<div class="latest-news">
		<div class="container">
			<div class="col-lg-12 col-md-12 col-sm-12 latest-news-main">
				<h2 class="wow fadeInRight">Latest News</h2>
				<div class="news-box">
					<div class="news-text">
						<h3 class="wow fadeInLeft">31st December,  2017</h3>
						<p class="wow fadeInLeft">&lsquo;Smile @ 60+&rsquo;, 2017 an event under our Senior Citizen Vertical</p>
					</div>
					<div class="news-text">
						<h3 class="wow fadeInLeft">11th October, 2017</h3>
						<p class="wow fadeInLeft">&lsquo;BETIYAAN&rsquo;, 2017 an event under our Think Girl Child Vertical</p>
					</div>
					<div class="news-text">
						<h3 class="wow fadeInLeft">5th June,2017</h3>
						<p class="wow fadeInLeft">World Environment Day Event, 2017</p>
					</div>
				</div>
				<div class="buttons">
					<a href="<?php echo base_url()?>home/media" class="btn btn-view">View all</a>
				</div>
			</div>
		</div>
	</div>
