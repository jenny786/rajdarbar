<div class="banner inner-b wow fadeInDown">
	<div class="banner-part1" style="background-image: url(<?php echo base_url();?>public/front/images/banner_media-n.jpg);">
		<img style="display: none;" src="<?php echo base_url();?>public/front/images/banner_media-n.jpg" alt=""/>
	</div>
</div>
<div class="banner-text inner wow fadeInDown" data-wow-delay="100ms">
	<div class="container">
		<div class="col-lg-12 col-md-12 col-sm-12 banner-text-main">
			<div class="col-lg-3 col-md-3 col-sm-3 banner-text-left">
				<h3 class="wow fadeInDown" data-wow-delay="200ms">Media</h3>
			</div>
			<div class="col-lg-9 col-md-9 col-sm-9 banner-text-right">
				<div class="bre">
				<ul class="wow fadeInDown" data-wow-delay="300ms">
					<li><a href="<?php echo base_url()?>">home</a></li>         
					<li><span>media</span></li> 
				</ul>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="media-area">
	<div class="container">
		<div class="media-list">
			<ul>
			<?php if(!empty($recordAll)):  foreach ($recordAll as $key => $value) { ?>
			
				<li>
					<div class="box">
						<div class="img">
							<img src="<?php echo base_url('images/media/'.$value->image); ?>" alt="" />
						</div>
						<div class="info">
							<div class="title"><?php echo  date("d M, Y", strtotime($value->dated)); ?></div>
							<div class="desc"><?php echo  $value->name ?></div>
							
						</div>
					</div>
				</li>
				<?php } endif; ?>
			</ul>
		</div>
	</div>
</div>

<div class="marriage-c">
	<div class="container">
		<div class="community">
			<div class="box left">
				<h2 class="title">World record of community marriage created at Tuterkhed in Gujarat</h2>
				<p class="desc">We recently organised world&rsquo;s largest community marriage ceremony for tribal couples at Tuterkhed in the remote Vapi district of Gujarat, where 1121 couples tied the knot under one roof. It was a landmark occasion that was also recorded by the Guinness Book of Records and has been covered by all the leading national and regional news channels and publications.</p>
			</div>
			<div class="box right">
				<div class="img"><img src="<?php echo base_url();?>public/front/images/community-marriage.jpg" alt="" /></div>
			</div>
		</div>
	</div>
</div>

