<div class="banner inner-b wow fadeInDown">
	<div class="banner-part1" style="background-image: url(<?php echo base_url();?>public/front/images/banner-chairmenmessage.jpg);">
		<img style="display: none;" src="<?php echo base_url();?>public/front/images/banner-chairmenmessage.jpg" alt=""/>
	</div>
</div>
<div class="banner-text inner wow fadeInDown" data-wow-delay="100ms">
	<div class="container">
		<div class="col-lg-12 col-md-12 col-sm-12 banner-text-main">
			<div class="col-lg-3 col-md-3 col-sm-3 banner-text-left">
				<h3 class="wow fadeInDown" data-wow-delay="200ms">Chairman&rsquo;s Message</h3>
			</div>
			<div class="col-lg-9 col-md-9 col-sm-9 banner-text-right">
				<div class="bre">
				<ul class="wow fadeInDown" data-wow-delay="300ms">
					<li><a href="<?php echo base_url()?>">home</a></li> 
					<!--<li><a href="javascript:;">corporate</a></li> -->
					<li><span>corporate</span></li>         
				</ul>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="chairmen-msg">
	<div class="container">
		<div class="box">
			<h2>Chairman&rsquo;s <br />Message</h2>
			<p class="txt">India is one of the fastest growing economies of the world with a billion people that spell a trillion opportunities. In a globalized world everybody has equal opportunities and we are ahead of the curve.</p>
			<p>Infrastructure spells the biggest challenge, creating a template of immense possibilities. India is predicted to become the world&rsquo;s second largest economy by 2020, with the largest intellectual talent pool contributing to every possible vertical in finance, banking, hi-tech, aerospace and the life sciences that spells mega entrepreneurial matrices.</p>
			<p>We are determined to reign in this emerging space, creating townships, group housing and mixed- usage condominiums.</p>
			<p>The real estate market in India is still in a nascent stage and the scope is simply unlimited. The Indian real estate market progresses towards unprecedented growth and fundamental structural changes. We will play an even more active role, with a focus on creating economic efficiencies, promoting international best practices and contributing to the market growth.</p>
			<p>We exist to make a positive difference in the way India lives and wish to touch the lives of our countrymen.</p>
		</div>
	</div>
</div>

