
<div class="banner inner-b wow fadeInDown cl">
	<div class="banner-part1" style="background-image: url(<?php echo base_url();?>public/front/images/banner-business.jpg);">
		<img style="display: none;" src="<?php echo base_url();?>public/front/images/banner-business.jpg" alt=""/>
		<div class="caption2">
			<ul>
				<li><p>Always strived for benchmark quality</p></li>
				<li><p>Customer centric approach</p></li>
				<li><p>Timeless values and transparency</p></li>
			</ul>
		</div>
	</div>
</div>
	<div class="banner-text">
		<div class="container">
			<div class="col-lg-12 col-md-12 col-sm-12 banner-text-main">
				<div class="col-lg-3 col-md-3 col-sm-3 banner-text-left">
					<h3>Projects</h3>
				</div>
				<div class="col-lg-9 col-md-9 col-sm-9 banner-text-right">
					<div class="bre">
					<ul>
						<li><a href="<?php echo base_url()?>">home</a></li>          
						<li><span>projects</span></li> 
					</ul>
					</div>
				</div>
			</div>
		</div>
	</div>
	
	
	<section class="businesses-page">
		<div class="container">
			<div class="col-lg-12 col-md-12 col-sm-12 businesses-page-main">
				<h2>Rajdarbar Limited bringing global standards to Indian real estate</h2>
				<p>Rajdarbar Limited is one of the largest and only backward integrated real estate players in the country. Since inception, the Company has always strived for benchmark quality, customer centric approach, robust engineering, in-house research, uncompromising business ethics, timeless values and transparency in all spheres of business conduct, which have contributed in making it a preferred real estate brand in India.</p>
			</div>
		</div>
	</section>	

	<section class="rc" id="rc">
		<div class="container">
			<div class="col-lg-12 col-md-12 col-sm-12 rc-main">

				<div class="col-lg-3 col-md-3 col-sm-3 rc-left">
					<!-- Nav tabs -->
					<ul class="nav nav-tabs nav-tabs2" role="tablist">
						<li role="presentation" class="active"><a href="#commercial"  role="tab" data-toggle="tab">Commercial</a></li>
						<li role="presentation"><a href="#residential" role="tab" data-toggle="tab">Residential</a></li>
					</ul>
				</div>

				<div class="col-lg-9 col-md-9 col-sm-9 rc-right">
					  <!-- Tab panes -->
					  <div class="tab-content">
						<div role="tabpanel" class="tab-pane active" id="commercial">
							<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
							  <div class="panel panel-default">
							  	<div class="top-pp">
									<div class="col-lg-12 col-md-12 col-sm-12 tab-text">
										<div class="col-lg-6 tab-text-left">
											<h2>Commercial Spaces</h2>
											<p>Rajdarbar Limited commercial spaces are a perfect amalgamation of retail and office spaces. Scientifically designed keeping in mind the climatic changes, Rajdarbar Limited commercial projects are designed to tap the potential of the captive market, and are ideal for young as well as experienced entrepreneurs Passionate about conducting successful businesses.</p>
										</div>
										<div class="col-lg-6 tab-text-right">
											<img src="<?php echo base_url();?>public/front/images/pic14.jpg" alt=""/>
										</div>
									</div>
									<div class="t-btn-main">
										<a class="t-btn" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
										<img class="more-less" src="<?php echo base_url();?>public/front/images/arrow1.png" alt=""/>
											View Projects 
										</a>
									</div>
								</div>
								<div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
								  <div class="panel-body">
									<!--<h2>Rajdarbar Limited Businesses</h2>-->
									<div class="row">
										<div class="col-lg-6 col-md-6">
											<h3>Global Foyer Gurgaon</h3>
											<div class="img"><a href="<?php echo base_url()?>home/global_foyer_gurgaon"><img class="img-sr" src="<?php echo base_url();?>public/front/images/pic13.jpg" alt=""/></a></div>
											<div class="global-foyer">
												<div class="pull-left">
													<img src="<?php echo base_url();?>public/front/images/kk.png" alt=""/>
												</div>
												<div class="pull-right">
													<p>Gurgaon</p>
												</div>
												<div class="clearfix"></div>
												<h2>THE WORKPLACE OF THE FUTURE</h2>
												<h3>Global Foyer is Gurgaon's first luxury mall. It offers premium space on Gurgaon's most sought after locale - the Golf Course Road. No wonder it is the location of choice for luxury retail and automotive brands like Mercedez Benz and Hyundai Motors <!--to showcase their products and is also a hub for Fine Dining &amp; celebrations!--></h3>
												<a class="a-one" href="<?php echo base_url()?>home/global_foyer_gurgaon">Explore More</a>
												<a class="a-two p_enquiry" alt="global_foyer_gurgaon" href="<?php echo base_url()?>home/contact_us">Enquiry Now</a>
											</div>
										</div>
										<div class="col-lg-6 col-md-6">
											<h3>Global Foyer Palam Vihar</h3>
											<div class="img"><a href="<?php echo base_url()?>home/global_foyer_palam_vihar"><img class="img-sr" src="<?php echo base_url();?>public/front/images/pic12.jpg" alt=""/></a></div>
											<div class="global-foyer">
												<div class="pull-left">
													<img src="<?php echo base_url();?>public/front/images/kk1.png" alt=""/>
												</div>
												<div class="pull-right">
													<p>Palam Vihar</p>
												</div>
												<div class="clearfix"></div>
												<h2>THE WORKPLACE OF THE FUTURE</h2>
												<h3>Palam vihar is home to thousands of proud residents. Well developed in every aspect, this buzzing township has been bereft of a global standard commercial landmark of its own. But, not for long! After the success of Global Foyer, Gurgaon, we are now ready to give Palam Vihar its very own Global Foyer</h3>
												<a class="a-one" href="<?php echo base_url()?>home/global_foyer_palam_vihar">Explore More</a>
												<a class="a-two p_enquiry" alt="global_foyer_palam_vihar" href="<?php echo base_url()?>home/contact_us">Enquiry Now</a>
											</div>
										</div>
									</div>	
								  </div>
								</div>
							  </div>
							</div>
						</div>
						<div role="tabpanel" class="tab-pane" id="residential">
							<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
							  <div class="panel panel-default">
							  <div class="top-pp">
								<div class="col-lg-12 col-md-12 col-sm-12 tab-text">
									<div class="col-lg-6 tab-text-left">
										<h2>Residential</h2>
										<p>We are spreading our touch across states. Our residential townships have become the preferred destinations among discerning home seekers. We plan our projects with picturesque landscaped greenery and pleasing ambiance at strategic locations.</p>
									</div>
									<div class="col-lg-6 tab-text-right">
										<img src="<?php echo base_url();?>public/front/images/space-hisar.jpg" alt=""/>
									</div>
								</div>
								<div class="t-btn-main">
								<a class="t-btn" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="true" aria-controls="collapseTwo">
									<img class="more-less" src="<?php echo base_url();?>public/front/images/arrow1.png" alt=""/>
									View Projects 
								</a>
								</div>
								</div>
								<div id="collapseTwo" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingTwo">
								  <div class="panel-body">
									<h2>Rajdarbar Limited Businesses</h2>
									<div class="row">
										<div class="col-lg-6">
											<h3>Rajdarbar Spaces Hisar</h3>
											<div class="img"><a href="<?php echo base_url();?>home/global_spaces_hisar"><img class="img-sr" src="<?php echo base_url();?>public/front/images/h-business-project-th.jpg" alt=""/></a></div>
											<div class="global-foyer spaces">
												<div class="pull-left">
													<img src="<?php echo base_url();?>public/front/images/residential-spaces.png" alt=""/>
												</div>
												<div class="pull-right">
													<p>Hisar</p>
												</div>
												<div class="clearfix"></div>
												<h2>THE WORKPLACE OF THE FUTURE</h2>
												<h3>Welcome to Rajdarbar Spaces Hisar, a fully integrated modern township spread across 65 acres of verdant greens, that has redefined the concept of modern living. Experience the luxury of living in close proximity to nature with lush green landscape, <!--manicured gardens and broad avenues offering an enriched quality of life away from the pollution and congestion of the city, yet close to all the amenities and conveniences of modern lifestyle that will take your breath away.--></h3>
												<a class="a-one" href="<?php echo base_url();?>home/global_spaces_hisar">Explore More</a>
												<a class="a-two p_enquiry" alt="global_spaces_hisar" href="<?php echo base_url()?>home/contact_us">Enquiry Now</a>
											</div>
										</div>
										<div class="col-lg-6">
											<h3>Rajdarbar Spaces Sirsa</h3>
											<div class="img"><a href="<?php echo base_url();?>home/global_spaces_sirsa"><img class="img-sr" src="<?php echo base_url();?>public/front/images/space-sirsa.jpg" alt=""/></a></div>
											<div class="global-foyer spaces">
												<div class="pull-left">
													<img src="<?php echo base_url();?>public/front/images/residential-spaces.png" alt=""/>
												</div>
												<div class="pull-right">
													<p>Sirsa</p>
												</div>
												<div class="clearfix"></div>
												<h2>THE WORKPLACE OF THE FUTURE</h2>
												<h3>Sirsa can trace its history all the way back to the Mahabharata. A city as ancient as Sirsa, brings about its own sweet challenges at modernisation. At Raj Darbar, we like challenges. And today, with our township we can proudly say that we have added <!--a few more illustrious pages to Sirsa's history!--></h3>
												<a class="a-one" href="<?php echo base_url();?>home/global_spaces_sirsa">Explore More</a>
												<a class="a-two p_enquiry" alt="global_spaces_sirsa" href="<?php echo base_url()?>home/contact_us">Enquiry Now</a>
											</div>
										</div>
									</div>
									<div class="row">
										<div class="col-lg-6">
											<h3>Rajdarbar Village Agra</h3>
											<div class="img"><a href="<?php echo base_url();?>home/global_spaces_agra"><img class="img-sr" src="<?php echo base_url();?>public/front/images/space-agra.jpg" alt=""/></a></div>
											<div class="global-foyer spaces">
												<div class="pull-left">
													<img src="<?php echo base_url();?>public/front/images/residential-village.png" alt=""/>
												</div>
												<div class="pull-right">
													<p>Agra</p>
												</div>
												<div class="clearfix"></div>
												<h2>THE WORKPLACE OF THE FUTURE</h2>
												<h3>Agra, known for the magnificence of its Taj, is also now the home of Royal Class housing. With the inception of Rajdarbar Village at Agra, Rajdarbar Limited has ensured that Agra's illustrious past gets a beautiful future as well.</h3>
												<a class="a-one" href="<?php echo base_url();?>home/global_spaces_agra">Explore More</a>
												<a class="a-two p_enquiry" alt="global_spaces_agra" href="<?php echo base_url()?>home/contact_us">Enquiry Now</a>
											</div>
										</div>
										<div class="col-lg-6">
											<h3>Global Spaces Karnal Phase 1</h3>
											<div class="img"><a href="<?php echo base_url();?>home/global_spaces_karnal_phase1"><img class="img-sr" src="<?php echo base_url();?>public/front/images/space-karnalphase-1.jpg" alt=""/></a></div>
											<div class="global-foyer spaces">
												<div class="pull-left">
													<img src="<?php echo base_url();?>public/front/images/global-space.png" alt=""/>
												</div>
												<div class="pull-right">
													<p>Karnal Phase 1</p>
												</div>
												<div class="clearfix"></div>
												<h2>THE WORKPLACE OF THE FUTURE</h2>
												<h3>Karnal is one of historical districts of Haryana. It is also known as a city of 'Daanveer Karn' of the Mahabharata fame. This district is known world over for its production of rice, wheat and milk. Karnal is an important city on Delhi Ambala rail line &amp; Sher Shah Suri Marg located on GT Road. <!--Karnal is well connected with all important places in the country; it is approximately 123 kms from Delhi and about 130 kms from Chandigarh.--></h3>
												<a class="a-one" href="<?php echo base_url();?>home/global_spaces_karnal_phase1">Explore More</a>
												<a class="a-two p_enquiry" alt="global_spaces_karnal_phase1" href="<?php echo base_url()?>home/contact_us">Enquiry Now</a>
											</div>
										</div>
									</div>
									<div class="row">
										<div class="col-lg-6">
											<h3>Global Spaces Karnal Phase 2</h3>
											<div class="img"><a href="<?php echo base_url();?>home/global_spaces_karnal_phase2"><img class="img-sr" src="<?php echo base_url();?>public/front/images/space-karnalphase-2.jpg" alt=""/></a></div>
											<div class="global-foyer spaces">
												<div class="pull-left">
													<img src="<?php echo base_url();?>public/front/images/global-space.png" alt=""/>
												</div>
												<div class="pull-right">
													<p>Karnal Phase 2</p>
												</div>
												<div class="clearfix"></div>
												<h2>THE WORKPLACE OF THE FUTURE</h2>
												<h3>Global Spaces, Karnal Phase 2 is a 26-acre township at sector 33, Karnal. It offers plots of various sizes. This project is an evidence of success of Global Spaces, Karnal Phase 1. It is an integrated township with ultra modern amenities, facilities, landscaped surroundings and security.</h3>
												<a class="a-one" href="<?php echo base_url();?>home/global_spaces_karnal_phase2">Explore More</a>
												<a class="a-two p_enquiry" alt="global_spaces_karnal_phase2" href="<?php echo base_url()?>home/contact_us">Enquiry Now</a>
											</div>
										</div>
									</div>	
								  </div>
								</div>
							  </div>
							</div>
						</div>
					  </div>
				</div>
			</div>	
		</div>
	</section>		

	<script>
	 $(".p_enquiry").click(function(){
		   var project = $(this).attr('alt');
		    var newStr = project.replace(/_/g, " ");
		   sessionStorage.setItem('session_pro', newStr);  
		 var select_proj =  sessionStorage.getItem('session_pro');
	});
		$(window).scroll(function() {    
		    var scroll = $(window).scrollTop();
		  
		    if (scroll >= 1) {
		      $("#rc").addClass("idlink");
		    } else {
		      $("#rc").removeClass("idlink");
		    }
		  });
	</script>