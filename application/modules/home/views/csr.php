<div class="banner inner-b wow fadeInDown csr">
	<div class="banner-part1" style="background-image: url(<?php echo base_url();?>public/front/images/banner_csr-n.jpg);">
		<img style="display: none;" src="<?php echo base_url();?>public/front/images/banner_csr-n.jpg" alt=""/>
		<div class="caption2">
			<ul>
				<li><p>Our care for Mother Earth</p></li>
				<li><p>Our respect to Senior Citizens</p></li>
				<li><p>Our committed <br>campaign to empower Girl Child</p></li>
			</ul>
		</div>
	</div>
</div>
<div class="banner-text inner wow fadeInDown" data-wow-delay="100ms">
	<div class="container">
		<div class="col-lg-12 col-md-12 col-sm-12 banner-text-main">
			<div class="col-lg-3 col-md-3 col-sm-3 banner-text-left">
				<h3 class="wow fadeInDown" data-wow-delay="200ms">CSR</h3>
			</div>
			<div class="col-lg-9 col-md-9 col-sm-9 banner-text-right">
				<div class="bre">
				<ul class="wow fadeInDown" data-wow-delay="300ms">
					<li><a href="<?php echo base_url()?>">home</a></li>         
					<li><span>csr</span></li> 
				</ul>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="csr-area">
	<div class="container">
		<div class="csr-grey-box">
			<div class="box left">
				<a href="<?php echo base_url()?>"><img src="<?php echo base_url();?>public/front/images/logo.png" alt="logo" /></a>
				<h2>A socially responsible <br>company, actively working to <br>support the underprivileged</h2>
				<p>At Rajdarbar Limited, Corporate Social Responsibility is an integral part of our company policy. We strongly believe in giving back to the world we inhabit. For us, it is not just the buildings we create, or the land we develop; we care for the people, specially the marginalised and underprivileged lot whose voice we represent through India Eye IHRO, an NGO set up by our Chairman Mr. Rakesh Garg, and supported by the Rajdarbar group.</p>
			</div>
			<div class="box right">
				<img src="<?php echo base_url();?>public/front/images/social-responsibility.jpg" alt="" />
			</div>
		</div>

		<div class="flagship-area docs-pictures">
			<h2>five flagship programs</h2>
			<div class="top-desc">
				<p>The Raj Darbar Group has been at the forefront of all social and developmental  activities and is creating new benchmarks <br />for social responsibility with its extensive support to the organisation that has been working for the economic and <br />social development of the marginalised sections of society through its :</p>
			</div>
			
			<div class="envir-list">
				<ul>
					<li>
						<div class="box one">
							<div class="img">
								<span>
									<img data-original="<?php echo base_url();?>public/front/images/csr-img-big-1.jpg" src="<?php echo base_url();?>public/front/images/csr-img-1.jpg" alt="csr image" />
								</span>
							</div>
							<div class="info">
								<h3 class="title">Think Environment</h3>
								<p class="desc">For protecting the Environment, for the safety of the human race and for the existence of this mother Earth, our organisation is actively working to create awareness for environment protection, Addressing the causes of pollution, its effects on the human body and other species Remedial measures for controlling multiple kinds of pollution such as Air, Water, Noise, Over Population, E- Waste, Soil, Light and Thought pollution.</p>
							</div>
						</div>
					</li> 
					<li>
						<div class="box two">
							<div class="img">
								<span>
									<img data-original="<?php echo base_url();?>public/front/images/csr-img-big-2.jpg" src="<?php echo base_url();?>public/front/images/csr-img-2.jpg" alt="csr image" />
								</span>
							</div>
							<div class="info">
								<h3 class="title">Think Tribes</h3>
								<p class="desc">India Eye IHRO’s “Think Tribes” initiative is totally dedicated for the betterment of Tribal Communities across the world. Majority tribes live under the poverty line. We are trying to provide the facilities which are really required for social development and empowerment. </p>
							</div>
						</div>
					</li>
					<li>
						<div class="box three">
							<div class="img">
								<span>
									<img data-original="<?php echo base_url();?>public/front/images/csr-img-big-3.jpg" src="<?php echo base_url();?>public/front/images/csr-img-3.jpg" alt="csr image" />
								</span>
							</div>
							<div class="info">
								<h3 class="title">Think Senior Citizens</h3>
								<p class="desc">We are preparing to initiate ‘Apna Ghar’ addressing issues of Senior Citizens. Majority of our their population is facing problems. Our home will give them a feeling of living in their own homes with duly deserved respect, honour, care and love.</p>
							</div>
						</div>
					</li>
					<li>
						<div class="box four">
							<div class="img">
								<span>
									<img data-original="<?php echo base_url();?>public/front/images/csr-img-big-4.jpg" src="<?php echo base_url();?>public/front/images/csr-img-4.jpg" alt="csr image" />
								</span>
							</div>
							<div class="info">
								<h3 class="title">Think Blind</h3>
								<p class="desc">India eye IHRO believes in humanity. We think about the entire human being of this planet. We want to create a world where there is no darkness, no blindness. There are more than 12 million* blind people in India. That’s about 30% of the world’s total blind population. In which 80% of blindness can be prevented.</p>
							</div>
						</div>
					</li>
				</ul>
			</div>

			<div class="envir-type">
				<div class="box left">
					<div class="white-box">
						<h3>Think Girl Child</h3>
						<p>Think Girl Child, is a campaign of India Eye to end the gender selective abortion of female foetus, which has skewed the          population towards a significant underrepresentation of girls in some Indian states. Since decades, daughters of India have been denied their rightful place by a patriarchal society, entrenched in the archaic belief that women are a liability to the community. Female foeticide and infanticide to illiteracy and child marriage, the plight of the girl child in India remains a matter of utmost      concern to social scientists and policymakers. To address this, it is important to acknowledge these challenges in detail. This will enable society to identify measures to make the girl children of today, into tomorrow&lsquo;s successful and healthy women.</p>
					</div>
				</div>
				<div class="box right">
					<div class="img">
						<span>
							<img data-original="<?php echo base_url();?>public/front/images/csr-img-big-5.jpg" src="<?php echo base_url();?>public/front/images/csr-img-5.jpg" alt="csr image" />
						</span>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>