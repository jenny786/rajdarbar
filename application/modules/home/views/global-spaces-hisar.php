<!--popup box-->
<div class="black11"></div>
<div class="popbox"><div class="popclose">X</div>
	<!--<h2>Global Foyer Golf Course Road: Key Highlights</h2>-->
	<ul>
		<li>24hr taxi stand</li>
		<li>Ample parking space</li>
		<li>healthcare centres</li>
		<li>primary schools &amp; nursery schools</li>
		<li>Children play area</li>
		<li>Multipurpose hall</li>
		<li>Main entrance with round the clock security</li>
		<li>24/7 CC TV surveillance</li>
		<li>Fully developed health club</li>
		<li>Amphitheater &amp; club</li>
	</ul>
</div>
<!--end popup-->

<div class="banner inner wow fadeInDown">

	<div class="banner-slider owl-carousel owl-theme">
		<div class="item">
			<div class="banner-part" style="background-image: url(<?php echo base_url();?>public/front/images/h-slider-12.jpg);">
				<img src="<?php echo base_url();?>public/front/images/h-slider-12.jpg" alt="inner banner" style="display: none;" />
			</div>
		</div>
<!--
		<div class="item">
			<div class="banner-part" style="background-image: url(<?php //echo base_url();?>public/front/images/h-slider-10.jpg);">
				<img src="<?php //echo base_url();?>public/front/images/h-slider-10.jpg" alt="inner banner" style="display: none;" />
			</div>
		</div>
-->
		<div class="item">
			<div class="banner-part" style="background-image: url(<?php echo base_url();?>public/front/images/h-slider-11.jpg);">
				<img src="<?php echo base_url();?>public/front/images/h-slider-11.jpg" alt="inner banner" style="display: none;" />
			</div>
		</div>
		<div class="item">
			<div class="banner-part" style="background-image: url(<?php echo base_url();?>public/front/images/h-slider-8.jpg);">
				<img src="<?php echo base_url();?>public/front/images/h-slider-8.jpg" alt="inner banner" style="display: none;" />
			</div>
		</div>
	</div>
	
	<div class="rotate-part">
		<div class="container">
			<div class="banner-bottom">
				<div class="spaces pull-left banner-bottom-left wow fadeInLeft" data-wow-delay="100ms">
					<img src="<?php echo base_url();?>public/front/images/residential-spaces.jpg" alt=""/>
				</div>
				<div class="spaces pull-right banner-bottom-right">

					<a class="wow fadeInDown enquiry" data-wow-delay="100ms" href="<?php echo base_url()?>home/contact_us"><img src="<?php echo base_url();?>public/front/images/icon-enquiry1.png" alt=""/> Enquiry</a>
					<a class="wow fadeInDown" data-wow-delay="200ms" target="_blank" href="<?php echo base_url();?>public/front/images/pdf/Global-Spaces-Hisar-Brochure.pdf"><img src="<?php echo base_url();?>public/front/images/eb.png" alt=""/> E-Brochure</a>
				</div>
			</div>
		</div>
	</div>
</div>

	<div class="banner-text inner bre-slider">
		<div class="container">
			<div class="col-lg-12 col-md-12 col-sm-12 banner-text-main">
				<div class="col-lg-4 col-md-4 col-sm-4 banner-text-left">
					<h3 class="wow fadeInDown" data-wow-delay="100ms">Pioneers in modern living</h3>
				</div>
				<div class="col-lg-8 col-md-8 col-sm-8 banner-text-right">
					<div class="bre">
					<ul class="wow fadeInLeft" data-wow-delay="100ms">
						<li><a href="<?php echo base_url()?>">home</a></li>     
						<li>businesses</li>      
						<li>residential</li>      
						<li><span>Hisar</span></li> 
					</ul>
					</div>
				</div>
			</div>
		</div>
	</div>
	
	
	<section class="overview residential">
		<div class="container">
			<div class="col-lg-12 col-md-12 col-sm-12 overview-main">
				<h2 class="wow fadeInDown" data-wow-delay="100ms">Overview</h2>
				<h3 class="wow fadeInDown" data-wow-delay="100ms">Hisar: modern habitats for modern india</h3>
				
				<div class="float_img">
							<img src="<?php echo base_url();?>public/front/images/global-spaces-hisar-th.jpg" alt="Global Spaces Hisar" />
						</div>
				<p class="wow fadeInDown" data-wow-delay="200ms">Welcome to Rajdarbar Spaces Hisar, a fully integrated modern township spread across 65 acres of verdant greens, that has redefined the concept of modern living. Experience the luxury of living in close proximity to nature with lush green landscape, manicured gardens and broad avenues offering an enriched quality of life away from the pollution and congestion of the city, yet close to all the amenities and conveniences of modern lifestyle that will take your breath away.</p>
				<p class="wow fadeInDown" data-wow-delay="100ms">Rajdarbar Spaces is the best township in Hisar, conveniently located near Jindal Chowk right on Delhi-Hisar-Sirsa bypass road, approachable from both Raipur and Mirzapur roads. It is the first modern township situated in one of the best residential sectors in Hisar, adjacent to HUDA plots of Sector 1 and Sector 3. What&rsquo;s more, it is a stone&rsquo;s throw from the new upcoming International Airport which would further enhance its value and locational advantage!</p>
				
				<p class="wow fadeInDown" data-wow-delay="100ms">Within this secure, gated community, you can enjoy an enviable lifestyle along with all the facilities that you can expect in a planned township. With the best properties on offer, Rajdarbar Spaces brings to you a choice of exquisitely planned plots from 150 to 440 square yards, Flats, Independent and Duplex villas. You can also enjoy the convenience of a 24 hour taxi stand, primary schools, two nursery schools and two nursing homes to take care of your family. A sprawling commercial area to meet all your shopping needs and a well appointed clubhouse and amphitheatre ensures that you never have a dull evening!</p>
			</div>
		</div>
	</section>
	
	<section class="plan-feature">
		<div class="container">
			<div class="col-lg-12 col-md-12 col-sm-12 plan-feature-main fix-h">
				<h2 class="wow fadeInDown" data-wow-delay="100ms">Plans &amp; Features</h2>
				<div class="tab-system">

					<button type="button" class="navbar-toggle open">
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span> 
					</button>
					
				  <!-- Nav tabs -->
				  <ul class="nav nav-tabs nav-tabs1 wow fadeInDown" data-wow-delay="100ms" id="product-tabs-area" role="tablist">
				  	<!--<li role="presentation"><a href="#tab1" role="tab" data-toggle="tab">Elevation</a></li>-->
					<li role="presentation" class="active"><a href="#tab2" role="tab" data-toggle="tab">Key Highlight</a></li>
					<li role="presentation"><a href="#tab3" role="tab" data-toggle="tab">Floor Plans</a></li>
					<li role="presentation"><a href="#tab4" role="tab" data-toggle="tab">Specifications</a></li>
				 <?php if(!empty($gallery)){ ?>
					<li role="presentation"><a href="#tab5" role="tab" data-toggle="tab">Photo Gallery</a></li>
				<?php } ?>
					<li role="presentation"><a href="#tab6" role="tab" data-toggle="tab">Site Plan</a></li>
					<li role="presentation"><a href="#tab8" role="tab" data-toggle="tab">Location Map</a></li>
				  </ul>

				  <!-- Tab panes -->
				  <div class="tab-content">
					<div class="tab-pane wow fadeInDown active" data-wow-delay="300ms" id="tab2">
						<div class="key-heighlight">
							<ul class="key-list">
								<li>
									<div class="img">
										<a href="javascript:;">
											<img class="img1" src="<?php echo base_url();?>public/front/images/icon/icon-taxi-stand.png" alt="keyhilight" />
											<img class="img2" src="<?php echo base_url();?>public/front/images/icon/iconw-taxi-stand.png" alt="keyhilight" />
										</a>
									</div>
									<div class="desc">
										<p>24hr Taxi Stand</p>
									</div>
								</li>
								<li>
									<div class="img">
										<a href="javascript:;">
											<img class="img1" src="<?php echo base_url();?>public/front/images/icon/icon-parking.png" alt="keyhilight" />
											<img class="img2" src="<?php echo base_url();?>public/front/images/icon/iconw-parking.png" alt="keyhilight" />
										</a>
									</div>
									<div class="desc">
										<p>Ample Parking Space</p>
									</div>
								</li>
								<li>
									<div class="img">
										<a href="javascript:;">
											<img class="img1" src="<?php echo base_url();?>public/front/images/icon/icon-healthcare-centres.png" alt="keyhilight" />
											<img class="img2" src="<?php echo base_url();?>public/front/images/icon/iconw-healthcare-centres.png" alt="keyhilight" />
										</a>
									</div>
									<div class="desc">
										<p>Healthcare Centres</p>
									</div>
								</li>
								<li>
									<div class="img">
										<a href="javascript:;">
											<img class="img1" src="<?php echo base_url();?>public/front/images/icon/icon-scool.png" alt="keyhilight" />
											<img class="img2" src="<?php echo base_url();?>public/front/images/icon/iconw-scool.png" alt="keyhilight" />
										</a>
									</div>
									<div class="desc">
										<p>Primary Schools &amp; Nursery Schools</p>
									</div>
								</li>
								<li>
									<div class="img">
										<a href="javascript:;">
											<img class="img1" src="<?php echo base_url();?>public/front/images/icon/icon-kids.png" alt="keyhilight" />
											<img class="img2" src="<?php echo base_url();?>public/front/images/icon/iconw-kids.png" alt="keyhilight" />
										</a>
									</div>
									<div class="desc">
										<p>Children Play Area</p>
									</div>
								</li>
								<li>
									<div class="img">
										<a href="javascript:;">
											<img class="img1" src="<?php echo base_url();?>public/front/images/icon/icon-multipurpose-hall.png" alt="keyhilight" />
											<img class="img2" src="<?php echo base_url();?>public/front/images/icon/iconw-multipurpose-hall.png" alt="keyhilight" />
										</a>
									</div>
									<div class="desc">
										<p>Multipurpose Hall</p>
									</div>
								</li>
								<li>
									<div class="img">
										<a href="javascript:;">
											<img class="img1" src="<?php echo base_url();?>public/front/images/icon/icon-protected.png" alt="keyhilight" />
											<img class="img2" src="<?php echo base_url();?>public/front/images/icon/iconw-protected.png" alt="keyhilight" />
										</a>
									</div>
									<div class="desc">
										<p>Main Entrance with round the Clock Security</p>
									</div>
								</li>
								<li>
									<div class="img">
										<a href="javascript:;">
											<img class="img1" src="<?php echo base_url();?>public/front/images/icon/icon-key-highlight-7.png" alt="keyhilight" />
											<img class="img2" src="<?php echo base_url();?>public/front/images/icon/iconw-key-highlight-7.png" alt="keyhilight" />
										</a>
									</div>
									<div class="desc">
										<p>24/7 CC TV Surveillance</p>
									</div>
								</li>
								<li>
									<div class="img">
										<a href="javascript:;">
											<img class="img1" src="<?php echo base_url();?>public/front/images/icon/icon-gym.png" alt="keyhilight" />
											<img class="img2" src="<?php echo base_url();?>public/front/images/icon/iconw-gym.png" alt="keyhilight" />
										</a>
									</div>
									<div class="desc">
										<p>Fully Developed Health Club</p>
									</div>
								</li>
								<li>
									<div class="img">
										<a href="javascript:;">
											<img class="img1" src="<?php echo base_url();?>public/front/images/icon/icon-amphitheater-&-club.png" alt="keyhilight" />
											<img class="img2" src="<?php echo base_url();?>public/front/images/icon/iconw-amphitheater-&-club.png" alt="keyhilight" />
										</a>
									</div>
									<div class="desc">
										<p>Amphitheater &amp; Club</p>
									</div>
								</li>
							</ul>
							<p class="more-text">For Detailed Key Highlights Please <a href="javascript:;" class="more-vv">Click Here</a></p>
						</div>
					</div>

					<div class="tab-pane wow fadeInDown docs-pictures" data-wow-delay="300ms" id="tab3">
					
						<div class="floorplan hisar">
							<div id="floorplan-slider" class="owl-carousel owl-theme">
							    <div class="item">
							    	<div class="box">
								    	
										<div class="img">
											<span>
												<img data-original="<?php echo base_url();?>public/front/images/floor-plan-h-1.jpg" src="<?php echo base_url();?>public/front/images/floor-plan-h-th-1.jpg" alt="floor-plan" />
											</span>
										</div>
										<div class="buttons">
											<!--<a href="javascript:;" class="btn">Ground Floor Plan</a>-->
											<p class="btn">Ground Floor Plan</p>
										</div>
									</div>
							    </div>
							     <div class="item">
							    	<div class="box">
								    	
										<div class="img">
											<span>
												<img data-original="<?php echo base_url();?>public/front/images/floor-plan-h-2.jpg" src="<?php echo base_url();?>public/front/images/floor-plan-h-th-2.jpg" alt="floor-plan" />
											</span>
										</div>
										<div class="buttons">
											<!--<a href="javascript:;" class="btn">First Floor Plan</a>-->
											<p class="btn">First Floor Plan</p>
										</div>
									</div>
							    </div>
							</div>
						</div>									
					</div>

					<div class="tab-pane wow fadeInDown" data-wow-delay="300ms" id="tab4">
					
						<div class="tab-pane wow fadeInDown" data-wow-delay="300ms" id="tab3">
							<div class="key-heighlight min-height">
								<p>On Request</p>
							</div>
						</div>	
					</div>

					<div class="tab-pane wow fadeInDown" data-wow-delay="300ms" id="tab5">
						<div class="elev-slider owl-carousel owl-theme wow fadeInUp">
									
						 <?php if(!empty($gallery)) { foreach ($gallery as $key => $value) { ?>
								<div class="item">
									<div class="shops-text">
									<img src="<?php echo base_url('images/project_gallery/'.$value->image);?>" alt="gurgaon" />	
								</div>
								</div>
						<?php } } ?>
					
						</div>
					</div>

					<div class="tab-pane wow fadeInDown docs-pictures" data-wow-delay="300ms" id="tab6">
						<div class="Walkthrough siteplan">
							<span><img data-original="<?php echo base_url();?>public/front/images/sp-hisar-bg.jpg" src="<?php echo base_url();?>public/front/images/sp-hisar-th.jpg" alt="Location" /></span>
							<!--<div class="site-disc">
								<p>Add Disclaimer here</p>
							</div>-->
						</div>	
						
					</div>
					<div class="tab-pane1" id="tab8">
						<iframe src="https://www.google.co.in/maps/d/embed?mid=1bTBnEsRIgYGlw4I2tp9WJ4_zKRk1TXGX" width="600" height="450" style="border:0"></iframe>
					</div>
				  </div>
				</div>
			</div>
		</div>
	</section>
	
	<section class="downloads">
		<div class="container">
			<div class="col-lg-12 col-md-12 col-sm-12 downloads-main">
				<h2 class="wow fadeInDown" data-wow-delay="100ms">Downloads</h2>
				<div class="downloads-icon">
					<div class="downloads-icon-text">
						<a class="one-a" target="_blank" href="<?php echo base_url();?>public/front/images/pdf/Global-Spaces-Hisar-Brochure.pdf"><p class="wow fadeInLeft" data-wow-delay="100ms">E-Brochure</p></a>					
					</div>
					<div class="downloads-icon-text">
						<a class="one-b" target="_blank" href="<?php echo base_url();?>public/front/images/pdf/Application-form_Hisar.pdf"><p class="wow fadeInLeft" data-wow-delay="200ms">Application Form</p></a>				
					</div>
					<div class="downloads-icon-text">
						<a class="one-c one-e" target="_blank" href="javascript:;"><p class="wow fadeInLeft" data-wow-delay="300ms">Payment Plan</p></a>				
					</div>
					<div class="downloads-icon-text">
						<a href="javascript:;" class="one-d one-e"><p data-wow-delay="400ms" class="wow fadeInLeft">Price List</p></a>				
					</div>
				</div>
			</div>
		</div>
	</section>
	
	<section class="loans">
		<div class="container">
			<div class="col-lg-12 col-md-12 col-sm-12 loans-main">
				<h2 class="wow fadeInDown" data-wow-delay="100ms">Home Loans Available By</h2>
				<div class="loans-slider owl-carousel owl-theme wow fadeInDown" data-wow-delay="300ms">
					<div class="item">
						<div class="loans-text">
							<img src="<?php echo base_url();?>public/front/images/bank1.jpg" alt=""/>
						</div>
					</div>
					<div class="item">
						<div class="loans-text">
							<img src="<?php echo base_url();?>public/front/images/bank-lic.jpg" alt=""/>
						</div>
					</div>
					<div class="item">
						<div class="loans-text">
							<img src="<?php echo base_url();?>public/front/images/bank-pnb.jpg" alt=""/>
						</div>
					</div>
					<div class="item">
						<div class="loans-text">
							<img src="<?php echo base_url();?>public/front/images/bank4.jpg" alt=""/>
						</div>
					</div>
				</div>	
			</div>
		</div>
	</section>
	
	
	<section class="location-map">
		<div class="container">
			<div class="map-area">
				<h2 class="wow fadeInDown" data-wow-delay="100ms">Location</h2>
				<ul class="location-map-main">
					<li>
						<h3 class="wow fadeInDown" data-wow-delay="200ms">Corporate Office</h3>
						<p class="wow fadeInDown" data-wow-delay="300ms">303, 3rd Floor, Global Foyer, Golf Course Road, <br>Sector-43, Gurgaon -122002, (Haryana) India</p>
						<p class="wow fadeInDown" data-wow-delay="100ms">
							<span>Email :</span> 
							<a href="mailto:info@globalrealty.com">info@rajdarbarrealty.com</a>
							<a href="mailto:customercare@globalrealty.com">customercare@rajdarbarrealty.com</a>
							<a href="mailto:sales@globalrealty.com">sales@rajdarbarrealty.com</a>
						</p>
						<p class="wow fadeInDown" data-wow-delay="400ms"><span></span> <a href="javascript:;"> </a> <a href="javascript:;"></a></p>
					</li>
					<li>
						<h3 class="wow fadeInDown" data-wow-delay="200ms">Site Office</h3>
						<p class="wow fadeInDown" data-wow-delay="300ms">Sector - 24, 7th KM Stone, Hisar - Delhi By Pass, <br>Hisar - 125051 (Haryana)</p>
						<p class="wow fadeInDown" data-wow-delay="100ms">
							<span></span> 
							<a href="javascript:;"></a>
							<a href="javascript:;"></a>
							<a href="javascript:;"></a>
						</p>
						<p class="wow fadeInDown" data-wow-delay="400ms"><span>Tel :</span> <a href="callto:919991888940">+91 9991888940 </a> <a href="javascript:;"></a></p>
					</li>
				</ul>
				<div class="map-location wow fadeInDown google-map-wrap" >
					<div class="map">
						<iframe class="g-map" src="https://www.google.com/maps/d/embed?mid=1wcLmJzu5etrXpkSCOoaHb89jwvmZxK5r" width="640" height="480"></iframe>
					</div>
				</div>
			</div>
		</div>
		
	</section>