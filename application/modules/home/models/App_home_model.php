<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class App_home_model extends CI_Model{
	
	function __construct() {
        parent::__construct();
        
    }
	protected $tbl_career_enquiry = 'tbl_career_enquiry';
	
	public function insertCareerEnquiry($post_value){
		if(!empty($post_value)){
			$this->db->insert($this->tbl_career_enquiry, $post_value);
			$inserted_id =$this->db->insert_id();
			if($inserted_id){			
				$return['status'] = 'true';
				$return['inserted_id'] = $inserted_id;
				$return['message'] = "Record added successfully.";				
			}else{
				$return['status'] = 'false';
				$return['message'] = "Somthing went wrong.";
			}
			
			return $return;
		}
	}
	
	/* Global News List Start */
	
	/*public function getWhatsNew(){
	$this->db->select('*');
    $this->db->from('tbl_whatsnew');
	$this->db->where('is_active', '1');
    //$this->db->join('tbl_news', 'tbl_news.news_id = tbl_news_management.id');
	$this->db->order_by('id', 'DESC');
	$query = $this->db->get();
	if($query->num_rows()>0)
	{
		$data = $query->result_array();
		return $data;
	}
	return false; 
	}*/
	


	public function getMedia() {		
        $this->db->select("*");
        $this->db->from('tbl_media');
        $this->db->order_by('id', 'DESC');
        return $this->db->get()->result();     
    }
	


	public function getProjectGallery($projectID) {		
        $this->db->select("*");
        $this->db->from('tbl_project_gallery');
        $this->db->where('project_id',$projectID);
        $this->db->order_by('id', 'DESC');
        return $this->db->get()->result();     
    }
	



}
