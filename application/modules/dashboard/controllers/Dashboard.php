<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {
	
	function __construct() {
        parent::__construct();
        if(checkSessionData() == false)
	    { 
	      redirect('admin'); 
	    }
    }


	public function index()
	{	
		$data['page_title'] = 'Dashboard';
		$data['main_content'] = 'dashboard/dashboard';
		$this->load->view('admin/layout', $data);
	}
	
	
	
}
