<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Career extends CI_Controller {
	
	function __construct() {
        parent::__construct();
		
        if(checkSessionData() == false)
	    { 
	      redirect('admin'); 
	    }	
    }
	
	public function ajax_careerstatus() {
	$return = array();
	$return['csrfvalue'] = $this->security->get_csrf_hash(); 	
	$this->load->model('career/App_career_model');
	$post_value = $this->input->post();
    $result = $this->App_career_model->status_active_inacive($post_value);
    $html = '';
		if($result['status']==1){
		$title = "Active";
		$text = "Active";
		$class = "label-success statuslabel label label-default activeDeactive Changecsrf";
		$status = "1";
			 
		}else{
		$title = "De-Active";
		$text = "De-Active";
		$class = "label-danger statuslabel label label-default activeDeactive Changecsrf";	
		$status = "0";	
		}
		$html ='<a title="'.$title.'"  href="javascript:void(0);" record_id="'.$_POST['record_id'].'" record_status="'.$status.'" data-csrf="'.$this->security->get_csrf_hash().'" class="'.$class.'">'.$text.' </a>';
		$return['html'] = $html;
		echo json_encode($return);    

    
    }
	
	public function editCareer($id=false){
		 
		$this->load->model('career/App_career_model');
		$user_id = $this->session->userdata('homzz_user_id');
		$this->form_validation->set_error_delimiters('<div class="error">', '</div>');
		 if($this->input->post()) {
				    
					$static_pageData2 =modifyUserIpDetail($user_id);
					$static_pageData1 = $this->input->post();
					$static_pageData = array_merge($static_pageData1, $static_pageData2);
					$updateStatus = $this->App_career_model->updatecareer_page($static_pageData,$id);
					if ($updateStatus) {
			$this->session->set_flashdata('success_message','updated sucessfully');
						redirect('career');
						exit();
					} else {
						$this->session->set_flashdata('error_message','error_message');
						redirect('career');
					}
		}  else {
					
					$result_module = $this->App_career_model->fetchEditCareer($id);
					if($result_module){
						//pr($result_module); exit;
						$data['fetchmodule'] = $result_module;
					}else{
						$this->session->set_flashdata('error_message','error_message');
						redirect('career');
					}
					
					$data['main_content'] = 'career/editCareer';
					$this->load->view('admin/layout', $data);
				}
	}
	
	public function enquiry()
	{
		$this->load->model('career/App_career_model');
		$data['career_list'] = $this->App_career_model->getCareerEnquiry();
		$data['main_content'] = 'career/career_enquiry';
		$this->load->view('admin/layout',$data);
	}
	
	public function showcareer($id)
	{
		$this->load->model('career/App_career_model');
		$data['career_list'] = $this->App_career_model->getCareerEnquiry($id);
		$data['main_content'] = 'career/career_enquiry_detail';
		$this->load->view('admin/layout',$data);
	}
	
	public function deleteCareerenquiry($id)
	{
		            $this->load->model('career/App_career_model');
					$this->App_career_model->deletecareerenquiry($id);
					$this->session->set_flashdata('success_message','deleted sucessfully');
					redirect('career/enquiry/');
					exit();
			
	}
	
	
	
	
	/*=== for unique Slug name ===================== */
	
	
}
