<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Enquiry_model extends CI_Model{
	
	function __construct() {
        parent::__construct();
        
    }
	
	public function getAllEnquiry() {
		$this->db->select("*");
        $this->db->from('tbl_contact');
        $this->db->order_by('id', 'DESC');
		return $this->db->get()->result();
             
    }
	
	public function getAllHomeEnquiry() {
		$this->db->select("*");
        $this->db->from('tbl_home_enquiry');
        $this->db->order_by('id', 'DESC');
		return $this->db->get()->result();
             
    }
	
	

	
}
