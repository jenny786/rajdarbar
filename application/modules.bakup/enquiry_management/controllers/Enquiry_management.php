<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Enquiry_management extends CI_Controller {
	
	public function __construct() {
        parent::__construct();
		$this->load->model('enquiry_model');
        if(checkSessionData() == false)
	    { 
	      redirect('admin'); 
	    }
    }


	function index(){ 	   
		$data['contact'] = $this->enquiry_model->getAllEnquiry();
		$data['page_title'] = 'Left Enquiry';
		$data['main_content'] = 'contact';
		$this->load->view('admin/layout', $data);	
	}
	
	function delete_record_by_id($encrypted_id){
		$combneid = decode_id_url($encrypted_id);
		$record_id = $combneid[1];
		$session_id = session_id();
		$get_session_id = $combneid[0];
		if($get_session_id == $session_id){
			if($combneid[1] !='' ){
				$record_id = $combneid[1];
				$rtrn = deleteRecord( 'tbl_contact', array('id'=>$record_id) );
				if($rtrn){
					$this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button><strong>Success ! &nbsp;&nbsp;</strong>Record deleted successfully</div>');
					redirect($this->uri->segment(1));
				}
			}else{
				setErrorFlashData('Something went wrong please try again.');
				redirect($this->uri->segment(1));
			}
		}else{
			setErrorFlashData('This request is not valid for current user.');
			redirect($this->uri->segment(1));
		}
	}
	
	function delete_record_by_id_home_enquiry($encrypted_id){
		$combneid = decode_id_url($encrypted_id);
		$record_id = $combneid[1];
		$session_id = session_id();
		$get_session_id = $combneid[0];
		if($get_session_id == $session_id){
			if($combneid[1] !='' ){
				$record_id = $combneid[1];
				$rtrn = deleteRecord( 'tbl_home_enquiry', array('id'=>$record_id) );
				if($rtrn){
					$this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button><strong>Success ! &nbsp;&nbsp;</strong>Record deleted successfully</div>');
					redirect('enquiry_management/home_enquiry');
				}
			}else{
				setErrorFlashData('Something went wrong please try again.');
				redirect('enquiry_management/home_enquiry');
			}
		}else{
			setErrorFlashData('This request is not valid for current user.');
			redirect('enquiry_management/home_enquiry');
		}
	}
	
	
	
	
	public function home_enquiry()
	{
		$data['contact'] = $this->enquiry_model->getAllHomeEnquiry();
		$data['page_title'] = 'Left Enquiry';
		$data['main_content'] = 'enquiry';
		$this->load->view('admin/layout', $data);	
	}
	
}
