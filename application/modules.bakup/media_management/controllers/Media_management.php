<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Media_management extends CI_Controller {
	
	public function __construct() {
        parent::__construct();
		$this->load->model('App_media_model');
        if(checkSessionData() == false)
	    { 
	      redirect('admin'); 
	    }
    }



	function index(){ 	   
		$data['recordAll'] = $this->App_media_model->getMedia();
		$data['page_title'] = 'Media Management';
		$data['main_content'] = 'index';
		$this->load->view('admin/layout', $data);	
	}
	
	
	function media($page="" , $id=""){
		
				switch ($page) {
					case 'create':

				    $this->load->library('form_validation');
					$this->form_validation->set_rules('name', 'Enter Title', 'required|encode_php_tags|xss_clean');
					$this->form_validation->set_rules('dated', 'Please select date', 'required|encode_php_tags|xss_clean');
				
					$this->form_validation->set_error_delimiters('<div class="error">', '</div>');
					
					if ($this->form_validation->run() == TRUE){
						if(isset($_POST['submitBTN'])){
						unset($_POST['submitBTN']);
						$post_value = $this->input->post();
					
			         if(!empty($_FILES['img']['name'])){
			                $config['upload_path'] = 'images/media/';
			                $config['allowed_types'] = 'jpg|png|jpeg|gif|';
							$config['file_name'] = time() . date('Ymd');
							$this->load->library('upload');
							$this->upload->initialize($config);				
			                if($this->upload->do_upload('img')){
			                    $uploadData = $this->upload->data();
								$post_value['image'] = $uploadData['file_name'];
			                }else{ 
								$this->session->set_flashdata('error_image', $this->upload->display_errors());
								redirect('media_management/media/create');
							} 
			            }else{
			                $post_value['image'] = '';
			            }


			        	$rtrn = addRecord('tbl_media', $post_value);
					
						if( $rtrn > 0 ){
							setSuccessFlashData('File added successfully');
							redirect('media_management/media');
						}else{
							setErrorFlashData('Something goes wrong, Please try again');
							redirect('media_management/media');
						}	
			        } } else {
					//echo "<pre>"; pr($data); exit;
			      	
					    $data['page_title'] = 'Add Media';
						$data['main_content'] = 'galleryadd';
						$this->load->view('admin/layout', $data);


					}


				   break;

		           case 'update':
		            
			         $data['rtrnData'] = getRecordById('tbl_media', array('id'=>$id) );

		             $this->load->library('form_validation');
					$this->form_validation->set_rules('name', 'Enter Title', 'required|encode_php_tags|xss_clean');
				
					$this->form_validation->set_error_delimiters('<div class="error">', '</div>');
					
					if ($this->form_validation->run() == TRUE){
						if(isset($_POST['submitBTN'])){
						unset($_POST['submitBTN']);
						$post_value = $this->input->post();
					
			         if(!empty($_FILES['img']['name'])){
			                $config['upload_path'] = 'images/media/';
			                $config['allowed_types'] = 'jpg|png|jpeg|gif|';
							$config['file_name'] = time() . date('Ymd');
							$this->load->library('upload');
							$this->upload->initialize($config);				
			                if($this->upload->do_upload('img')){
			                    $uploadData = $this->upload->data();
								$post_value['image'] = $uploadData['file_name'];
			                }else{ 
								$this->session->set_flashdata('error_image', $this->upload->display_errors());
								redirect('media_management/media/update/'.$id);
							} 
			            }else{
			                $post_value['image'] = $data['rtrnData']->image;
			            }
                      
			            $rtrn = updateRecord('tbl_media', array('id'=>$id), $post_value);
						if( $rtrn > 0 ){
							setSuccessFlashData('File added successfully');
							redirect('media_management/media');
						}else{
							setErrorFlashData('Something goes wrong, Please try again');
							redirect('media_management/media');
						}	
			        } } else {
					//echo "<pre>"; pr($data); exit;
                   
                  
						$data['page_title'] = 'Update Media';
						$data['main_content'] = 'galleryadd';
						$this->load->view('admin/layout', $data);

					}

		         
			       break;

			    case 'status':
			          $get_status = updateRecordStatus('media_gallery', $_POST);
			          $return = array("status"=>"true","message"=>"status changesuccessfully.");
			          echo json_encode($return);
			       	break;	
		         case 'delete':
		             $rtrn = deleteRecord( 'tbl_media', array('id'=>$id) );
						if($rtrn){
							setSuccessFlashData('Record deleted successfully');
							redirect('media_management/media');
					}
		         	break;

				 default:
					$data['recordAll'] = $this->App_media_model->getMedia();
					$data['page_title'] = 'Media Management';
					$data['main_content'] = 'index';
					$this->load->view('admin/layout', $data);	
					break;
				}
				
		    }
		
}
