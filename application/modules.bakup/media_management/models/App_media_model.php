<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class App_media_model extends CI_Model{
	
	function __construct() {
        parent::__construct();
        
    }
	
	public function getMedia() {		
        $this->db->select("*");
        $this->db->from('tbl_media');
        $this->db->order_by('id', 'DESC');
        return $this->db->get()->result();     
    }
	

}
