
<div class="banner inner-b wow fadeInDown">
	<div class="banner-part1" style="background-image: url(<?php echo base_url();?>public/front/images/banner-vision-mission.jpg);">
		<img style="display: none;" src="<?php echo base_url();?>public/front/images/banner-vision-mission.jpg" alt=""/>
	</div>
</div>
<div class="banner-text inner wow fadeInDown" data-wow-delay="100ms">
	<div class="container">
		<div class="col-lg-12 col-md-12 col-sm-12 banner-text-main">
			<div class="col-lg-3 col-md-3 col-sm-3 banner-text-left">
				<h3 class="wow fadeInDown" data-wow-delay="200ms">Mission &amp; Visions</h3>
			</div>
			<div class="col-lg-9 col-md-9 col-sm-9 banner-text-right">
				<div class="bre">
				<ul class="wow fadeInDown" data-wow-delay="300ms">
					<li><a href="<?php echo base_url()?>">home</a></li>
					<li><a href="javascript:;">corporate</a></li>          
					<li><span>mission &amp; visions</span></li> 
				</ul>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="mission-area">
	<div class="container">
		<ul class="mission-box">
			<li class="mission">
				<div class="box pull-left">
					<h2>Our Mission</h2>
					<p>To make India a royal place to live , work and play . To regain our heritage of noble lifestyles in a most inclusive manner.</p>		
				</div>
			</li>
			<li class="vision">
				<div class="box pull-right">
					<h2>Vision</h2>
					<p>Our vision is to achieve perfection in all our endeavours in India and abroad. We strive towards ensuring quality and excellence, Commitment and Integrity Always keeping in mind our commitment to create value for our customers, investors &amp; associates.</p>		
				</div>
			</li>
		</ul>
		<div class="global_value">
			<h2><span>Rajdarbar Values</span></h2>
			<ul>
				<li>
					<div class="box one">
						<h3>We Deliver what Promise</h3>
						<p>We value a deep sense of responsibility and self discipline, to not only meet but to surpass the commitments made.</p>
					</div>
				</li>
				<li>
					<div class="box two">
						<h3>We Never compromise in integrity</h3>
						<p>We gain your trust the old fashion way ....we earn it ! We believe in developing trust within the people associated with us, like our customers, suppliers and partners. We consistently demonstrate integrity and fairness not only through our words, but through our actions as well.</p>
					</div>
				</li>
				<li>
					<div class="box three">
						<h3>We have a Passion for excellence</h3>
						<p>We strive to attain excellence, both for individuals and teams through continuous learning and by setting high standards for ourselves.</p>
					</div>
				</li>
				<li>
					<div class="box four">
						<h3>We will Always be socially responsible</h3>
						<p>An essential component of our corporate social responsibility is to care for the community. We wish to make a positive contribution to the underprivileged communities by supporting a wide range of socio-economic, educational and health initiatives.</p>
					</div>
				</li>
			</ul>
		</div>
	</div>
</div>