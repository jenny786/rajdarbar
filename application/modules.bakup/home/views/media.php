<div class="banner inner-b wow fadeInDown">
	<div class="banner-part1" style="background-image: url(<?php echo base_url();?>public/front/images/banner_media-n.jpg);">
		<img style="display: none;" src="<?php echo base_url();?>public/front/images/banner_media-n.jpg" alt=""/>
	</div>
</div>
<div class="banner-text inner wow fadeInDown" data-wow-delay="100ms">
	<div class="container">
		<div class="col-lg-12 col-md-12 col-sm-12 banner-text-main">
			<div class="col-lg-3 col-md-3 col-sm-3 banner-text-left">
				<h3 class="wow fadeInDown" data-wow-delay="200ms">Media</h3>
			</div>
			<div class="col-lg-9 col-md-9 col-sm-9 banner-text-right">
				<div class="bre">
				<ul class="wow fadeInDown" data-wow-delay="300ms">
					<li><a href="<?php echo base_url()?>">home</a></li>         
					<li><span>media</span></li> 
				</ul>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="media-area">
	<div class="container">
		<div class="media-list">
			<ul>
				<li>
					<div class="box">
						<div class="img">
							<img src="<?php echo base_url();?>public/front/images/media-img-13.jpg" alt="" />
						</div>
						<div class="info">
							<div class="title">31st December,  2017</div>
							<div class="desc">&lsquo;Smile @ 60+&rsquo;, 2017 an event under our Senior Citizen Vertical</div>
							
						</div>
					</div>
				</li>
				<li>
					<div class="box">
						<div class="img">
							<img src="<?php echo base_url();?>public/front/images/media-img-3.jpg" alt="" />
						</div>
						<div class="info">
							<div class="title">11th October, 2017</div>
							<div class="desc">&lsquo;BETIYAAN&rsquo;, 2017 an event under our Think Girl Child Vertical</div>
							
						</div>
					</div>
				</li>
				<li>
					<div class="box">
						<div class="img">
							<img src="<?php echo base_url();?>public/front/images/media-img-4.jpg" alt="" />
						</div>
						<div class="info">
							<div class="title">5th June,2017</div>
							<div class="desc">World Environment Day Event, 2017</div>
							
						</div>
					</div>
				</li>
				<li>
					<div class="box">
						<div class="img">
							<img src="<?php echo base_url();?>public/front/images/media-img-5.jpg" alt="" />
						</div>
						<div class="info">
							<div class="title">25th Feb, 2017</div>
							<div class="desc">&lsquo;ANHAD&rsquo;, 2017 an event under our Think for Blind Vertical </div>
							
						</div>
					</div>
				</li>
				<li>
					<div class="box">
						<div class="img">
							<img src="<?php echo base_url();?>public/front/images/media-img-2.jpg" alt="" />
						</div>
						<div class="info">
							<div class="title">31st December, 2016</div>
							<div class="desc">&lsquo;Smile @ 60+&rsquo;, 2016 an event under our Senior Citizen Vertical</div>
							
						</div>
					</div>
				</li>
				<li>
					<div class="box">
						<div class="img">
							<img src="<?php echo base_url();?>public/front/images/media-img-6.jpg" alt="" />
						</div>
						<div class="info">
							<div class="title">16th October, 2016</div>
							<div class="desc">BETIYAAN, 2016 an event under our Think Girl Child Vertical</div>
							
						</div>
					</div>
				</li>
				<li>
					<div class="box">
						<div class="img">
							<img src="<?php echo base_url();?>public/front/images/media-img-7.jpg" alt="" />
						</div>
						<div class="info">
							<div class="title">2nd April, 2016</div>
							<div class="desc">&lsquo;PARINAY MAHAKUMBH&rsquo; an event under our Think Tribes  Vertical </div>
							
						</div>
					</div>
				</li>
				<li>
					<div class="box">
						<div class="img">
							<img src="<?php echo base_url();?>public/front/images/media-img-8.jpg" alt="" />
						</div>
						<div class="info">
							<div class="title">May, 2016</div>
							<div class="desc">Awareness Drive for environment protection started form </div>
							
						</div>
					</div>
				</li>
				<li>
					<div class="box">
						<div class="img">
							<img src="<?php echo base_url();?>public/front/images/media-img-9.jpg" alt="" />
						</div>
						<div class="info">
							<div class="title">5th June,2016</div>
							<div class="desc">World Environment Day Event, 2016</div>
							
						</div>
					</div>
				</li>
				<li>
					<div class="box">
						<div class="img">
							<img src="<?php echo base_url();?>public/front/images/media-img-10.jpg" alt="" />
						</div>
						<div class="info">
							<div class="title">17th July, 2016</div>
							<div class="desc">Plantation drive at Lohvan village Mathura, Uttar Pradesh by India Eye IHRO started on 17th July, 2016</div>
							
						</div>
					</div>
				</li>
			</ul>
		</div>
	</div>
</div>

<div class="marriage-c">
	<div class="container">
		<div class="community">
			<div class="box left">
				<h2 class="title">World record of community marriage created at Tuterkhed in Gujarat</h2>
				<p class="desc">We recently organised world&rsquo;s largest community marriage ceremony for tribal couples at Tuterkhed in the remote Vapi district of Gujarat, where 1121 couples tied the knot under one roof. It was a landmark occasion that was also recorded by the Guinness Book of Records and has been covered by all the leading national and regional news channels and publications.</p>
			</div>
			<div class="box right">
				<div class="img"><img src="<?php echo base_url();?>public/front/images/community-marriage.jpg" alt="" /></div>
			</div>
		</div>
	</div>
</div>

