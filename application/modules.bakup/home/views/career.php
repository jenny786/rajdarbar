<div class="banner inner-b career wow fadeInDown">
	<div class="banner-part1" style="background-image: url(<?php echo base_url();?>public/front/images/career-banner-n.jpg);">
		<img style="display: none;" src="<?php echo base_url();?>public/front/images/career-banner-n.jpg" alt=""/>
	</div>
</div>
<div class="banner-text inner wow fadeInDown" data-wow-delay="100ms">
	<div class="container">
		<div class="col-lg-12 col-md-12 col-sm-12 banner-text-main">
			<div class="col-lg-3 col-md-3 col-sm-3 banner-text-left">
				<h3 class="wow fadeInDown" data-wow-delay="200ms">Career</h3>
			</div>
			<div class="col-lg-9 col-md-9 col-sm-9 banner-text-right">
				<div class="bre">
				<ul class="wow fadeInDown" data-wow-delay="300ms">
					<li><a href="<?php echo base_url()?>">home</a></li>         
					<li><span>career</span></li> 
				</ul>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="career-area">
	<div class="container">
		<div class="top-area">
			<div class="box left">
				<h2>JOIN US FOR A GREAT CAREER</h2>
				<p>The environment at Rajdarbar Limited translates to meaningful work and real growth at every level. Here your hard work is supported by a talented team that helps you reach the desired goals – professional &amp; personal. Our collaborative work style offers the support you need to make an impact on business, while simultaneously developing the career you want through your own efforts. Rajdarbar prides itself as a young and vibrant organisation and recognises its employees as its greatest assets.</p>
				<p>Our success depends on our ability to attract and retain the brightest and most innovative minds, professionals who are up to the challenge of shaping the future success and vision of Rajdarbar Group.</p>
			</div>
			<div class="box right">
				<h2>Submit Your Resume</h2>
				<?php if($this->session->flashdata('success_message')){ ?>
					<div class="alert alert-success alert-dismissable">
						<a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
						<strong>Success!</strong> <?php echo $this->session->flashdata('success_message'); ?>
					</div>
				<?php }else if($this->session->flashdata('error_message')){ ?>
					<div class="alert alert-danger alert-dismissable">
						<a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
						<strong>Oops!</strong> <?php echo $this->session->flashdata('error_message'); ?>
					</div>
				<?php } ?>
				<?php $attributes = array('id' => 'applyform', 'name'=>'applyform');
				echo form_open_multipart('home/apply_now_submit', $attributes); ?>
					<div class="fields">
						<div class="field">
							<div class="input-box">
								<img src="<?php echo base_url();?>public/front/images/icon-user.png" alt="" />
								<input type="text" name="name" id="name" alt="" placeholder="Name *" required="true" /> 
							</div>
							<div class="input-box">
								<img src="<?php echo base_url();?>public/front/images/icon-email.png" alt="" />
								<input type="email" name="email" id="email" alt="" placeholder="Email *" required="true" /> 
							</div>
						</div>
						<div class="field">
							<div class="input-box">
								<img src="<?php echo base_url();?>public/front/images/icon-calender.png" alt="" />
								<input id="datepicker" name="dob" type="text" alt="" required="true" placeholder="Date of Birth (dd-mm-yy)" />
							</div>
							<div class="input-box">
								<img src="<?php echo base_url();?>public/front/images/icon-mobile.png" alt="" />
								<input type="text" name="mobile" id="mobile" alt="" placeholder="Mobile No *" />
							</div>
						</div>

						<div class="field checkbox">
							<div class="input-box">
								<h5 class="title-field">Gender</h5>
								<div class="form-check form-check-inline">
								  <input class="form-check-input" type="radio" name="gender"  value="male">
								  <label class="form-check-label" for="inlineCheckbox1">Male</label>
								</div>
								<div class="form-check form-check-inline">
								  <input class="form-check-input" type="radio" name="gender" value="female">
								  <label class="form-check-label" for="inlineCheckbox2">Female</label>
								</div>
							</div>
						</div>

						<div class="field">
							<div class="input-box">
								<img src="<?php echo base_url();?>public/front/images/icon-qualification.png" alt="" />
								<input type="text" name="subject" id="subject" alt="" placeholder="Subject Line *" />
							</div>
							<div class="input-box">
								<img src="<?php echo base_url();?>public/front/images/icon-apply.png" alt="" />
								<input type="text" name="position" id="position" alt="" placeholder="Position *" />
							</div>
						</div>
						<div class="field msg">
							<div class="input-box">
								<img src="<?php echo base_url();?>public/front/images/icon-location.png" alt="" />
								<textarea name="comment" id="comment" placeholder="Message "></textarea>
							</div>
						</div>
						<div class="field file">
							<div class="input-box">
								<img src="<?php echo base_url();?>public/front/images/icon-upload.png" alt="" />
								<label for="cv" class="custom-file-upload">
									Upload your resume
									<input type="file" name="cv" id="cv" placeholder="Upload your resume" />
								</label>
								<p>Upload your resume (.doc,.docx,.pdf)*</p>
							</div>
						</div>
						<div class="field">
							<div class="input-box">
								<input type="submit" value="Submit" name="Submit" alt="" />
							</div>
						</div>
					</div>
				</form>
			</div>
		</div>
		
		<div class="locations">
			<h2>Open positions</h2>
			<ul>
				<li>
					<div class="box">
						<h3>Sales Manager</h3>
						<a href="javascript:;" class="locate">Gurgaon</a>
					</div>
				</li>

				<li>
					<div class="box">
						<h3>Legal Head</h3>
						<a href="javascript:;" class="locate">Head Office</a>
					</div>
				</li>
				<!--<li>
					<div class="box">
						<h3>Assistant Controller</h3>
						<a href="javascript:;" class="locate">Gurugram</a>
					</div>
				</li>
				<li>
					<div class="box">
						<h3>Senior Product Manager</h3>
						<a href="javascript:;" class="locate">Agra</a>
					</div>
				</li>
-->
			</ul>
		</div>
	</div>
</div>
<script src="<?php echo base_url();?>/public/front/js/jquery.validate.js"></script>
<script type="text/javascript" >

 /****************************************************************************
* Submit the form data 
******************************************************************************/  	
	$.validator.addMethod("lettersonly", function(value, element) {
	return this.optional(element) || /^[a-zA-Z\s]*$/.test(value);
	}, "Letters only please");
	
	$("#applyform").validate({
		errorClass: "validationError",
        rules: {
				name:{
				  required:true,
				  lettersonly: true
				},
				email: {
					required: true,
					email: true
					},
				mobile:"required",
				dob:"required",
				gender:"required",
				subject:"required",
				position:"required",
				cv:"required",
				//captcha_code_reg:"required"
        },
            messages: {
                name: {
					required: "Name is required",
					lettersonly: "Enter Valid Name",
				},
				email:"This field is required.",
				mobile:"This field is required.",
				dob:"This field is required.",
				gender:"This field is required.",
				subject:"This field is required.",
				position:"This field is required.",
				cv:"This field is required."

			 },
		submitHandler: function(form){			
			form.submit();
		}
	});
    
</script>