<!--popup box-->
<div class="black11"></div>
<div class="popbox"><div class="popclose">X</div>
	<!--<h2>Global Foyer Golf Course Road: Key Highlights</h2>-->
	<ul>
		<li>A green environment with landscaped parks and fountains.</li>
		<li>Wide, tarred internal roads lined with trees.</li>
		<li>Solar powered street lighting.</li>
		<li>Convenience shopping outlets within the township.</li>
		<li>Over 1.8 acres of prime commercial space.</li>
		<li>Provision for nursery &amp; primary school.</li>
		<li>24 hours security.</li>
		<li>Uninterrupted water supply.</li>
		<li>Under-ground electrical wiring.</li>
	</ul>
</div>
<!--end popup-->


<div class="banner inner wow fadeInDown">
	<div class="banner-slider owl-carousel owl-theme">
		<div class="item">
			<div class="banner-part" style="background-image: url(<?php echo base_url();?>public/front/images/agra-slider-1.jpg);">
				<img src="<?php echo base_url();?>public/front/images/agra-slider-1.jpg" alt="inner banner" style="display: none;" />
			</div>
		</div>
		<div class="item">
			<div class="banner-part" style="background-image: url(<?php echo base_url();?>public/front/images/agra-slider-2.jpg);">
				<img src="<?php echo base_url();?>public/front/images/agra-slider-2.jpg" alt="inner banner" style="display: none;" />
			</div>
		</div>
		<div class="item">
			<div class="banner-part" style="background-image: url(<?php echo base_url();?>public/front/images/agra-slider-3.jpg);">
				<img src="<?php echo base_url();?>public/front/images/agra-slider-3.jpg" alt="inner banner" style="display: none;" />
			</div>
		</div>
		<div class="item">
			<div class="banner-part" style="background-image: url(<?php echo base_url();?>public/front/images/agra-slider-4.jpg);">
				<img src="<?php echo base_url();?>public/front/images/agra-slider-4.jpg" alt="inner banner" style="display: none;" />
			</div>
		</div>
	</div>
	
	<div class="rotate-part">
		<div class="container">
			<div class="banner-bottom">
				<div class="spaces pull-left banner-bottom-left wow fadeInLeft" data-wow-delay="100ms">
					<img src="<?php echo base_url();?>public/front/images/residential-village.jpg" alt=""/>
				</div>
				<div class="spaces pull-right banner-bottom-right">
					<!--<a class="wow fadeInDown" data-wow-delay="100ms" href="javascript:;"><img src="images/360.png" alt=""/> Virtual Tour</a>-->
					<a class="wow fadeInDown enquiry" data-wow-delay="100ms" href="<?php echo base_url()?>home/contact_us"><img src="<?php echo base_url();?>public/front/images/icon-enquiry1.png" alt=""/> Enquiry</a>
					<a class="wow fadeInDown" data-wow-delay="200ms" target="_blank" href="<?php echo base_url();?>public/front/images/pdf/Agra_Brochure_new.pdf"><img src="<?php echo base_url();?>public/front/images/eb.png" alt=""/> E-Brochure</a>
				</div>
			</div>
		</div>
	</div>
</div>

	<div class="banner-text inner bre-slider">
		<div class="container">
			<div class="col-lg-12 col-md-12 col-sm-12 banner-text-main">
				<div class="col-lg-4 col-md-4 col-sm-4 banner-text-left">
					<h3 class="wow fadeInDown" data-wow-delay="100ms">Agra</h3>
				</div>
				<div class="col-lg-8 col-md-8 col-sm-8 banner-text-right">
					<div class="bre">
					<ul class="wow fadeInLeft" data-wow-delay="100ms">
						<li><a href="<?php echo base_url();?>">home</a></li>     
						<li>businesses</li>      
						<li>residential</li>      
						<li><span>Agra</span></li> 
					</ul>
					</div>
				</div>
			</div>
		</div>
	</div>
	
	
	<section class="overview residential">
		<div class="container">
			<div class="col-lg-12 col-md-12 col-sm-12 overview-main">
				<h2 class="wow fadeInDown" data-wow-delay="100ms">Overview</h2>
				<h3 class="wow fadeInDown" data-wow-delay="100ms">Agra: Regal Living, Royal Life</h3>
				<div class="float_img">
					<img src="<?php echo base_url();?>public/front/images/global-spaces-agra-th.jpg" alt="Global Spaces Agra" />
				</div>
				<p class="wow fadeInDown" data-wow-delay="200ms">Agra, known for the magnificence of its Taj, is also now the home of Royal Class housing. With the inception of Rajdarbar Village at Agra, Rajdarbar Limited has ensured that Agra's illustrious past gets a beautiful future as well!</p>
				<p class="wow fadeInDown" data-wow-delay="100ms">Rajdarbar Village gives its residents luxurious independent row houses just a little away from the bustle of the busy main city. The thoughtful township has all amenities at walking distance. While out for a walk the beautiful landscaping, waterbodies and gardens not only give beautiful company to the resident but also ends up soothing the weary soul!</p>
			</div>
		</div>
	</section>
	
	<section class="plan-feature">
		<div class="container">
			<div class="col-lg-12 col-md-12 col-sm-12 plan-feature-main fix-h">
				<h2 class="wow fadeInDown" data-wow-delay="100ms">Plans &amp; Features</h2>
				<div class="tab-system">

					<button type="button" class="navbar-toggle open">
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span> 
					</button>

				  <!-- Nav tabs -->
				  <ul class="nav nav-tabs nav-tabs1 wow fadeInDown" data-wow-delay="100ms" id="product-tabs-area" role="tablist">
				  	<li role="presentation" class="active"><a href="#tab1" role="tab" data-toggle="tab">Elevation</a></li>
					<li role="presentation"><a href="#tab2" role="tab" data-toggle="tab">Key Highlight</a></li>
					<li role="presentation"><a href="#tab3" role="tab" data-toggle="tab">Floor Plans</a></li>
					<li role="presentation"><a href="#tab4" aria-controls="home" role="tab" data-toggle="tab">Specifications</a></li>
					<li role="presentation"><a href="#tab5" role="tab" data-toggle="tab">Photo Gallery</a></li>
					<li role="presentation"><a href="#tab6" role="tab" data-toggle="tab">Site Plan</a></li>
					<li role="presentation"><a href="#tab8" role="tab" data-toggle="tab">Location Map</a></li>
				  </ul>

				  <!-- Tab panes -->
				  <div class="tab-content">
					<div class="tab-pane wow fadeInDown active" data-wow-delay="300ms" id="tab1">
						<div class="elev-slider owl-carousel owl-theme">
							<div class="item wow fadeInDown" data-wow-delay="130ms">
								<div class="shops-text">
									<img src="<?php echo base_url();?>public/front/images/agra-slider-3.jpg" alt=""/>
								</div>
							</div>
							<div class="item wow fadeInDown" data-wow-delay="130ms">
								<div class="shops-text">
									<img src="<?php echo base_url();?>public/front/images/agra-slider-4.jpg" alt=""/>
								</div>
							</div>
							<div class="item wow fadeInDown" data-wow-delay="130ms">
								<div class="shops-text">
									<img src="<?php echo base_url();?>public/front/images/agra-slider-gl1.jpg" alt=""/>
								</div>
							</div>
							<div class="item wow fadeInDown" data-wow-delay="130ms">
								<div class="shops-text">
									<img src="<?php echo base_url();?>public/front/images/agra-slider-gl2.jpg" alt=""/>
								</div>
							</div>
							<div class="item wow fadeInDown" data-wow-delay="130ms">
								<div class="shops-text">
									<img src="<?php echo base_url();?>public/front/images/agra-slider-gl3.jpg" alt=""/>
								</div>
							</div>
						</div>	
					</div>
					<div class="tab-pane wow fadeInDown" data-wow-delay="300ms" id="tab2">
						<div class="key-heighlight">
							<ul class="key-list">
								<li>
									<div class="img">
										<a href="javascript:;">
											<img class="img1" src="<?php echo base_url();?>public/front/images/icon/icon-park.png" alt="keyhilight" />
											<img class="img2" src="<?php echo base_url();?>public/front/images/icon/iconw-park.png" alt="keyhilight" />
										</a>
									</div>
									<div class="desc">
										<p>A green environment with landscaped parks and fountains.</p>
									</div>
								</li>
								<li>
									<div class="img">
										<a href="javascript:;">
											<img class="img1" src="<?php echo base_url();?>public/front/images/icon/icon-environment.png" alt="keyhilight" />
											<img class="img2" src="<?php echo base_url();?>public/front/images/icon/iconw-environment.png" alt="keyhilight" />
										</a>
									</div>
									<div class="desc">
										<p>Wide, tarred internal roads lined with trees.</p>
									</div>
								</li>
								<li>
									<div class="img">
										<a href="javascript:;">
											<img class="img1" src="<?php echo base_url();?>public/front/images/icon/icon-water-power.png" alt="keyhilight" />
											<img class="img2" src="<?php echo base_url();?>public/front/images/icon/iconw-water-power.png" alt="keyhilight" />
										</a>
									</div>
									<div class="desc">
										<p>Solar powered street lighting.</p>
									</div>
								</li>
								<li>
									<div class="img">
										<a href="javascript:;">
											<img class="img1" src="<?php echo base_url();?>public/front/images/icon/icon-shopping-outlets.png" alt="keyhilight" />
											<img class="img2" src="<?php echo base_url();?>public/front/images/icon/iconw-shopping-outlets.png" alt="keyhilight" />
										</a>
									</div>
									<div class="desc">
										<p>Convenience shopping outlets within the township.</p>
									</div>
								</li>
								<li>
									<div class="img">
										<a href="javascript:;">
											<img class="img1" src="<?php echo base_url();?>public/front/images/icon/icon-acred-township.png" alt="keyhilight" />
											<img class="img2" src="<?php echo base_url();?>public/front/images/icon/iconw-acred-township.png" alt="keyhilight" />
										</a>
									</div>
									<div class="desc">
										<p>Over 1.8 acres of prime commercial space.</p>
									</div>
								</li>
								<li>
									<div class="img">
										<a href="javascript:;">
											<img class="img1" src="<?php echo base_url();?>public/front/images/icon/icon-scool.png" alt="keyhilight" />
											<img class="img2" src="<?php echo base_url();?>public/front/images/icon/iconw-scool.png" alt="keyhilight" />
										</a>
									</div>
									<div class="desc">
										<p>Provision for nursery &amp; primary school.</p>
									</div>
								</li>
								<li>
									<div class="img">
										<a href="javascript:;">
											<img class="img1" src="<?php echo base_url();?>public/front/images/icon/icon-key-highlight-7.png" alt="keyhilight" />
											<img class="img2" src="<?php echo base_url();?>public/front/images/icon/iconw-key-highlight-7.png" alt="keyhilight" />
										</a>
									</div>
									<div class="desc">
										<p>24 hours security.</p>
									</div>
								</li>
								<li>
									<div class="img">
										<a href="javascript:;">
											<img class="img1" src="<?php echo base_url();?>public/front/images/icon/icon-water.png" alt="keyhilight" />
											<img class="img2" src="<?php echo base_url();?>public/front/images/icon/iconw-water.png" alt="keyhilight" />
										</a>
									</div>
									<div class="desc">
										<p>Uninterrupted water supply.</p>
									</div>
								</li>
								<!--<li>
									<div class="img">
										<a href="javascript:;">
											<img class="img1" src="<?php echo base_url();?>public/front/images/icon/icon-electric-wiring.png" alt="keyhilight" />
											<img class="img2" src="<?php echo base_url();?>public/front/images/icon/iconw-electric-wiring.png" alt="keyhilight" />
										</a>
									</div>
									<div class="desc">
										<p>Under-ground electrical wiring.</p>
									</div>
								</li>-->
							</ul>
							<p class="more-text">For Detailed Key Highlights Please <a href="javascript:;" class="more-vv">Click Here</a></p>
						</div>
					</div>
					<div class="tab-pane wow fadeInDown" data-wow-delay="300ms" id="tab3">
					
						<div class="key-heighlight min-height">
							<p>On Request</p>
						</div>
					</div>
					<div class="tab-pane wow fadeInDown" data-wow-delay="300ms" id="tab4">
						<div id="spec-slider" class="owl-carousel owl-theme">
						    <div class="item">
						    	<div class="tab-text-part">
									<h3><img src="<?php echo base_url();?>public/front/images/icon1.png" alt=""/> STRUCTURE</h3>
									<p><span>Mixed</span> (RCC Framed &amp; Load Bearing).</p>
									
								</div>
						    </div>
						    <div class="item">
						    	<div class="tab-text-part">
									<h3><img src="<?php echo base_url();?>public/front/images/icon1.png" alt=""/> FLOORING</h3>
									<p><span>Living/Dining:</span> vitrified tiles (city/asian)</p>
									<p><span>Master Bedroom:</span> wooden laminate</p>
									<p><span>Guest/Kids Bedroom:</span> vitrified tiles (city/asian)</p>
									<p><span>Kitchen:</span> vitrified tiles &amp; granite counter</p>
									<p><span>Toilets:</span> anti skid tiles</p>
									<p><span>Stairs:</span> composite flooring</p>
									<p><span>Couryard/Driveway:</span> kota stone/tiles</p>
								</div>
						    </div>
						    <div class="item">
						    	<div class="tab-text-part wall-icon">
									<h3><img src="<?php echo base_url();?>public/front/images/icon2.png" alt=""/> WALLS &amp; Ceiling Finishes</h3>
									<p><span>Living/Dining:</span> plastic emulsion paint (Asian / Berger)</p>
									<p><span>Bedrooms:</span> plastic emulsion paint (Asian / Berger)</p>	
									<p><span>All Ceilings:</span> Oil Bound Distemper</p>
									<p><span>Kitchen:</span> plastic emulsion paint (Asian / Berger) &amp; ceramic tiles dado</p>
									<p><span>Toilets:</span> glazed/ceramic tiles up to 7 ft height</p>
									<p><span>Stairs:</span> Oil Bound Distemper</p>
									<p><span>External Walls:</span> weather proof exterior emulsion paint (Asian / Berger)</p>
								</div>
						    </div>
						    <div class="item">
						    	<div class="tab-text-part">
									<h3><img src="<?php echo base_url();?>public/front/images/icon3.png" alt=""/> Door</h3>
									<p><span>Main Entrance Door:</span> modular door - factory finish</p>
									<p><span>Toilets Doors:</span> water-proof commercial flush doors</p>
									<p><span>All Other Doors:</span> factory finish</p>
									<p><span>Windows:</span> sal wood frames with grill and glass</p>
								</div>
						    </div>
						    <div class="item">
						    	<div class="tab-text-part">
									<h3><img src="<?php echo base_url();?>public/front/images/icon3.png" alt=""/> Other Fixtures</h3>
									<p><span>Electricals:</span> complete electrical wiring with conduits for light &amp; power points. Includes modular switches &amp; distribution board</p>
									<p><span>Toilet:</span> complete bath fittings including wash-basin, shower &amp; mirror</p>
									<p><span>Kitchen:</span> sink with drain board</p>
									<p><span>Water supply:</span> concealed ppr/upvc waterlines. Individual water tanks on terrace (1000 ltrs capacity)</p>
								</div>
						    </div>
						</div>
					</div>
					<div class="tab-pane wow fadeInDown" data-wow-delay="300ms" id="tab5">
						<!--<p style="text-align: center; color:#fff; font-size: 20px; margin-top: 40px;">No Images Available</p>-->
						<div class="elev-slider owl-carousel owl-theme wow fadeInUp">
							<div class="item">
								<div class="shops-text">
									<img src="<?php echo base_url();?>public/front/images/agra-slider-gl3.jpg" alt="gurgaon" />
								</div>
							</div>
							<div class="item">
								<div class="shops-text">
									<img src="<?php echo base_url();?>public/front/images/agra-slider-gl1.jpg" alt="gurgaon" />
								</div>
							</div>
							<div class="item">
								<div class="shops-text">
									<img src="<?php echo base_url();?>public/front/images/agra-slider-gl2.jpg" alt="gurgaon" />
								</div>
							</div>
							<div class="item">
								<div class="shops-text">
									<img src="<?php echo base_url();?>public/front/images/agra-slider-gl4.jpg" alt="gurgaon" />
								</div>
							</div>
						</div>
					</div>
					<div class="tab-pane wow fadeInDown docs-pictures" data-wow-delay="300ms" id="tab6">
						<div class="Walkthrough">
							<span><img data-original="<?php echo base_url();?>public/front/images/sp-agra-bg.jpg" src="<?php echo base_url();?>public/front/images/sp-agra-th.jpg" alt="Location" /></span>
						</div>	
					</div>
					<div class="tab-pane1" id="tab8">
						<!--<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d321130.6216676858!2d77.81721349739!3d27.218345144644992!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x39740d857c2f41d9%3A0x784aef38a9523b42!2sAgra%2C+Uttar+Pradesh!5e0!3m2!1sen!2sin!4v1532695955349" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>-->
						<iframe src="https://www.google.co.in/maps/d/embed?mid=1b4ASfjRg8SQwZk0-ZH36d7XA4uXTDo67" width="600" height="450" style="border:0"></iframe>
					</div>
				  </div>
				</div>
			</div>
		</div>
	</section>
	
	<section class="downloads">
		<div class="container">
			<div class="col-lg-12 col-md-12 col-sm-12 downloads-main">
				<h2 class="wow fadeInDown" data-wow-delay="100ms">Downloads</h2>
				<div class="downloads-icon">
					<div class="downloads-icon-text">
						<a class="one-a" target="_blank" href="<?php echo base_url();?>public/front/images/pdf/Agra_Brochure_new.pdf"><p class="wow fadeInLeft" data-wow-delay="100ms">E-Brochure</p></a>					
					</div>
					<div class="downloads-icon-text">
						<a class="one-b" target="_blank" href="<?php echo base_url();?>public/front/images/pdf/Application-form_Agra.pdf"><p class="wow fadeInLeft" data-wow-delay="200ms">Application Form</p></a>				
					</div>
					<div class="downloads-icon-text">
						<a class="one-c one-e" target="_blank" href="javascript:;"><p class="wow fadeInLeft" data-wow-delay="300ms">Payment Plan</p></a>				
					</div>
					<div class="downloads-icon-text">
						<a class="one-d one-e" target="_blank" href="javascript:;"><p class="wow fadeInLeft" data-wow-delay="400ms">Price List</p></a>				
					</div>
				</div>
			</div>
		</div>
	</section>
	
	<section class="loans">
		<div class="container">
			<div class="col-lg-12 col-md-12 col-sm-12 loans-main">
				<h2 class="wow fadeInDown" data-wow-delay="100ms">Home Loans Available By</h2>
				<div class="loans-slider owl-carousel owl-theme wow fadeInDown" data-wow-delay="300ms">
					<div class="item">
						<div class="loans-text">
							<img src="<?php echo base_url();?>public/front/images/bank1.jpg" alt=""/>
						</div>
					</div>
					<div class="item">
						<div class="loans-text">
							<img src="<?php echo base_url();?>public/front/images/bank-lic.jpg" alt=""/>
						</div>
					</div>
					<div class="item">
						<div class="loans-text">
							<img src="<?php echo base_url();?>public/front/images/bank-pnb.jpg" alt=""/>
						</div>
					</div>
					<div class="item">
						<div class="loans-text">
							<img src="<?php echo base_url();?>public/front/images/bank4.jpg" alt=""/>
						</div>
					</div>
				</div>	
			</div>
		</div>
	</section>
	
	
	<section class="location-map">
		<div class="container">
			<div class="map-area">
				<h2 class="wow fadeInDown" data-wow-delay="100ms">Location</h2>
				<ul class="location-map-main">
					<li>
						<h3 class="wow fadeInDown" data-wow-delay="200ms">Corporate Office</h3>
						<p class="wow fadeInDown" data-wow-delay="300ms">303, 3rd Floor, Global Foyer, Golf Course Road, <br>Sector-43, Gurgaon -122002, (Haryana) India</p>
						<p class="wow fadeInDown" data-wow-delay="100ms">
							<span>Email :</span> 
							<a href="mailto:info@rajdarbarrealty.com">info@rajdarbarrealty.com</a>
							<a href="mailto:customercare@rajdarbarrealty.com">customercare@rajdarbarrealty.com</a>
							<a href="mailto:sales@rajdarbarrealty.com">sales@rajdarbarrealty.com</a>
						</p>
						<p class="wow fadeInDown" data-wow-delay="400ms"><span></span> <a href="javascript:;"> </a> <a href="javascript:;"></a></p>
					</li>
					<li>
						<h3 class="wow fadeInDown" data-wow-delay="200ms">Site Office</h3>
						<p class="wow fadeInDown" data-wow-delay="300ms">Rajdarbar Village, Plot No. 1 Magtai, Near Shastripuram, Agra, Uttar Pradesh - 282007</p>
						<p class="wow fadeInDown" data-wow-delay="100ms">
							<span></span> 
							<a href="javascript:;"></a>
							<a href="javascript:;"></a>
							<a href="javascript:;"></a>
						</p>
						<p class="wow fadeInDown" data-wow-delay="400ms"><span>Tel :</span> <a href="callto:919927085295">+91 9927085295 </a> <a href="javascript:;"></a></p>
					</li>
				</ul>
				<div class="map-location wow fadeInDown google-map-wrap" >
					<div class="map">
						<iframe class="g-map" src="https://www.google.com/maps/d/embed?mid=1wcLmJzu5etrXpkSCOoaHb89jwvmZxK5r" width="640" height="480"></iframe>
					</div>
				</div>
			</div>
		</div>
		
	</section>