

<!--popup box-->
<!--<div class="black11"></div>
<div class="popbox"><div class="popclose">X</div>
	<h2>Global Foyer Golf Course Road: Key Highlights</h2>
	<ul>
		<li>Strategically located on the main Golf Course Road. </li>
		<li>Full glass exterior and top of the line architectural design building built in Ground plus Eight floors. </li>
		<li>Premium space available for retail, commercial, luxury spa, gym, restaurants, etc. </li>
		<li>Retail &amp; Commercial available for immediate fits-outs. </li>
		<li>Complete 8th floor available for high end lounge, corporate office, fine dining with huge open place. </li>
		<li>Ample, secured 3 level basement parking. </li>
		<li>High speed elevators &amp; escalators. </li>
		<li>Centrally air conditioned mall with complete power back-up.</li>
	</ul>
</div>-->
<!--end popup-->

<div class="banner inner wow fadeInDown">
	<!--<div class="banner-part1" style="background-image: url(images/gs-karnal1-1.jpg);">
		<img style="display: none;" src="images/gs-karnal1-1.jpg" alt=""/>
	</div>-->
	<div class="banner-slider owl-carousel owl-theme">
		<div class="item">
			<div class="banner-part" style="background-image: url(<?php echo base_url();?>public/front/images/gs-karnal1-1.jpg);">
				<img src="<?php echo base_url();?>public/front/images/gs-karnal1-1.jpg" alt="inner banner" style="display: none;" />
			</div>
		</div>
		<div class="item">
			<div class="banner-part" style="background-image: url(<?php echo base_url();?>public/front/images/gs-karnal1-2.jpg);">
				<img src="<?php echo base_url();?>public/front/images/gs-karnal1-2.jpg" alt="inner banner" style="display: none;" />
			</div>
		</div>
		<div class="item">
			<div class="banner-part" style="background-image: url(<?php echo base_url();?>public/front/images/gs-karnal1-3.jpg);">
				<img src="<?php echo base_url();?>public/front/images/gs-karnal1-3.jpg" alt="inner banner" style="display: none;" />
			</div>
		</div>
		<div class="item">
			<div class="banner-part" style="background-image: url(<?php echo base_url();?>public/front/images/gs-karnal1-4.jpg);">
				<img src="<?php echo base_url();?>public/front/images/gs-karnal1-4.jpg" alt="inner banner" style="display: none;" />
			</div>
		</div>
	</div>
	
	
	<div class="rotate-part">
		<div class="container">
			<div class="banner-bottom">
				<div class="pull-left banner-bottom-left wow fadeInLeft" data-wow-delay="100ms">
					<img src="<?php echo base_url();?>public/front/images/text-logo1.png" alt=""/>
				</div>
				<div class="pull-right banner-bottom-right">
					<a class="wow fadeInDown enquiry" data-wow-delay="100ms" href="<?php base_url()?>home/contact_us"><img src="<?php echo base_url();?>public/front/images/icon-enquiry1.png" alt=""/> Enquiry</a>
					<!--<a class="wow fadeInDown" data-wow-delay="100ms" href="javascript:;"><img src="images/360.png" alt=""/> Virtual Tour</a>
					<a class="wow fadeInDown" data-wow-delay="200ms" href="javascript:;"><img src="images/eb.png" alt=""/> E-Brochure</a>-->
				</div>
			</div>
		</div>
	</div>
</div>

	<div class="banner-text inner bre-slider">
		<div class="container">
			<div class="col-lg-12 col-md-12 col-sm-12 banner-text-main">
				<div class="col-lg-4 col-md-4 col-sm-4 banner-text-left">
					<h3 class="wow fadeInDown" data-wow-delay="100ms">Karnal Phase I <span>Delivered</span></h3>
				</div>
				<div class="col-lg-8 col-md-8 col-sm-8 banner-text-right">
					<div class="bre">
					<ul class="wow fadeInLeft" data-wow-delay="100ms">
						<li><a href="<?php echo base_url();?>">home</a></li>     
						<li>businesses</li>      
						<li>commercial</li>      
						<li><span>karnal phase I</span></li> 
					</ul>
					</div>
				</div>
			</div>
		</div>
	</div>
	
	
	<section class="overview residential">
		<div class="container">
			<div class="col-lg-12 col-md-12 col-sm-12 overview-main">
				<h2 class="wow fadeInDown marB" data-wow-delay="100ms">Overview</h2>
				<div class="float_img">
							<img src="<?php echo base_url();?>public/front/images/global-spaces-karnal1-th.jpg" alt="Global Spaces karnal" />
				</div>
				<p class="wow fadeInDown" data-wow-delay="200ms">Karnal is one of historical districts of Haryana. It is also known as a city of 'Daanveer Karn' of the Mahabharata fame. This district is known world over for its production of rice, wheat and milk. Karnal is an important city on Delhi Ambala rail line &amp; Sher Shah Suri Marg located on GT Road. Karnal is well connected with all important places in the country; it is approximately 123 kms from Delhi and about 130 kms from Chandigarh.</p>
				<p class="wow fadeInDown" data-wow-delay="100ms">Rajdarbar Limited Venture becomes a part of the grand history of Karnal with the launching of global spaces.</p>
				<h4 class="wow fadeInDown" data-wow-delay="100ms">A Township For Tomorrow</h4>
				<p class="wow fadeInDown" data-wow-delay="100ms">Welcome to global spaces, Karnal&rsquo;s most modern township situated near Atal park, behind the upper class neighborhood of Huda Sector 8 (ii).</p>
				<p class="wow fadeInDown" data-wow-delay="100ms">We have created ideal homes, from a choice of four luxury villas, located in garden surroundings. Each villa, designed to high standards of quality in construction and fittings, is provided with everything from built-in storage to fixtures, so you can just walk into your dream home.</p>
				<p class="wow fadeInDown" data-wow-delay="100ms">Global spaces, formerly called Narsi Village, is developed by Rajdarbar Limited - India&rsquo;s fastest growing real estate company, specializing in creating habitats for tomorrow. Our projects include luxury hotels, premium malls and office spaces, integrated townships and IT parks that have set new standards when it comes to quality and design.</p>
				
			</div>
		</div>
	</section>
	
	<section class="plan-feature">
		<div class="container">
			<div class="col-lg-12 col-md-12 col-sm-12 plan-feature-main">
				<h2 class="wow fadeInDown" data-wow-delay="100ms">Plans &amp; Features</h2>
				<div class="tab-system">

					<button type="button" class="navbar-toggle open">
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span> 
					</button>

				  <!-- Nav tabs -->
				  <ul class="nav nav-tabs nav-tabs1 wow fadeInDown" data-wow-delay="100ms" id="product-tabs-area" role="tablist">
				  	<li role="presentation" class="active"><a href="#tab1" role="tab" data-toggle="tab">Elevation</a></li>
					<li role="presentation"><a href="#tab2" role="tab" data-toggle="tab">Key Highlight</a></li>
					<li role="presentation"><a href="#tab3" role="tab" data-toggle="tab">Floor Plans</a></li>
					<li role="presentation"><a href="#tab4" aria-controls="home" role="tab" data-toggle="tab">Specifications</a></li>
					<li role="presentation"><a href="#tab5" role="tab" data-toggle="tab">Photo Gallery</a></li>
					<li role="presentation"><a href="#tab6" role="tab" data-toggle="tab">Site Plan</a></li>
					<li role="presentation"><a href="#tab7" role="tab" data-toggle="tab">Location Map</a></li>
				  </ul>

				  <!-- Tab panes -->
				  <div class="tab-content">
					<div class="tab-pane wow fadeInDown active" data-wow-delay="300ms" id="tab1">
						<div class="elev-slider owl-carousel owl-theme">
							<div class="item wow fadeInDown" data-wow-delay="130ms">
								<div class="shops-text">
									<img src="<?php echo base_url();?>public/front/images/gs-karnal1-gallery-1.jpg" alt=""/>
								</div>
							</div>
							<div class="item wow fadeInDown" data-wow-delay="130ms">
								<div class="shops-text">
									<img src="<?php echo base_url();?>public/front/images/gs-karnal1-gallery-2.jpg" alt=""/>
								</div>
							</div>
							<div class="item wow fadeInDown" data-wow-delay="130ms">
								<div class="shops-text">
									<img src="<?php echo base_url();?>public/front/images/gs-karnal1-gallery-3.jpg" alt=""/>
								</div>
							</div>
							<div class="item wow fadeInDown" data-wow-delay="130ms">
								<div class="shops-text">
									<img src="<?php echo base_url();?>public/front/images/gs-karnal1-gallery-4.jpg" alt=""/>
								</div>
							</div>
							<div class="item wow fadeInDown" data-wow-delay="130ms">
								<div class="shops-text">
									<img src="<?php echo base_url();?>public/front/images/gs-karnal1-gallery-5.jpg" alt=""/>
								</div>
							</div>
						</div>	
					</div>
					<div class="tab-pane wow fadeInDown" data-wow-delay="300ms" id="tab2">
						
							<div class="key-heighlight min-height">
								<p>On Request</p>
								<!--<p class="more-text">For Detailed Key Highlights Please <a href="javascript:;" class="more-vv">Click Here</a></p>-->
							</div>
						
					</div>
					<div class="tab-pane wow fadeInDown" data-wow-delay="300ms" id="tab3">
						<div class="key-heighlight min-height">
										<p>On Request</p>
							</div>			
					</div>
					<div class="tab-pane wow fadeInDown" data-wow-delay="300ms" id="tab4">
						<div class="key-heighlight min-height">
										<p>On Request</p>
							</div>
					</div>
					<div class="tab-pane wow fadeInDown" data-wow-delay="300ms" id="tab5">
						<!--<p style="text-align: center; color:#fff; font-size: 20px; margin-top: 40px;">No Images Available</p>-->
						<div class="elev-slider owl-carousel owl-theme wow fadeInUp">
							<div class="item">
								<div class="shops-text">
									<img src="<?php echo base_url();?>public/front/images/gs-karnal1-gallery-1.jpg" alt="gurgaon" />
								</div>
							</div>
							<div class="item">
								<div class="shops-text">
									<img src="<?php echo base_url();?>public/front/images/gs-karnal1-gallery-2.jpg" alt="gurgaon" />
								</div>
							</div>
							<div class="item">
								<div class="shops-text">
									<img src="<?php echo base_url();?>public/front/images/gs-karnal1-gallery-3.jpg" alt="gurgaon" />
								</div>
							</div>
							<div class="item">
								<div class="shops-text">
									<img src="<?php echo base_url();?>public/front/images/gs-karnal1-gallery-4.jpg" alt="gurgaon" />
								</div>
							</div>
							<div class="item">
								<div class="shops-text">
									<img src="<?php echo base_url();?>public/front/images/gs-karnal1-gallery-5.jpg" alt="gurgaon" />
								</div>
							</div>
							<div class="item">
								<div class="shops-text">
									<img src="<?php echo base_url();?>public/front/images/gs-karnal1-gallery-6.jpg" alt="gurgaon" />
								</div>
							</div>
						</div>
					</div>
					<div class="tab-pane wow fadeInDown" data-wow-delay="300ms" id="tab6">
						<div class="key-heighlight min-height">
										<p>Being updated</p>
							</div>	
					</div>
					<div class="tab-pane wow fadeInDown" data-wow-delay="300ms" id="tab7">
						<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3483.932820093233!2d75.76985921451731!3d29.166651866457656!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3912324df5c550b3%3A0x43a9171bdc1cb008!2sGlobal+Spaces!5e0!3m2!1sen!2sin!4v1532589993326" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
					</div>
				  </div>
				</div>
			</div>
		</div>
	</section>
	
	<section class="downloads">
		<div class="container">
			<div class="col-lg-12 col-md-12 col-sm-12 downloads-main">
				<h2 class="wow fadeInDown" data-wow-delay="100ms">Downloads</h2>
				<div class="downloads-icon">
					<div class="downloads-icon-text">
						<a class="one-a one-e" href="javascript:;"><p class="wow fadeInLeft" data-wow-delay="100ms">E-Brochure</p></a>					
					</div>
					<div class="downloads-icon-text">
						<a class="one-b one-e" href="javascript:;"><p class="wow fadeInLeft" data-wow-delay="200ms">Application Form</p></a>				
					</div>
					<div class="downloads-icon-text">
						<a class="one-c one-e" href="javascript:;"><p class="wow fadeInLeft" data-wow-delay="300ms">Payment Plan</p></a>				
					</div>
					<div class="downloads-icon-text">
						<a class="one-d one-e" href="javascript:;"><p class="wow fadeInLeft" data-wow-delay="400ms">Price List</p></a>				
					</div>
				</div>
			</div>
		</div>
	</section>
	
	<section class="loans">
		<div class="container">
			<div class="col-lg-12 col-md-12 col-sm-12 loans-main">
				<h2 class="wow fadeInDown" data-wow-delay="100ms">Home Loans Available By</h2>
				<div class="loans-slider owl-carousel owl-theme wow fadeInDown" data-wow-delay="300ms">
					<div class="item">
						<div class="loans-text">
							<img src="<?php echo base_url();?>public/front/images/bank1.jpg" alt=""/>
						</div>
					</div>
					<div class="item">
						<div class="loans-text">
							<img src="<?php echo base_url();?>public/front/images/bank-lic.jpg" alt=""/>
						</div>
					</div>
					<div class="item">
						<div class="loans-text">
							<img src="<?php echo base_url();?>public/front/images/bank-pnb.jpg" alt=""/>
						</div>
					</div>
					<div class="item">
						<div class="loans-text">
							<img src="<?php echo base_url();?>public/front/images/bank4.jpg" alt=""/>
						</div>
					</div>
				</div>	
			</div>
		</div>
	</section>
	
	
	<section class="location-map">
		<div class="container">
			<div class="map-area">
				<h2 class="wow fadeInDown" data-wow-delay="100ms">Location</h2>
				<ul class="location-map-main">
					<li>
						<h3 class="wow fadeInDown" data-wow-delay="200ms">Corporate Office</h3>
						<p class="wow fadeInDown" data-wow-delay="300ms">303, 3rd Floor, Global Foyer, Golf Course Road, <br>Sector-43, Gurgaon -122002, (Haryana) India</p>
						<p class="wow fadeInDown" data-wow-delay="100ms">
							<span>Email :</span> 
							<a href="mailto:info@globalrealty.com">info@globalrealty.com</a>
							<a href="mailto:customercare@globalrealty.com">customercare@globalrealty.com</a>
							<a href="mailto:sales@globalrealty.com">sales@globalrealty.com</a>
						</p>
						<p class="wow fadeInDown" data-wow-delay="400ms"><span></span> <a href="javascript:;"> </a> <a href="javascript:;"></a></p>
					</li>
					<li>
						<h3 class="wow fadeInDown" data-wow-delay="200ms">Site Office</h3>
						<p class="wow fadeInDown" data-wow-delay="300ms">Global Spaces, Sector 32, Behind Sector 8, Part- 2, <br>Karnal - 132001 (Haryana)</p>
						<p class="wow fadeInDown" data-wow-delay="100ms">
							<span></span> 
							<a href="javascript:;"></a>
							<a href="javascript:;"></a>
							<a href="javascript:;"></a>
						</p>
						<p class="wow fadeInDown" data-wow-delay="400ms"><span>Tel :</span> <a href="callto:919991888948">+91 9991888948 </a> <a href="javascript:;"></a></p>
					</li>
				</ul>
				<div class="map-location wow fadeInDown google-map-wrap" >
					<div class="map">
						<iframe class="g-map" src="https://www.google.com/maps/d/embed?mid=1wcLmJzu5etrXpkSCOoaHb89jwvmZxK5r" width="640" height="480"></iframe>
					</div>
				</div>
			</div>
		</div>
		
	</section>
