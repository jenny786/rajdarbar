<!--popup box-->
	
<div class="black11"></div>
<div class="popbox"><div class="popclose">X</div>
	<h2>Global Foyer Golf Course Road: Key Highlights</h2>
	<ul>
		<li>Strategically located on the main Golf Course Road. </li>
		<li>Full glass exterior and top of the line architectural design building built in Ground plus Eight floors. </li>
		<li>Premium space available for retail, commercial, luxury spa, gym, restaurants, etc. </li>
		<li>Retail &amp; Commercial available for immediate fits-outs. </li>
		<li>Complete 8th floor available for high end lounge, corporate office, fine dining with huge open place. </li>
		<li>Ample, secured 3 level basement parking. </li>
		<li>High speed elevators &amp; escalators. </li>
		<li>Centrally air conditioned mall with complete power back-up.</li>
	</ul>
</div>
<!--end popup-->


<div class="banner inner wow fadeInDown">
	<!--<div class="banner-part1" style="background-image: url(images/banner2.jpg);">
		<img style="display: none;" src="images/banner2.jpg" alt=""/>
	</div>-->

	<div class="banner-slider owl-carousel owl-theme">
		<div class="item">
			<div class="banner-part" style="background-image: url(<?php echo base_url();?>public/front/images/gurg-slide-3.jpg);">
				<img src="<?php echo base_url();?>public/front/images/gurg-slide-3.jpg" alt="inner banner" style="display: none;" />
			</div>
		</div>
		
	</div>
	
	<div class="rotate-part">
		<div class="container">
			<div class="banner-bottom">
				<div class="spaces pull-left banner-bottom-left wow fadeInLeft" data-wow-delay="100ms">
					<img src="<?php echo base_url();?>public/front/images/commercial-foyer.jpg" alt=""/>
				</div>
				<div class="spaces pull-right banner-bottom-right">
					<!--<a class="wow fadeInDown" data-wow-delay="100ms" href="javascript:;" target="_blank"><img src="images/360.png" alt=""/> Virtual Tour</a>-->
					<a class="wow fadeInDown enquiry" data-wow-delay="100ms" href="<?php echo base_url()?>home/contact_us"><img src="<?php echo base_url();?>public/front/images/icon-enquiry1.png" alt=""/> Enquiry</a>
					<a class="wow fadeInDown" data-wow-delay="200ms" href="<?php echo base_url();?>public/front/images/pdf/the-Foyer-Book-final.pdf" target="_blank"><img src="<?php echo base_url();?>public/front/images/eb.png" alt=""/> E-Brochure</a>
				</div>
			</div>
		</div>
	</div>
</div>

	<div class="banner-text inner bre-slider">
		<div class="container">
			<div class="col-lg-12 col-md-12 col-sm-12 banner-text-main">
				<div class="col-lg-4 col-md-4 col-sm-4 banner-text-left">
					<h3 class="wow fadeInDown" data-wow-delay="100ms">Gurgaon <span>Delivered</span></h3>
				</div>
				<div class="col-lg-8 col-md-8 col-sm-8 banner-text-right">
					<div class="bre">
					<ul class="wow fadeInLeft" data-wow-delay="100ms">
						<li><a href="<?php echo base_url();?>">home</a></li>     
						<li>project</li>      
						<li>commercial</li>      
						<li><span>gurgaon</span></li> 
					</ul>
					</div>
				</div>
			</div>
		</div>
	</div>
	
	
	<section class="overview commercial">
		<div class="container">
			<div class="col-lg-12 col-md-12 col-sm-12 overview-main">
				<h2 class="wow fadeInDown" data-wow-delay="100ms">Overview</h2>
				<h3>India's Royal Luxury Mall</h3>
				<div class="row">
					<div class="col-md-6 col-sm-6">
						<div class="box left">
							<p class="wow fadeInDown" data-wow-delay="200ms">Global Foyer is Gurgaon's first luxury mall. It offers premium space on Gurgaon's most sought after locale - the Golf Course Road. No wonder it is the location of choice for luxury retail and automotive brands like Mercedez Benz and Hyundai Motors to showcase their products. The Global Foyer, Gurgaon is also a hub for Fine Dining &amp; celebrations! To some corporations the mix of luxury, food and frolic also makes it a vibrant choice for an office that is right above the most happening spot in the city. </p>	
						</div>
					</div>
					<div class="col-md-6 col-sm-6">
						<div class="box right">
							<img src="<?php echo base_url();?>public/front/images/foyer-gurgaon-th-n.jpg" alt="" />
						</div>
					</div>
				</div>
			</div>
			
		</div>
	</section>
	
	<section class="plan-feature">
		<div class="container">
			<div class="col-lg-12 col-md-12 col-sm-12 plan-feature-main">
				<h2 class="wow fadeInDown" data-wow-delay="100ms">Plans &amp; Features</h2>
				<div class="tab-system">

					<button type="button" class="navbar-toggle open">
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span> 
					</button>

				  <!-- Nav tabs -->
				  <ul class="nav nav-tabs nav-tabs1 wow fadeInDown" id="product-tabs-area" data-wow-delay="100ms" role="tablist">
				  	<li role="presentation" class="active"><a href="#tab1" role="tab" data-toggle="tab">Elevation</a></li>
					<li role="presentation"><a href="#tab2" role="tab" data-toggle="tab">Key Highlight</a></li>
					<li role="presentation"><a href="#tab3" role="tab" data-toggle="tab">Floor Plans</a></li>
					<li role="presentation"><a href="#tab4" aria-controls="home" role="tab" data-toggle="tab">Specifications</a></li>
					<li role="presentation"><a href="#tab5" role="tab" data-toggle="tab">Photo Gallery</a></li>
					<li role="presentation"><a href="#tab6" role="tab" data-toggle="tab">Site Plan</a></li>
					<li role="presentation"><a href="#tab7" role="tab" data-toggle="tab">Location Map</a></li>
				  </ul>

				  <!-- Tab panes -->
				  <div class="tab-content">
					<div class="tab-pane wow fadeInDown active" data-wow-delay="300ms" id="tab1">
						<div class="elev-slider owl-carousel owl-theme">
							<div class="item wow fadeInDown" data-wow-delay="130ms">
								<div class="shops-text">
									<img src="<?php echo base_url();?>public/front/images/gmpv-slide-1.jpg" alt=""/>
								</div>
							</div>
							<div class="item wow fadeInDown" data-wow-delay="130ms">
								<div class="shops-text">
									<img src="<?php echo base_url();?>public/front/images/gurg-slide-2.jpg" alt=""/>
								</div>
							</div>
							<div class="item wow fadeInDown" data-wow-delay="130ms">
								<div class="shops-text">
									<img src="<?php echo base_url();?>public/front/images/gurg-gallery-new-1.jpg" alt=""/>
								</div>
							</div>
							<div class="item wow fadeInDown" data-wow-delay="130ms">
								<div class="shops-text">
									<img src="<?php echo base_url();?>public/front/images/gurg-gallery-new-5.jpg" alt=""/>
								</div>
							</div>
							<div class="item wow fadeInDown" data-wow-delay="130ms">
								<div class="shops-text">
									<img src="<?php echo base_url();?>public/front/images/gurg-gallery-new-3.jpg" alt=""/>
								</div>
							</div>
						</div>	
					</div>
					<div class="tab-pane wow fadeInDown" data-wow-delay="300ms" id="tab2">
						<div class="key-heighlight">
							<ul class="key-list">
								<li>
									<div class="img">
										<a href="javascript:;">
											<img class="img1" src="<?php echo base_url();?>public/front/images/icon/icon-key-highlight-3.png" alt="keyhilight" />
											<img class="img2" src="<?php echo base_url();?>public/front/images/icon/iconw-key-highlight-3.png" alt="keyhilight" />
										</a>
									</div>
									<div class="desc">
										<p>Strategically located on the main Golf Course Road. </p>
									</div>
								</li>
								<li>
									<div class="img">
										<a href="javascript:;">
											<img class="img1" src="<?php echo base_url();?>public/front/images/icon/icon-escalators.png" alt="keyhilight" />
											<img class="img2" src="<?php echo base_url();?>public/front/images/icon/iconw-escalators.png" alt="keyhilight" />
										</a>
									</div>
									<div class="desc">
										<p>Built in Ground plus Eight floors.</p>
									</div>
								</li>
								<li>
									<div class="img">
										<a href="javascript:;">
											<img class="img1" src="<?php echo base_url();?>public/front/images/icon/icon-gym.png" alt="keyhilight" />
											<img class="img2" src="<?php echo base_url();?>public/front/images/icon/iconw-gym.png" alt="keyhilight" />
										</a>
									</div>
									<div class="desc">
										<p>Luxury spa, gym, restaurants, etc. </p>
									</div>
								</li>
								<li>
									<div class="img">
										<a href="javascript:;">
											<img class="img1" src="<?php echo base_url();?>public/front/images/icon/icon-key-highlight-8.png" alt="keyhilight" />
											<img class="img2" src="<?php echo base_url();?>public/front/images/icon/iconw-key-highlight-8.png" alt="keyhilight" />
										</a>
									</div>
									<div class="desc">
										<p>Retail &amp; Commercial available for immediate fits-outs. </p>
									</div>
								</li>
								<li>
									<div class="img">
										<a href="javascript:;">
											<img class="img1" src="<?php echo base_url();?>public/front/images/icon/icon-Lounge.png" alt="keyhilight" />
											<img class="img2" src="<?php echo base_url();?>public/front/images/icon/iconw-Lounge.png" alt="keyhilight" />
										</a>
									</div>
									<div class="desc">
										<p>Lounge, Corporate Office, Fine dining with Huge open Place. </p>
									</div>
								</li>
								<li>
									<div class="img">
										<a href="javascript:;">
											<img class="img1" src="<?php echo base_url();?>public/front/images/icon/icon-parking.png" alt="keyhilight" />
											<img class="img2" src="<?php echo base_url();?>public/front/images/icon/iconw-parking.png" alt="keyhilight" />
										</a>
									</div>
									<div class="desc">
										<p>Ample, secured 3 level basement parking. </p>
									</div>
								</li>
								<li>
									<div class="img">
										<a href="javascript:;">
											<img class="img1" src="<?php echo base_url();?>public/front/images/icon/icon-elevators.png" alt="keyhilight" />
											<img class="img2" src="<?php echo base_url();?>public/front/images/icon/iconw-elevators.png" alt="keyhilight" />
										</a>
									</div>
									<div class="desc">
										<p>High speed elevators &amp; escalators. </p>
									</div>
								</li>
								<li>
									<div class="img">
										<a href="javascript:;">
											<img class="img1" src="<?php echo base_url();?>public/front/images/icon/icon-key-highlight-6.png" alt="keyhilight" />
											<img class="img2" src="<?php echo base_url();?>public/front/images/icon/iconw-key-highlight-6.png" alt="keyhilight" />
										</a>
									</div>
									<div class="desc">
										<p>Centrally air conditioned mall with complete power back-up.</p>
									</div>
								</li>
								
							</ul>
						<p class="more-text">For Detailed Key Highlights Please <a href="javascript:;" class="more-vv">Click Here</a></p>


						</div>
					</div>
					<div class="tab-pane wow fadeInDown docs-pictures" data-wow-delay="300ms" id="tab3">
						<div class="floorplan">
							<div id="floorplan-slider" class="owl-carousel owl-theme">
								<div class="item">
							    	<div class="box">
								    	<div class="img">
											<span>
												<img data-original="<?php echo base_url();?>public/front/images/plan-first-basement.jpg" src="<?php echo base_url();?>public/front/images/plan-first-basement-th.jpg" alt="floor-plan" />
											</span>
										</div>
										<div class="buttons">
											<a href="javascript:;" class="btn">First Basement Plan</a>
										</div>
									</div>
							    </div>
							    <div class="item">
							    	<div class="box">
								    	<div class="img">
											<span>
												<img data-original="<?php echo base_url();?>public/front/images/plan-second-basement.jpg" src="<?php echo base_url();?>public/front/images/plan-second-basement-th.jpg" alt="floor-plan" />
											</span>
										</div>
										<div class="buttons">
											<a href="javascript:;" class="btn">Second Basement Plan</a>
										</div>
									</div>
							    </div>
							    <div class="item">
							    	<div class="box">
								    	<div class="img">
											<span>
												<img data-original="<?php echo base_url();?>public/front/images/plan-third-basement.jpg" src="<?php echo base_url();?>public/front/images/plan-third-basement-th.jpg" alt="floor-plan" />
											</span>
										</div>
										<div class="buttons">
											<a href="javascript:;" class="btn">Third Basement Plan</a>
										</div>
									</div>
							    </div>
							    <div class="item">
							    	<div class="box">
								    	<div class="img">
											<span>
												<img data-original="<?php echo base_url();?>public/front/images/plan-first-floor.jpg" src="<?php echo base_url();?>public/front/images/plan-first-floor-th.jpg" alt="floor-plan" />
											</span>
										</div>
										<div class="buttons">
											<a href="javascript:;" class="btn">First Floor Plan</a>
										</div>
									</div>
							    </div>
							     <div class="item">
							    	<div class="box">
								    	<div class="img">
											<span>
												<img data-original="<?php echo base_url();?>public/front/images/plan-second-floor.jpg" src="<?php echo base_url();?>public/front/images/plan-second-floor-th.jpg" alt="floor-plan" />
											</span>
										</div>
										<div class="buttons">
											<a href="javascript:;" class="btn">Second Floor Plan</a>
										</div>
									</div>
							    </div>
							     <div class="item">
							    	<div class="box">
								    	<div class="img">
											<span>
												<img data-original="<?php echo base_url();?>public/front/images/plan-third-floor.jpg" src="<?php echo base_url();?>public/front/images/plan-third-floor-th.jpg" alt="floor-plan" />
											</span>
										</div>
										<div class="buttons">
											<a href="javascript:;" class="btn">Third Floor Plan</a>
										</div>
									</div>
							    </div>
							     <div class="item">
							    	<div class="box">
								    	<div class="img">
											<span>
												<img data-original="<?php echo base_url();?>public/front/images/plan-typical-floor.jpg" src="<?php echo base_url();?>public/front/images/plan-typical-floor-th.jpg" alt="floor-plan" />
											</span>
										</div>
										<div class="buttons">
											<a href="javascript:;" class="btn">Typical 4, 5 &amp; 6th Floor Plan</a>
										</div>
									</div>
							    </div>
							    <div class="item">
							    	<div class="box">
								    	<div class="img">
											<span>
												<img data-original="<?php echo base_url();?>public/front/images/plan-seventh-floor.jpg" src="<?php echo base_url();?>public/front/images/plan-seventh-floor-th.jpg" alt="floor-plan" />
											</span>
										</div>
										<div class="buttons">
											<a href="javascript:;" class="btn">Seventh Floor Plan</a>
										</div>
									</div>
							    </div>
							    <div class="item">
							    	<div class="box">
								    	<div class="img">
											<span>
												<img data-original="<?php echo base_url();?>public/front/images/plan-eight-floor.jpg" src="<?php echo base_url();?>public/front/images/plan-eight-floor-th.jpg" alt="floor-plan" />
											</span>
										</div>
										<div class="buttons">
											<a href="javascript:;" class="btn">Eight Floor Plan</a>
										</div>
									</div>
							    </div>
							</div>
						</div>			
					</div>
					<div class="tab-pane wow fadeInDown" data-wow-delay="300ms" id="tab4">
							<div class="key-heighlight min-height">
								<p>On Request</p>
							</div>
					</div>
					
					

					
					<div class="tab-pane wow fadeInDown" data-wow-delay="300ms" id="tab5">
						<div class="elev-slider owl-carousel owl-theme wow fadeInUp">
							<div class="item">
								<div class="shops-text">
									<img src="<?php echo base_url();?>public/front/images/gurg-gallery-new-1.jpg" alt="gurgaon" />
								</div>
							</div>
							<div class="item">
								<div class="shops-text">
									<img src="<?php echo base_url();?>public/front/images/gurg-gallery-new-2.jpg" alt="gurgaon" />
								</div>
							</div>
							<div class="item">
								<div class="shops-text">
									<img src="<?php echo base_url();?>public/front/images/gurg-gallery-new-3.jpg" alt="gurgaon" />
								</div>
							</div>
							<div class="item">
								<div class="shops-text">
									<img src="<?php echo base_url();?>public/front/images/gurg-gallery-new-4.jpg" alt="gurgaon" />
								</div>
							</div>
							<div class="item">
								<div class="shops-text">
									<img src="<?php echo base_url();?>public/front/images/gurg-gallery-new-5.jpg" alt="gurgaon" />
								</div>
							</div>
							<div class="item">
								<div class="shops-text">
									<img src="<?php echo base_url();?>public/front/images/gurg-gallery-new-6.jpg" alt="gurgaon" />
								</div>
							</div>
							<div class="item">
								<div class="shops-text">
									<img src="<?php echo base_url();?>public/front/images/gurg-gallery-new-7.jpg" alt="gurgaon" />
								</div>
							</div>
						</div>
					</div>

					<div class="tab-pane wow fadeInDown docs-pictures" data-wow-delay="300ms" id="tab6">
						
						<div class="Walkthrough siteplan">
							<span><img data-original="<?php echo base_url();?>public/front/images/plan-site-map.jpg" src="<?php echo base_url();?>public/front/images/plan-site-map.jpg" alt="Location" /></span>
						</div>	
					</div>

					
					<div class="tab-pane wow fadeInDown" data-wow-delay="300ms" id="tab7">
						<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3507.6335705719416!2d77.09284221432372!3d28.460460395800382!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x390d18e7b01a21cb%3A0xee16c496a9f86abd!2sGlobal+Foyer!5e0!3m2!1sen!2sin!4v1532586072294" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
					</div>
				  </div>
				</div>
			</div>
		</div>
	</section>
	
	<section class="downloads">
		<div class="container">
			<div class="col-lg-12 col-md-12 col-sm-12 downloads-main">
				<h2 class="wow fadeInDown" data-wow-delay="100ms">Downloads</h2>
				<div class="downloads-icon">
					<div class="downloads-icon-text">
						<a class="one-a" target="_blank" href="<?php echo base_url();?>public/front/images/pdf/the-Foyer-Book-final.pdf"><p class="wow fadeInLeft" data-wow-delay="100ms">E-Brochure</p></a>				
					</div>
					<div class="downloads-icon-text vv">
						<a class="one-b one-e" target="_blank" href="javascript:;"><p class="wow fadeInLeft" data-wow-delay="200ms">Application Form</p></a>				
					</div>
					<div class="downloads-icon-text">
						<a class="one-c one-e" target="_blank" href="javascript:;"><p class="wow fadeInLeft" data-wow-delay="300ms">Payment Plan</p></a>				
					</div>
					<div class="downloads-icon-text">
						<a class="one-d one-e" target="_blank" href="javascript:;"><p class="wow fadeInLeft" data-wow-delay="400ms">Price List</p></a>				
					</div>
				</div>
			</div>
		</div>
	</section>
	
	<section class="loans">
		<div class="container">
			<div class="col-lg-12 col-md-12 col-sm-12 loans-main">
				<h2 class="wow fadeInDown" data-wow-delay="100ms">Home Loans Available By</h2>
				<div class="loans-slider owl-carousel owl-theme wow fadeInDown" data-wow-delay="300ms">
					<div class="item">
						<div class="loans-text">
							<img src="<?php echo base_url();?>public/front/images/bank1.jpg" alt=""/>
						</div>
					</div>
					<div class="item">
						<div class="loans-text">
							<img src="<?php echo base_url();?>public/front/images/bank-lic.jpg" alt=""/>
						</div>
					</div>
					<div class="item">
						<div class="loans-text">
							<img src="<?php echo base_url();?>public/front/images/bank-pnb.jpg" alt=""/>
						</div>
					</div>
					<div class="item">
						<div class="loans-text">
							<img src="<?php echo base_url();?>public/front/images/bank4.jpg" alt=""/>
						</div>
					</div>
				</div>	
			</div>
		</div>
	</section>
	
	
	<section class="location-map">
		<div class="container">
			<div class="map-area">
				<h2 class="wow fadeInDown" data-wow-delay="100ms">Location</h2>
				<ul class="location-map-main">
					<li>
						<h3 class="wow fadeInDown" data-wow-delay="200ms">Corporate Office</h3>
						<p class="wow fadeInDown" data-wow-delay="300ms">303, 3rd Floor, Global Foyer, Golf Course Road, <br>Sector-43, Gurgaon -122002, (Haryana) India</p>
						<p class="wow fadeInDown" data-wow-delay="100ms">
							<span>Email :</span> 
							<a href="mailto:info@globalrealty.com">info@rajdarbarrealty.com</a>
							<a href="mailto:customercare@globalrealty.com">customercare@rajdarbarrealty.com</a>
							<a href="mailto:sales@globalrealty.com">sales@rajdarbarrealty.com</a>
						</p>
						<p class="wow fadeInDown" data-wow-delay="400ms"><span></span> <a href="javascript:;"> </a> <a href="javascript:;"></a></p>
					</li>
					<li>
						<h3 class="wow fadeInDown" data-wow-delay="200ms">Site Office</h3>
						<p class="wow fadeInDown" data-wow-delay="300ms">Global Foyer, Golf Course Road, Sector-43, <br>Gurgaon -122002, (Haryana) India</p>
						<p class="wow fadeInDown" data-wow-delay="100ms">
							<span></span> 
							<a href="javascript:;"></a>
							<a href="javascript:;"></a>
							<a href="javascript:;"></a>
						</p>
						<p class="wow fadeInDown" data-wow-delay="400ms"><span>Tel :</span> <a href="callto:918373912275">+91 8373912275 </a> <a href="callto:918373912276">+91 8373912276</a></p>
					</li>
				</ul>
				<div class="map-location wow fadeInDown google-map-wrap" >
					<div class="map">
						<iframe class="g-map" src="https://www.google.com/maps/d/embed?mid=1wcLmJzu5etrXpkSCOoaHb89jwvmZxK5r" width="640" height="480"></iframe>
					</div>
				</div>
			</div>
		</div>
		
	</section>