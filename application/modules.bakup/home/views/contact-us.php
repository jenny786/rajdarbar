<div class="banner inner-b wow fadeInDown">
	<div class="banner-part1" style="background-image: url(<?php echo base_url();?>public/front/images/banner-contactus.jpg);">
		<img style="display: none;" src="<?php echo base_url();?>public/front/images/banner-contactus.jpg" alt=""/>
	</div>
</div>
<div class="banner-text inner wow fadeInDown" data-wow-delay="100ms">
	<div class="container">
		<div class="col-lg-12 col-md-12 col-sm-12 banner-text-main">
			<div class="col-lg-3 col-md-3 col-sm-3 banner-text-left">
				<h3 class="wow fadeInDown" data-wow-delay="200ms">Contact Us</h3>
			</div>
			<div class="col-lg-9 col-md-9 col-sm-9 banner-text-right">
				<div class="bre">
				<ul class="wow fadeInDown" data-wow-delay="300ms">
					<li><a href="<?php echo base_url()?>">home</a></li>         
					<li><span>contact us</span></li> 
				</ul>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="contactus">
	<div class="container">
		<div class="getintouch" id="contactus">
			<div class="box left">
				<h2>Get In Touch</h2>
				<?php $attributes = array('id' => 'applyform', 'name'=>'applyform');
	echo form_open_multipart('home/contactsubmit', $attributes); ?>	
					<div class="fields">
						<div class="field">
							<div class="input-box">
								<img src="<?php echo base_url();?>public/front/images/icon-user.png" alt="" />
								<input type="text" name="name" id="name" alt="" placeholder="Name" /> 
							</div>
							<div class="input-box">
								<img src="<?php echo base_url();?>public/front/images/icon-email.png" alt="" />
								<input type="text" name="email" id="email" alt="" placeholder="Email" /> 
							</div>
						</div>
						<div class="field">
							<div class="input-box">
								<img src="<?php echo base_url();?>public/front/images/icon-mobile.png" alt="" />
								<input type="text" name="phone" id="phone" alt="" placeholder="Phone no." />
							</div>
							<div class="select-box">
								<img src="<?php echo base_url();?>public/front/images/icon-building.png" alt="" />
								<select name="project" id="project">
									<option value="">Select your project</option>
									<option value="global foyer gurgaon">Global Foyer Gurgaon</option>
									<option value="global foyer palam vihar">Global Foyer Palam Vihar</option>
									<option value="global spaces hisar">Global Spaces Hisar</option>
									<option value="global spaces sirsa">Global Spaces Sirsa</option>
									<option value="global spaces agra">Global Spaces Agra</option>
									<option value="global spaces karnal phase1">Global Spaces Karnal Phase I</option>
									<option value="global spaces karnal phase2">Global Spaces Karnal Phase II</option>
								</select>
							</div>
						</div>
						<div class="field msg">
							<div class="input-box">
								<img src="<?php echo base_url();?>public/front/images/icon-query.png" alt="" />
								<textarea name="message" id="message" placeholder="Comment"></textarea>
							</div>
						</div>
						<div class="field captcha">
							<div class="input-box">
								<div class="capbox">
									<div class="captcha-title">Captcha</div>
									<div id="CaptchaDiv"></div>
									<div class="capbox-inner">
										<input type="hidden" id="txtCaptcha">
										<input type="text" name="CaptchaInput" id="CaptchaInput" size="15">
									</div>
								</div>
							</div>
						</div>
						<div class="field buttons">
							<div class="input-box">
								<input type="submit" value="Submit" alt="" />
								<input type="button" value="Reset" class="btn-reset" alt="" />
							</div>
						</div>
					</div>
				</form>
			</div>
			<div class="box right">
				<div class="map">
					<iframe src="https://www.google.com/maps/d/embed?mid=1wcLmJzu5etrXpkSCOoaHb89jwvmZxK5r" width="640" height="480"></iframe>
				</div>
			</div>
		</div>
		<div class="multiple-add">
			<ul>
				<li>
					<h3>Corporate Office</h3>
					<address>303, 3rd Floor, Global Foyer, Golf Course Road, <br />Sector-43, Gurgaon -122002, (Haryana) India</address>
					<div class="call"><a href="callto:918373912275">+91 8373912275</a> / <a href="callto:918373912276">+91 8373912276</a></div>
					<div class="mail">
						<p><a href="mailto:info@rajdarbarrealty.com">info@rajdarbarrealty.com</a></p>
						<p><a href="mailto:customercare@rajdarbarrealty.com">customercare@rajdarbarrealty.com</a></p>
						<p><a href="mailto:sales@rajdarbarrealty.com">sales@rajdarbarrealty.com</a></p>
					</div>
				</li>
				<li>
					<h3>Leasing Enquires</h3>
					<address>Global Foyer, Behind Ansal Plaza, Palam Vihar, <br />Sector-1, Gurgaon (Haryana)</address>
					<div class="call"><a href="callto:918373912275">+91 8373912275</a> / <a href="callto:918373912276">+91 8373912276</a></div>
					<div class="mail">
						<p><a href="mailto:leasing@rajdarbarrealty.co.in">leasing@rajdarbarrealty.com</a></p>
					</div>
				</li>
			</ul>
		</div>
		<div class="site-offices">
			<h3>Site Offices</h3>
			<ul>
				<li>
					<div class="box">
						<h5>Karnal I</h5>
						<address>Global Spaces, Sector 32, Behind Sector 8, Part- 2, <br>Karnal - 132001 (Haryana)</address>
						<div class="call"><a href="callto:919991888948">+91 9991888948</a></div>
					</div>
				</li>
				<li>
					<div class="box">
						<h5>Karnal II</h5>
						<address>Global Spaces, Sector 33, Behind Sector 7, <br>Karnal - 132001 (Haryana)</address>
						<div class="call"><a href="callto:919991888948">+91 9991888948</a></div>
					</div>
				</li>
				<li>
					<div class="box">
						<h5>Agra</h5>
						<address>Rajdarbar Village, Plot No. 1 Magtai, Near Shastripuram, Agra, Uttar Pradesh - 282007</address>
						<div class="call"><a href="callto:919927085295">+91 9927085295</a></div>
					</div>
				</li>
				<li>
					<div class="box">
						<h5>Hisar</h5>
						<address>Rajdarbar Spaces, Sector - 24, 7th KM Stone,<br>Hisar - Delhi By Pass, Hisar - 125051 (Haryana)</address>
						<div class="call"><a href="callto:919991888940">+91 9991888940</a></div>
					</div>
				</li>
				<li>
					<div class="box">
						<h5>Sirsa</h5>
						<address>Rajdarbar Spaces, Sector - 1, Opp Sector 20<br>Part - III HUDA, Sirsa - 125055 (Haryana)</address>
						<div class="call"><a href="callto:919991888940">+91 9991888940</a></div>
					</div>
				</li>
				<li>
					<div class="box">
						<h5>Palam Vihar Gurgaon</h5>
						<address>Global Foyer, Behind Ansal Plaza, Palam Vihar, Sector-1, Gurgaon (Haryana)</address>
						<div class="call"><a href="callto:918373912275">+91 8373912275</a> / <a href="callto:918373912276">+91 8373912276</a></div>
					</div>
				</li>
			</ul>
		</div>
	</div>
</div>

<script src="<?php echo base_url();?>/public/front/js/jquery.validate.js"></script>

<script type="text/javascript">

/****************************************************************************
* Submit the form data 
******************************************************************************/  	
	$.validator.addMethod("lettersonly", function(value, element) {
	return this.optional(element) || /^[a-zA-Z\s]*$/.test(value);
	}, "Letters only please");
	
	$("#applyform").validate({
		errorClass: "validationError",
        rules: {
				name:{
				  required:true,
				  lettersonly: true
				},
				phone:"required",
				message:"required",
				email:"required",
				project:"required",
				CaptchaInput:{
				  required:true,
				  equalTo: "#txtCaptcha"
				}
            },
            messages: {
                name: {
					required: "Name is required",
					lettersonly: "Enter Valid Name",
				},
				phone:"Please Enter Phone Number",
				email:"Please enter email",
				message:"Please enter message",
				project:"Please select Equipment Category",
				CaptchaInput: {
					required: "Captcha is required",
					equalTo: "Captcha is not valid",
				}
            },
		submitHandler: function(form){	
            sessionStorage.removeItem("session_pro");			
			form.submit();
		}
	});
	

var a = Math.ceil(Math.random() * 9)+ '';
var b = Math.ceil(Math.random() * 9)+ '';
var c = Math.ceil(Math.random() * 9)+ '';
var d = Math.ceil(Math.random() * 9)+ '';
var e = Math.ceil(Math.random() * 9)+ '';

var code = a + b + c + d + e;

document.getElementById("txtCaptcha").value = code;
document.getElementById("CaptchaDiv").innerHTML = code;

// Validate input against the generated number
function ValidCaptcha(){
var str1 = removeSpaces(document.getElementById('txtCaptcha').value);
var str2 = removeSpaces(document.getElementById('CaptchaInput').value);
if (str1 == str2){
return true;
}else{
return false;
}
}

// Remove the spaces from the entered and generated code
function removeSpaces(string){
return string.split(' ').join('');
}
</script>
