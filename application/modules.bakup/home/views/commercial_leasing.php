
<div class="banner inner-b cm-ls wow fadeInDown cl">
	<div class="banner-part1" style="background-image: url(<?php echo base_url();?>public/front/images/banner-leasing.jpg);">
		<img style="display: none;" src="<?php echo base_url();?>public/front/images/banner-leasing.jpg" alt=""/>
		<!--<div class="cust-leasing-slider owl-carousel owl-theme wow fadeInDown">
			<div class="item">
				<img src="<?php echo base_url();?>public/front/images/c12.jpg" alt=""/>
			</div>
			<div class="item">
				<img src="<?php echo base_url();?>public/front/images/c1.jpg" alt=""/>
			</div>
			<div class="item">
				<img src="<?php echo base_url();?>public/front/images/c5.jpg" alt=""/>
			</div>
			<div class="item">
				<img src="<?php echo base_url();?>public/front/images/c6.jpg" alt=""/>
			</div>
			<div class="item">
				<img src="<?php echo base_url();?>public/front/images/c7.jpg" alt=""/>
			</div>
			<div class="item">
				<img src="<?php echo base_url();?>public/front/images/c8.jpg" alt=""/>
			</div>
			<div class="item">
				<img src="<?php echo base_url();?>public/front/images/c9.jpg" alt=""/>
			</div>
			<div class="item">
				<img src="<?php echo base_url();?>public/front/images/c10.jpg" alt=""/>
			</div>
			<div class="item">
				<img src="<?php echo base_url();?>public/front/images/c11.jpg" alt=""/>
			</div>
			<div class="item">
				<img src="<?php echo base_url();?>public/front/images/c13.jpg" alt=""/>
			</div>
			<div class="item">
				<img src="<?php echo base_url();?>public/front/images/c14.jpg" alt=""/>
			</div>
			<div class="item">
				<img src="<?php echo base_url();?>public/front/images/c15.jpg" alt=""/>
			</div>
			<div class="item">
				<img src="<?php echo base_url();?>public/front/images/c16.jpg" alt=""/>
			</div>
		</div>-->
		<div class="caption2">
			<ul>
				<li><p>Thoughtful commercial spaces</p></li>
				<li><p>Pleasant working environment</p></li>
				<li><p>Incredible retail experience</p></li>
			</ul>
		</div>
	</div>
</div>
<div class="banner-text inner wow fadeInDown" data-wow-delay="100ms">
	<div class="container">
		<div class="col-lg-12 col-md-12 col-sm-12 banner-text-main">
			<div class="col-lg-3 col-md-3 col-sm-3 banner-text-left">
				<h3 class="wow fadeInDown" data-wow-delay="200ms">Commercial Leasing</h3>
			</div>
			<div class="col-lg-9 col-md-9 col-sm-9 banner-text-right">
				<div class="bre">
				<ul class="wow fadeInDown" data-wow-delay="300ms">
					<li><a href="<?php echo base_url()?>">home</a></li>       
					<li><span>commercial leasing</span></li> 
				</ul>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="leasing-area">
	<div class="container">
		<h2>Providing professional leasing services for Rajdarbar Limited’s commercial portfolio</h2>
		<p class="top-text">Considering the ever increasing demand for commercial rentals and business spaces in real estate, we offers professional leasing services to our investors and patrons of our commercial properties</p>
		<div class="row">
			<div class="col-md-8 col-sm-8 rc-right">
				<div class="box left">
					<div class="panel-body">
						<div class="row">
							<div class="col-lg-6 col-md-6">
								<div class="img"><a href="<?php echo base_url()?>home/global_foyer_gurgaon"><img class="img-sr" src="<?php echo base_url();?>public/front/images/foyer-gurgaon-th-n.jpg" alt=""/></a></div>
								<div class="global-foyer">
									<div class="pull-left">
										<img src="<?php echo base_url();?>public/front/images/kk.png" alt=""/>
									</div>
									<div class="pull-right">
										<p>Gurgaon</p>
									</div>
									<div class="clearfix"></div>
									<h2>THE WORKPLACE OF THE FUTURE</h2>
									<h3>Global Foyer is Gurgaon's first luxury mall. It offers premium space on Gurgaon's most sought after locale - the Golf Course Road. </h3>
									<a class="a-one" href="<?php echo base_url()?>home/global_foyer_gurgaon">Explore More</a>
									<!--<a class="a-two p_enquiry" alt="global_foyer_gurgaon" href="<?php echo base_url()?>home/contact_us">Enquiry Now</a>-->
								</div>
							</div>
							<div class="col-lg-6 col-md-6">
								<div class="img"><a href="<?php echo base_url()?>home/global_foyer_palam_vihar"><img class="img-sr" src="<?php echo base_url();?>public/front/images/leasin-palamvihar.jpg" alt=""/></a></div>
								<div class="global-foyer">
									<div class="pull-left">
										<img src="<?php echo base_url();?>public/front/images/kk1.png" alt=""/>
									</div>
									<div class="pull-right">
										<p>Palam Vihar</p>
									</div>
									<div class="clearfix"></div>
									<h2>THE WORKPLACE OF THE FUTURE</h2>
									<h3>Palam vihar is home to thousands of proud residents. Well developed in every aspect, this buzzing township has been bereft of a global standard commercial</h3>
									<a class="a-one" href="<?php echo base_url()?>home/global_foyer_palam_vihar">Explore More</a>
									<!--<a class="a-two p_enquiry" alt="global_foyer_palam_vihar" href="<?php echo base_url()?>home/contact_us">Enquiry Now</a>-->
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-md-4 col-sm-4">
				<div class="box right">
					<ul class="plan-list">
						<li>
							<a href="<?php echo base_url();?>public/front/images/pdf/the-Foyer-Book-final.pdf" target="_blank">
								View brochure and floor plans for Gurgaon Commercial Leasing
							</a>
						</li>
						<li>
							<a href="<?php echo base_url();?>public/front/images/pdf/Global-Foyer-@-Palam-Vihar.pdf" target="_blank">
								View brochure and floor plans for Gurgaon Palam Vihar Commercial Leasing
							</a>
						</li>
					</ul>

					<div class="add-info">
						<p><strong>Leasing Specialists and Patrons interested, please contact :</strong></p>
						<h3>Leasing Enquires</h3>
						<address>Global Foyer, Palam Vihar, Sector-1,<br> Gurgaon (Haryana)</address>
						<div class="call"><a href="callto:919811447786">+91 9811447786</a></div>
						<div class="mail"><a href="mailto:leasing@rajdarbarrealty.com">leasing@rajdarbarrealty.com</a></div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>