
<!--popup box-->
<div class="black11"></div>
<div class="popbox"><div class="popclose">X</div>
	<!--<h2>Global Foyer Golf Course Road: Key Highlights</h2>-->
	<ul>
		<li>Fully furnished executive Suites</li>
		<li>Concierge and desk service</li>
		<li>Health club &amp; gym</li>
		<li>Swimming pool</li>
		<li>Spa centre &amp; power yoga centre</li>
		<li>Grand entrance lobby</li>
		<li>Intelligent air-conditioning and lighting systems</li>
		<li>Multi-tier business security - full CCTV</li>
		<li>Easy access to shops and retail</li>
		<li>Excellent connectivity from domestic &amp; international airport</li>
		<li>100% power backup</li>
	</ul>
</div>
<!--end popup-->

<div class="banner inner wow fadeInDown">
	
	<div class="banner-slider owl-carousel owl-theme">
		<div class="item">
			<div class="banner-part" style="background-image: url(<?php echo base_url();?>public/front/images/gallery-pv-10.jpg);">
				<img src="<?php echo base_url();?>public/front/images/gallery-pv-10.jpg" alt="inner banner" style="display: none;" />
			</div>
		</div>
	</div>
	
	<div class="rotate-part">
		<div class="container">
			<div class="banner-bottom">
				<div class="spaces pull-left banner-bottom-left wow fadeInLeft" data-wow-delay="100ms">
					<img src="<?php echo base_url();?>public/front/images/commercial-foyer.jpg" alt=""/>
				</div>
				<div class="spaces pull-right banner-bottom-right">
					<a class="wow fadeInDown enquiry" data-wow-delay="100ms" href="<?php base_url()?>contact_us"><img src="<?php echo base_url();?>public/front/images/icon-enquiry1.png" alt=""/> Enquiry</a>
					<a class="wow fadeInDown" data-wow-delay="200ms" target="_blank" href="<?php echo base_url();?>public/front/images/pdf/Global-Foyer-@-Palam-Vihar.pdf"><img src="<?php echo base_url();?>public/front/images/eb.png" alt=""/> E-Brochure</a>
				</div>
			</div>
		</div>
	</div>
</div>

	<div class="banner-text inner bre-slider">
		<div class="container">
			<div class="col-lg-12 col-md-12 col-sm-12 banner-text-main">
				<div class="col-lg-4 col-md-4 col-sm-4 banner-text-left">
					<h3 class="wow fadeInDown" data-wow-delay="100ms">Palam Vihar</h3>
				</div>
				<div class="col-lg-8 col-md-8 col-sm-8 banner-text-right">
					<div class="bre">
					<ul class="wow fadeInLeft" data-wow-delay="100ms">
						<li><a href="<?php echo base_url() ?>">home</a></li>     
						<li>projects</li>      
						<li>commercial</li>      
						<li><span>global foyer</span></li> 
					</ul>
					</div>
				</div>
			</div>
		</div>
	</div>
	
	
	<section class="overview commercial">
		<div class="container">
			<div class="col-lg-12 col-md-12 col-sm-12 overview-main">
				<!--<h2 class="wow fadeInDown" data-wow-delay="100ms">Overview</h2>-->
				<h3 class="wow fadeInDown" data-wow-delay="100ms">The Perfect Address for Global Brands</h3>
				<div class="row">
					<div class="col-md-6 col-sm-6">
						<div class="box left">
							<p class="wow fadeInDown" data-wow-delay="200ms">Global Foyer :  well developed in every aspect, this buzzing feature has been bereft of a global standard commercial landmark of its own. But, not for long! After the success of Global Foyer, Gurgaon, we are now ready to give Palam Vihar its very own Global Foyer.</p>
							<p class="wow fadeInDown" data-wow-delay="100ms">Strategically located in the heart of Palam Vihar, Global Foyer will become a landmark address like no other - complete with retail, commercial and serviced apartments. It is already seen as a golden opportunity for businesses and investors alike. At par with international standards, only the best players in the industry have been roped in for the design and execution of the serviced apartments and commercial plaza. </p>
							<h4 class="wow fadeInDown" data-wow-delay="100ms">In short, now, Palam Vihar too will go global in its shopping and fine dining experience!  </h4>
						</div>
					</div>
					<div class="col-md-6 col-sm-6">
						<div class="box right">
							<img src="<?php echo base_url();?>public/front/images/overview-palam-vihar.png" alt="" />
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	
	<section class="plan-feature">
		<div class="container">
			<div class="col-lg-12 col-md-12 col-sm-12 plan-feature-main">
				<h2 class="wow fadeInDown" data-wow-delay="100ms">Plans &amp; Features</h2>
				<div class="tab-system">

					<button type="button" data-toggle="#product-tabs-area" class="navbar-toggle open">
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span> 
					</button>

				  <!-- Nav tabs -->
				  <ul class="nav nav-tabs nav-tabs1 wow fadeInDown" data-wow-delay="100ms" id="product-tabs-area" role="tablist">
				  	<li role="presentation" class="active"><a href="#tab1" role="tab" data-toggle="tab">Elevation</a></li>
					<li role="presentation"><a href="#tab2" role="tab" data-toggle="tab">Key Highlight</a></li>
					<li role="presentation"><a href="#tab3" role="tab" data-toggle="tab">Floor Plans</a></li>
					<li role="presentation"><a href="#tab4" aria-controls="home" role="tab" data-toggle="tab">Specifications</a></li>
					<li role="presentation"><a href="#tab5" role="tab" data-toggle="tab">Photo Gallery</a></li>
					<li role="presentation"><a href="#tab6" role="tab" data-toggle="tab">Site Map</a></li>
					<li role="presentation"><a href="#tab7" role="tab" data-toggle="tab">Location Map</a></li>
				  </ul>

				  <!-- Tab panes -->
				  <div class="tab-content">
					<div class="tab-pane wow fadeInDown active" data-wow-delay="300ms" id="tab1">
						<div class="elev-slider owl-carousel owl-theme">
							<div class="item wow fadeInDown" data-wow-delay="130ms">
								<div class="shops-text">
									<img src="<?php echo base_url();?>public/front/images/gallery-pv-8.jpg" alt=""/>
								</div>
							</div>
							<!--<div class="item wow fadeInDown" data-wow-delay="130ms">
								<div class="shops-text">
									<img src="<?php echo base_url();?>public/front/images/gmpv-slide-1.jpg" alt=""/>
								</div>
							</div>-->
							<div class="item wow fadeInDown" data-wow-delay="130ms">
								<div class="shops-text">
									<img src="<?php echo base_url();?>public/front/images/gallery-pv-5.jpg" alt=""/>
								</div>
							</div>
						</div>	
					</div>
					<div class="tab-pane wow fadeInDown" data-wow-delay="300ms" id="tab2">
						<div class="key-heighlight">
							<ul class="key-list">
								<li>
									<div class="img">
										<a href="javascript:;">
											<img class="img1" src="<?php echo base_url();?>public/front/images/icon/icon-key-highlight-1.png" alt="keyhilight" />
											<img class="img2" src="<?php echo base_url();?>public/front/images/icon/iconw-key-highlight-1.png" alt="keyhilight" />
										</a>
									</div>
									<div class="desc">
										<p>Fully Furnished <br>Executive Suites</p>
									</div>
								</li>
								
								<li>
									<div class="img">
										<a href="javascript:;">
											<img class="img1" src="<?php echo base_url();?>public/front/images/icon/icon-yoga.png" alt="keyhilight" />
											<img class="img2" src="<?php echo base_url();?>public/front/images/icon/iconw-yoga.png" alt="keyhilight" />
										</a>
									</div>
									<div class="desc">
										<p>Health Club &amp; Gym</p>
									</div>
								</li>
								<li>
									<div class="img">
										<a href="javascript:;">
											<img class="img1" src="<?php echo base_url();?>public/front/images/icon/icon-key-highlight-4.png" alt="keyhilight" />
											<img class="img2" src="<?php echo base_url();?>public/front/images/icon/iconw-key-highlight-4.png" alt="keyhilight" />
										</a>
									</div>
									<div class="desc">
										<p>Swimming Pool</p>
									</div>
								</li>
								
								<li>
									<div class="img">
										<a href="javascript:;">
											<img class="img1" src="<?php echo base_url();?>public/front/images/icon/icon-key-highlight-6.png" alt="keyhilight" />
											<img class="img2" src="<?php echo base_url();?>public/front/images/icon/iconw-key-highlight-6.png" alt="keyhilight" />
										</a>
									</div>
									<div class="desc">
										<p>Air-conditioning <br>and lighting systems</p>
									</div>
								</li>
								<li>
									<div class="img">
										<a href="javascript:;">
											<img class="img1" src="<?php echo base_url();?>public/front/images/icon/icon-key-highlight-7.png" alt="keyhilight" />
											<img class="img2" src="<?php echo base_url();?>public/front/images/icon/iconw-key-highlight-7.png" alt="keyhilight" />
										</a>
									</div>
									<div class="desc">
										<p>Multi-tier business <br>security - full CCTV</p>
									</div>
								</li>
								<li>
									<div class="img">
										<a href="javascript:;">
											<img class="img1" src="<?php echo base_url();?>public/front/images/icon/icon-key-highlight-8.png" alt="keyhilight" />
											<img class="img2" src="<?php echo base_url();?>public/front/images/icon/iconw-key-highlight-8.png" alt="keyhilight" />
										</a>
									</div>
									<div class="desc">
										<p>Easy access to <br>shops and retail</p>
									</div>
								</li>
								<li>
									<div class="img">
										<a href="javascript:;">
											<img class="img1" src="<?php echo base_url();?>public/front/images/icon/icon-key-highlight-9.png" alt="keyhilight" />
											<img class="img2" src="<?php echo base_url();?>public/front/images/icon/iconw-key-highlight-9.png" alt="keyhilight" />
										</a>
									</div>
									<div class="desc">
										<p>Excellent connectivity <br>from domestic &amp; <br>international airport</p>
									</div>
								</li>
								<li>
									<div class="img">
										<a href="javascript:;">
											<img class="img1" src="<?php echo base_url();?>public/front/images/icon/icon-key-highlight-10.png" alt="keyhilight" />
											<img class="img2" src="<?php echo base_url();?>public/front/images/icon/iconw-key-highlight-10.png" alt="keyhilight" />
										</a>
									</div>
									<div class="desc">
										<p>100% power <br>backup</p>
									</div>
								</li>
							</ul>
							<p class="more-text">For Detailed Key Highlights Please <a href="javascript:;" class="more-vv">Click Here</a></p>
						</div>
					</div>
					<div class="tab-pane wow fadeInDown docs-pictures" data-wow-delay="300ms" id="tab3">
						<div class="floorplan">
							<div id="floorplan-slider" class="owl-carousel owl-theme">
							    <div class="item">
							    	<div class="box">
								    	<div class="img">
											<span><img data-original="<?php echo base_url();?>public/front/images/pv-plan-floor-ground.jpg" src="<?php echo base_url();?>public/front/images/pv-plan-floor-ground.jpg" alt="Ground Floor Plan" /></span>
										</div>
										<div class="buttons">
											<a href="javascript:;" class="btn">Ground Floor Plan</a>
										</div>
									</div>
							    </div>
							     <div class="item active">
							    	<div class="box">
								    	<div class="img">
											<span><img data-original="<?php echo base_url();?>public/front/images/pv-plan-floor-first.jpg" src="<?php echo base_url();?>public/front/images/pv-plan-floor-first.jpg" alt="Ground Floor Plan" /></span>
										</div>
										<div class="buttons">
											<a href="javascript:;" class="btn">First Floor Plan</a>
										</div>
									</div>
							    </div>
							     <div class="item">
							    	<div class="box">
								    	<div class="img">
											<span><img data-original="<?php echo base_url();?>public/front/images/pv-plan-floor-second.jpg" src="<?php echo base_url();?>public/front/images/pv-plan-floor-second.jpg" alt="Ground Floor Plan" /></span>
										</div>
										<div class="buttons">
											<a href="javascript:;" class="btn">Second Floor Plan</a>
										</div>
									</div>
							    </div>
							     <div class="item">
							    	<div class="box">
								    	<div class="img">
											<span><img data-original="<?php echo base_url();?>public/front/images/floor-plan-pv-bg3.jpg" src="<?php echo base_url();?>public/front/images/floor-plan-pv-th3.jpg" alt="Ground Floor Plan" /></span>
										</div>
										<div class="buttons">
											<a href="javascript:;" class="btn">Third Floor Plan</a>
										</div>
									</div>
							    </div>
							    <div class="item">
							    	<div class="box">
								    	<div class="img">
											<span><img data-original="<?php echo base_url();?>public/front/images/floor-plan-pv-bg4.jpg" src="<?php echo base_url();?>public/front/images/floor-plan-pv-th4.jpg" alt="Ground Floor Plan" /></span>
										</div>
										<div class="buttons">
											<a href="javascript:;" class="btn">Fourth Floor Plan</a>
										</div>
									</div>
							    </div>
							    <div class="item">
							    	<div class="box">
								    	<div class="img">
											<span><img data-original="<?php echo base_url();?>public/front/images/floor-plan-pv-bg5.jpg" src="<?php echo base_url();?>public/front/images/floor-plan-pv-th5.jpg" alt="Ground Floor Plan" /></span>
										</div>
										<div class="buttons">
											<a href="javascript:;" class="btn">Fifth Floor Plan</a>
										</div>
									</div>
							    </div>
							    <div class="item">
							    	<div class="box">
								    	<div class="img">
											<span><img data-original="<?php echo base_url();?>public/front/images/floor-plan-pv-bg6.jpg" src="<?php echo base_url();?>public/front/images/floor-plan-pv-th6.jpg" alt="Ground Floor Plan" /></span>
										</div>
										<div class="buttons">
											<a href="javascript:;" class="btn">Six Floor Plan</a>
										</div>
									</div>
							    </div>
							</div>
						</div>
										
					</div>
					<div class="tab-pane wow fadeInDown" data-wow-delay="300ms" id="tab4">
						<div id="spec-slider" class="owl-carousel owl-theme">
						    <div class="item">
						    	<div class="tab-text-part">
									<h3><img src="<?php echo base_url();?>public/front/images/icon1.png" alt=""/> FLOORING</h3>
									<p><span>Lounges:</span> Imported Marble</p>
									<p><span>Retiring Room:</span> Wooden Flooring</p>
									<p><span>Kitchenette:</span> Imported Marble</p>
									<p><span>Bathroom:</span> Ceramic Tiles</p>
									<p><span>Entrance:</span> Vitrified Tiles</p>
									<p><span>Lift Lobby:</span> Vitrified Tiles</p>
									<p><span>Staircase/Lobby:</span> Combination Of One Or More Indian Granite/Kota/Carpet</p>
									<p><span>Corridor:</span> Vitrified Tiles</p>
								</div>
						    </div>
						    <div class="item">
						    	<div class="tab-text-part wall-icon">
									<h3><img src="<?php echo base_url();?>public/front/images/icon2.png" alt=""/> WALLS</h3>
									<p><span>Lounges:</span> Plastic Emulsion Paint</p>
									<p><span>Retiring Room:</span> Plastic Emulsion Paint</p>
									<p><span>Kitchenette:</span> 2&rsquo;High Ceramic Tiles Above Counter &amp; Plastic Emulsion Paint On Remaining</p>
									<p><span>Bathroom:</span> Ceramic Tiles Up To 7&rsquo; High</p>
									<p><span>Entrance:</span> Plastic Emulsion Paint</p>
									<p><span>Lift Lobby:</span> 7&rsquo; Granite Cladding Round Lift Entrance &amp; Plastic Emulsion Paint On Remaining</p>
									<p><span>Corridor:</span> Plastic Emulsion Paint</p>
								</div>
						    </div>
						    <div class="item">
						    	<div class="tab-text-part">
									<h3><img src="<?php echo base_url();?>public/front/images/icon3.png" alt=""/> CEILING</h3>
									<p><span>Lounges:</span> Plastic Emulsion Paint</p>
									<p><span>Retiring Room:</span> Plastic Emulsion Paint</p>
									<p><span>Kitchenette:</span> Plastic Emulsion Paint</p>
									<p><span>Bathroom:</span> Plastic Emulsion Paint</p>
									<p><span>Entrance:</span> Plastic Emulsion Paint</p>
									<p><span>Lift Lobby:</span> Plastic Emulsion Paint</p>
									<p><span>Corridor:</span> Plastic Emulsion Paint</p>
								</div>
						    </div>
						    <div class="item">
						    	<div class="tab-text-part">
									<h3><img src="<?php echo base_url();?>public/front/images/icon3.png" alt=""/> Door</h3>
									<p><span>Lounges:</span>  H/W Door Frame With Flush Door/p>
									<p><span>Retiring Room:</span> H/W Door Frame With Flush Door And UPVC Windows With Sliding Shutters</p>
									<p><span>Kitchenette:</span> H/W Door Frame With Flush Door</p>
									<p><span>Bathroom:</span> H/W Door Frame With Flush Door</p>
									<p><span>Entrance:</span> H/W Door Frame With Flush Door</p>
									<p><span>Lift Lobby:</span> S Finished Lift Doors</p>
								</div>
						    </div>
						    <div class="item">
						    	<div class="tab-text-part">
									<h3><img src="<?php echo base_url();?>public/front/images/icon3.png" alt=""/> Fittings/Fixtures</h3>
									<p><span>Lounges:</span> Contemporary Lounge Seating With Coffee Table,AC</p>
									<p><span>Retiring Room:</span> Plastic Emulsion Paint</p>
									<p><span>Kitchenette:</span> 1 Queen Size Bed With Mattress And Linen, others.</p>
									<p><span>Bathroom:</span> CP Fittings, Single Bowl SS Sink, </p>
									<p><span>Entrance:</span> Reception Desk</p>
									<p><span>Lift Lobby:</span> Lift Cabins Will Have Granite Flooring And Panels In Dado</p>
								</div>
						    </div>
						</div>
					</div>
					<div class="tab-pane wow fadeInDown" data-wow-delay="300ms" id="tab5">
						
						<div class="elev-slider owl-carousel owl-theme wow fadeInUp">
							
							<div class="item">
								<div class="shops-text">
									<img src="<?php echo base_url();?>public/front/images/gallery-pv-11.jpg" alt="palam vihar" />
								</div>
							</div>

							<div class="item">
								<div class="shops-text">
									<img src="<?php echo base_url();?>public/front/images/gallery-pv-12.jpg" alt="palam vihar" />
								</div>
							</div>

							<div class="item">
								<div class="shops-text">
									<img src="<?php echo base_url();?>public/front/images/gallery-pv-13.jpg" alt="palam vihar" />
								</div>
							</div>

							<div class="item">
								<div class="shops-text">
									<img src="<?php echo base_url();?>public/front/images/gallery-pv-14.jpg" alt="palam vihar" />
								</div>
							</div>

							<div class="item">
								<div class="shops-text">
									<img src="<?php echo base_url();?>public/front/images/gallery-pv-15.jpg" alt="palam vihar" />
								</div>
							</div>

							<div class="item">
								<div class="shops-text">
									<img src="<?php echo base_url();?>public/front/images/gallery-pv-16.jpg" alt="palam vihar" />
								</div>
							</div>

							<div class="item">
								<div class="shops-text">
									<img src="<?php echo base_url();?>public/front/images/gallery-pv-7.jpg" alt="palam vihar" />
								</div>
							</div>							
							
						</div>
					</div>
					<div class="tab-pane wow fadeInDown docs-pictures" data-wow-delay="300ms" id="tab6">
						<div class="Walkthrough siteplan">
						<span><img data-original="<?php echo base_url();?>public/front/images/pv-plan-floor-ground.jpg" src="<?php echo base_url();?>public/front/images/pv-plan-floor-ground.jpg" alt="Location" /></span>
						</div>	
					</div>
					<div class="tab-pane wow fadeInDown" data-wow-delay="300ms" id="tab7">
						<!--<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3505.9361203327508!2d77.03835001434864!3d28.511569582465665!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x390d1988fe713649%3A0x17d1820f3d0fc1ce!2sGlobal+Foyer+Palam+Vihar!5e0!3m2!1sen!2sin!4v1530708670427" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>-->
						<iframe src="https://www.google.co.in/maps/d/embed?mid=1yWwofqrXulZilY8eWnI7BZ7xT07939id" width="600" height="450" style="border:0"></iframe>
					</div>
				  </div>
				</div>
			</div>
		</div>
	</section>
	
	

	
	<section class="downloads">
		<div class="container">
			<div class="col-lg-12 col-md-12 col-sm-12 downloads-main">
				<h2 class="wow fadeInDown" data-wow-delay="100ms">Downloads</h2>
				<div class="downloads-icon">
					<div class="downloads-icon-text">
						<a class="one-a" href="<?php echo base_url();?>public/front/images/pdf/Global-Foyer-@-Palam-Vihar.pdf" target="_blank"><p class="wow fadeInLeft" data-wow-delay="100ms">E-Brochure</p></a>					
					</div>
					<div class="downloads-icon-text">
						<a class="one-b" href="<?php echo base_url();?>public/front/images/pdf/Application-Form-Palam-Vihar-new.pdf" target="_blank"><p class="wow fadeInLeft" data-wow-delay="200ms">Application Form</p></a>		
					</div>
					<div class="downloads-icon-text">
						<a class="one-c one-e" href="javascript:;"><p class="wow fadeInLeft" data-wow-delay="300ms">Payment Plan</p></a><span class="tooltiptext">On Request</span>				
					</div>
					<div class="downloads-icon-text">
						<a class="one-d one-e" href="javascript:;"><p class="wow fadeInLeft" data-wow-delay="400ms">Price List</p></a><span class="tooltiptext">On Request</span>				
					</div>
				</div>
			</div>
		</div>
	</section>
	
	<section class="loans">
		<div class="container">
			<div class="col-lg-12 col-md-12 col-sm-12 loans-main">
				<h2 class="wow fadeInDown" data-wow-delay="100ms">Home Loans Available By</h2>
				<div class="loans-slider owl-carousel owl-theme wow fadeInDown" data-wow-delay="300ms">
					<div class="item">
						<div class="loans-text">
							<img src="<?php echo base_url();?>public/front/images/bank1.jpg" alt=""/>
						</div>
					</div>
					<div class="item">
						<div class="loans-text">
							<img src="<?php echo base_url();?>public/front/images/bank2.jpg" alt=""/>
						</div>
					</div>
					<div class="item">
						<div class="loans-text">
							<img src="<?php echo base_url();?>public/front/images/bank3.jpg" alt=""/>
						</div>
					</div>
					<div class="item">
						<div class="loans-text">
							<img src="<?php echo base_url();?>public/front/images/bank4.jpg" alt=""/>
						</div>
					</div>
					<div class="item">
						<div class="loans-text">
							<img src="<?php echo base_url();?>public/front/images/bank5.jpg" alt=""/>
						</div>
					</div>
				</div>	
			</div>
		</div>
	</section>
	
	
	<section class="location-map">
		<div class="container">
			<div class="map-area">
				<h2 class="wow fadeInDown" data-wow-delay="100ms">Location</h2>
				<ul class="location-map-main">
					<li>
						<h3 class="wow fadeInDown" data-wow-delay="200ms">Corporate Office</h3>
						<p class="wow fadeInDown" data-wow-delay="300ms">303, 3rd Floor, Global Foyer, Golf Course Road, <br>Sector-43, Gurgaon -122002, (Haryana) India</p>
						<p class="wow fadeInDown" data-wow-delay="100ms">
							<span>Email :</span> 
							<a href="mailto:info@globalrealty.com">info@rajdarbarrealty.com</a>
							<a href="mailto:customercare@globalrealty.com">customercare@rajdarbarrealty.com</a>
							<a href="mailto:sales@globalrealty.com">sales@rajdarbarrealty.com</a>
						</p>
						<p class="wow fadeInDown" data-wow-delay="400ms"><span></span> <a href="javascript:;"> </a> <a href="javascript:;"></a></p>
					</li>
					<li>
						<h3 class="wow fadeInDown" data-wow-delay="200ms">Site Office</h3>
						<p class="wow fadeInDown" data-wow-delay="300ms">Rajdarbar Foyer, Behind Ansal Plaza, Palam Vihar, <br>Sector-1, Gurgaon (Haryana)</p>
						<p class="wow fadeInDown" data-wow-delay="100ms">
							<span></span> 
							<a href="javascript:;"></a>
							<a href="javascript:;"></a>
							<a href="javascript:;"></a>
						</p>
						<p class="wow fadeInDown" data-wow-delay="400ms"><span>Tel :</span> <a href="callto:918373912275">+91 8373912275 </a> <a href="callto:918373912276">+91 8373912276</a></p>
					</li>
				</ul>
				<div class="map-location wow fadeInDown google-map-wrap" >
					<div class="map">
						<iframe class="g-map" src="https://www.google.com/maps/d/embed?mid=1wcLmJzu5etrXpkSCOoaHb89jwvmZxK5r" width="640" height="480"></iframe>
					</div>
				</div>
			</div>
		</div>
		
	</section>