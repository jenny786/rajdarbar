<div class="thankyou">
	<div class="container">
		<div class="top-img">
			<img src="<?php echo base_url();?>public/front/images/thankyou.png" alt="thankyou" />
		</div>
		<div class="view-msg-box">
			<p>We will be in touch shortly <br>to let you know we received your informations</p>
			<div class="button">
				<a href="<?php echo base_url()?>" class="btn btn-thank">View Homepage</a>
			</div>
		</div>
	</div>
</div>