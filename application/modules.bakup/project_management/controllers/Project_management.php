<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Project_management extends CI_Controller {
	
	public function __construct() {
        parent::__construct();
		$this->load->model('App_project_model');
        if(checkSessionData() == false)
	    { 
	      redirect('admin'); 
	    }
    }



	function index(){ 	   
		$data['recordAll'] = $this->App_project_model->getgallery();
		$data['page_title'] = 'Project Gallery';
		$data['main_content'] = 'index';
		$this->load->view('admin/layout', $data);	
	}
	
	
	function gallery($page="" , $id=""){
		
		$data['project'] = array("1"=>"Global foyer gurgaon","2"=>"Global foyer palam vihar","3"=>"Global spaces hisar","4"=>"Global spaces sirsa","5"=>"Global spaces agra","6"=>"Global spaces karnal phase1","7"=>"Global spaces karnal phase2");
				switch ($page) {
					case 'create':

				    $this->load->library('form_validation');
					$this->form_validation->set_rules('project_id', 'Please select project', 'required|encode_php_tags|xss_clean');
			
					$this->form_validation->set_error_delimiters('<div class="error">', '</div>');
					
					if ($this->form_validation->run() == TRUE){
						if(isset($_POST['submitBTN'])){
						unset($_POST['submitBTN']);
						$post_value = $this->input->post();
					
			         if(!empty($_FILES['img']['name'])){
			                $config['upload_path'] = 'images/project_gallery/';
			                $config['allowed_types'] = 'jpg|png|jpeg|gif|';
							$config['file_name'] = time() . date('Ymd');
							$this->load->library('upload');
							$this->upload->initialize($config);				
			                if($this->upload->do_upload('img')){
			                    $uploadData = $this->upload->data();
								$post_value['image'] = $uploadData['file_name'];
			                }else{ 
								$this->session->set_flashdata('error_image', $this->upload->display_errors());
								redirect('project_management/gallery/create');
							} 
			            }else{
			                $post_value['image'] = '';
			            }


			        	$rtrn = addRecord('tbl_project_gallery', $post_value);
					
						if( $rtrn > 0 ){
							setSuccessFlashData('File added successfully');
							redirect('project_management/gallery');
						}else{
							setErrorFlashData('Something goes wrong, Please try again');
							redirect('project_management/gallery');
						}	
			        } } else {
					//echo "<pre>"; pr($data); exit;
			      	
					    $data['page_title'] = 'Add Project Gallery';
						$data['main_content'] = 'galleryadd';
						$this->load->view('admin/layout', $data);


					}


				   break;

		           case 'update':
		            
			         $data['rtrnData'] = getRecordById('tbl_project_gallery', array('id'=>$id) );

		             $this->load->library('form_validation');
				 	$this->form_validation->set_rules('project_id', 'Please select project', 'required|encode_php_tags|xss_clean');
			
				
					$this->form_validation->set_error_delimiters('<div class="error">', '</div>');
					
					if ($this->form_validation->run() == TRUE){
						if(isset($_POST['submitBTN'])){
						unset($_POST['submitBTN']);
						$post_value = $this->input->post();
					
			         if(!empty($_FILES['img']['name'])){
			                $config['upload_path'] = 'images/project_gallery/';
			                $config['allowed_types'] = 'jpg|png|jpeg|gif|';
							$config['file_name'] = time() . date('Ymd');
							$this->load->library('upload');
							$this->upload->initialize($config);				
			                if($this->upload->do_upload('img')){
			                    $uploadData = $this->upload->data();
								$post_value['image'] = $uploadData['file_name'];
			                }else{ 
								$this->session->set_flashdata('error_image', $this->upload->display_errors());
								redirect('project_management/gallery/update/'.$id);
							} 
			            }else{
			                $post_value['image'] = $data['rtrnData']->image;
			            }
                      
			            $rtrn = updateRecord('tbl_project_gallery', array('id'=>$id), $post_value);
						if( $rtrn > 0 ){
							setSuccessFlashData('File added successfully');
							redirect('project_management/gallery');
						}else{
							setErrorFlashData('Something goes wrong, Please try again');
							redirect('project_management/gallery');
						}	
			        } } else {
					//echo "<pre>"; pr($data); exit;
                   
                  
						$data['page_title'] = 'Update Project Gallery';
						$data['main_content'] = 'galleryadd';
						$this->load->view('admin/layout', $data);

					}

		         
			       break;

			    case 'status':
			          $get_status = updateRecordStatus('tbl_project_gallery', $_POST);
			          $return = array("status"=>"true","message"=>"status changesuccessfully.");
			          echo json_encode($return);
			       	break;	
		         case 'delete':
		             $rtrn = deleteRecord( 'tbl_project_gallery', array('id'=>$id) );
						if($rtrn){
							setSuccessFlashData('Record deleted successfully');
							redirect('project_management/gallery');
					}
		         	break;

				 default:
					$data['recordAll'] = $this->App_project_model->getgallery();
					$data['page_title'] = 'Project Gallery Management';
					$data['main_content'] = 'index';
					$this->load->view('admin/layout', $data);	
					break;
				}
				
		    }
		



		public function leasing()
		{
            
              $data['rtrnData'] = getRecordById('tbl_leasing', array('id'=>1) );

		             $this->load->library('form_validation');
				 	$this->form_validation->set_rules('project_id', 'Please select project', 'required|encode_php_tags|xss_clean');
			
				
					$this->form_validation->set_error_delimiters('<div class="error">', '</div>');
					
					if ($this->form_validation->run() == TRUE){
						if(isset($_POST['submitBTN'])){
						unset($_POST['submitBTN']);
						$post_value = $this->input->post();
					 
			            $rtrn = updateRecord('tbl_project_gallery', array('id'=>$id), $post_value);
						if( $rtrn > 0 ){
							setSuccessFlashData('File added successfully');
							redirect('project_management/gallery');
						}else{
							setErrorFlashData('Something goes wrong, Please try again');
							redirect('project_management/gallery');
						}	
			        } } else {
					//echo "<pre>"; pr($data); exit;
                   
                  
						$data['page_title'] = 'Update Leasing';
						$data['main_content'] = 'leasing';
						$this->load->view('admin/layout', $data);

					}


		}
}
