<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class App_project_model extends CI_Model{
	
	function __construct() {
        parent::__construct();
        
    }
	
	public function getgallery() {		
        $this->db->select("*");
        $this->db->from('tbl_project_gallery');
        $this->db->order_by('id', 'DESC');
        return $this->db->get()->result();     
    }
	

}
