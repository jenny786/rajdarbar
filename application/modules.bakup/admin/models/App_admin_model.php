<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class App_admin_model extends CI_Model{
	
	function __construct() {
        parent::__construct();
        
    }

	protected $tbl_users = 'tbl_users';
	
	public function userAutheticate($username=false, $password=false){
		if($username){
			$this->db->where('user_password', $password);
			$this->db->where('user_name', $username);
			$this->db->or_where('user_email', $username);
			$this->db->or_where('user_mobile', $username);
			
			$query = $this->db->get($this->tbl_users);
			if($query->num_rows() == 1){
				$userDetail = $query->row();
				$return['status'] = 'okay';
				$return['userDetail'] = $userDetail;				
			}else{
				$return['status'] = 'failure';
			}
		}else{
			$return['status'] = 'failure';
		}
		return $return;
	}
	
	public function get_user_email($usesrname){
		$this->db->select('*');
		$this->db->from('tbl_users');
		$this->db->where('user_name', $usesrname);
		$this->db->where('user_is_active', 1);
		$result =  $this->db->get()->row();
		if(!empty($result)){
			$return['status'] = "true";
			$return['list'] = $result;
		}else{
			$return['status'] = "false";
		}
		return $return;
	}
	
	
}
