<?php
if(checkSessionData() == true)
    { 
      redirect('dashboard'); 
    }
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>ACE Ltd | Log in</title>
	<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
	<link rel="stylesheet" href="<?php echo base_url();?>public/admin/css/bootstrap.min.css">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
	<link rel="stylesheet" href="<?php echo base_url();?>public/admin/css/AdminLTE.min.css">
	<link rel="stylesheet" href="<?php echo base_url();?>public/admin/css/blue.css">
	<style>
	.error{
		color: red;
	}
	</style>
	

	
	
	
</head>
<body class="hold-transition login-page" >
<div class="login-box">
  <div class="login-logo">
    <a href="<?php echo base_url('dashboard');?>"><b>Ace Ltd</b></a>
  </div>
  <!-- /.login-logo -->
  <div class="login-box-body">
    <p class="login-box-msg">Please Enter username</p>
	<?php if($this->session->flashdata('message')){ echo $this->session->flashdata('message'); }?>
    <?php $array = array('method'=>'post','name'=>'admin_login', 'enctype'=>'multipart/form-data', 'id'=> 'admin_login');
	echo form_open_multipart('', $array);?>
      <div class="form-group has-feedback">
        <input type="text" class="form-control" name="username" placeholder="username">
        <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
      </div>
      
	  
      <div class="row">
        
        <div class="col-xs-12">
		<button type="submit" class="btn btn-primary btn-block btn-flat">Submit</button>
		
        </div>
      </div>
	  <a href="<?php echo base_url('admin');?>">Log-in</a>
    </form>
  </div>
</div>
</body>
<script src="<?php echo base_url();?>public/admin/js/jquery-2.2.3.min.js"></script>
<!-- Bootstrap 3.3.6 -->
<script src="<?php echo base_url();?>public/admin/js/bootstrap.min.js"></script>
<!-- iCheck -->
<script src="<?php echo base_url();?>public/admin/js/icheck.min.js"></script>
<script src="<?php echo base_url();?>public/admin/js/jquery.validate.js"></script>
<script>
$(document).ready(function() {
		$("#admin_login").validate({
			rules: {
				username: "required"				
			},
			messages: {
				username: "Please enter your username"
			},
			
			submitHandler: function(form) {
			  form.submit();
			}
			
		});
	});
</script>


</html>
