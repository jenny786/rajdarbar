<?php
if(checkSessionData() == true)
    { 
      redirect('dashboard'); 
    }
$user = decode_id_url(get_cookie('hploginuser'));
$user = $user[1];
$pass = decode_id_url(get_cookie('hploginpass'));
$pass = $pass[1];	
	
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Rajdarbar reality | Log in</title>
	<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
	<link rel="stylesheet" href="<?php echo base_url();?>public/admin/css/bootstrap.min.css">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
	<link rel="stylesheet" href="<?php echo base_url();?>public/admin/css/AdminLTE.min.css">
	<link rel="stylesheet" href="<?php echo base_url();?>public/admin/css/blue.css">
	<style>
	.error{
		color: red;
	}
	</style>
	
	
	
</head>
<body class="hold-transition login-page" >
<div class="login-box">
  <div class="login-logo">
    <a href="<?php echo base_url('dashboard');?>"><b>Rajdarbar reality </b>Ltd.</a>
  </div>
  <!-- /.login-logo -->
  <!-- /.login-logo -->
  <div class="login-box-body">
    <p class="login-box-msg">Sign in</p>

    <?php if($this->session->flashdata('message')){ echo $this->session->flashdata('message'); }?>

    <?php echo form_open('admin/authAuthentication', array('id' => 'loginForm','name'=>'loginForm')); ?>
      <div class="form-group has-feedback">
        <input type="text" class="form-control" name="username" placeholder="Username/Email" value="<?php echo $user; ?>">
        <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
      </div>
      <div class="form-group has-feedback">
        <input type="password" class="form-control" name="password" placeholder="Password" value="<?php echo $pass; ?>">
        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
      </div>
      <div class="row">
        <div class="col-xs-8">
          <div>
            <label>
      <input type="checkbox" name="rememberme" value="yes" <?php echo (get_cookie('hploginuser') != '' ) ? 'checked' : '';?>> Remember Me 
            </label>
          </div>
        </div>
        <!-- /.col -->
        <div class="col-xs-4">
          <button type="submit" class="btn btn-primary btn-block btn-flat">Sign In</button>
		  
        </div>
		
        <!-- /.col -->
      </div>
	  <a href="<?php echo base_url('admin/forgot_password');?>">Forgot Password</a>
    </form>

    <!--<a href="<?php echo base_url();?>admin/forgot_password">I forgot my password</a><br>-->
 
  </div>
  
</div>


</body>
<script src="<?php echo base_url();?>public/admin/js/jquery-2.2.3.min.js"></script>
<!-- Bootstrap 3.3.6 -->
<script src="<?php echo base_url();?>public/admin/js/bootstrap.min.js"></script>
<!-- iCheck -->
<script src="<?php echo base_url();?>public/admin/js/icheck.min.js"></script>
<script src="<?php echo base_url();?>public/admin/js/jquery.validate.js"></script>
<script>
/*
$(function () {
    $('input').iCheck({
      checkboxClass: 'icheckbox_square-blue',
      radioClass: 'iradio_square-blue',
      increaseArea: '30%' // optional
    });
});
*/
// login form submit
$(document).ready(function() {
	$("#loginForm").validate({
		rules: {
			username: "required",
			password: "required",
			captcha_code: "required"				
		},
		messages: {
 			username: "Please enter your username",
 			password: "Please enter your password",
			captcha_code: "Please enter captcha code"
 		},
		
 		submitHandler: function(form) {
 		  form.submit();
 		}
	});
}); 

</script>
<script>
  var cval = '<?php echo $this->security->get_csrf_hash(); ?>';
  
$(document).ready(function(){
        $('.refreshCaptchaReg').on('click', function(){
			 $.ajax({
				url: '<?php echo base_url();?>admin/refresh/',
				type: 'POST',
				datatype: 'json',
				data: {'<?php echo $this->security->get_csrf_token_name(); ?>':cval},
				success: function(response){
					obj = JSON.parse(response);
					if(obj.status=='true'){
						$('#captImgReg').html(obj.data);
						cval = obj.key;
					$("input[name='csrf_token_name']").val(cval);
					}
				},
				error: function(){
				   
				}
			});
			
        });
		
		$('#captcha_code_reg').on('change', function(){
			var inputCapCode = $('#captcha_code_reg').val();
			$.ajax({
				url: '<?php echo base_url();?>admin/get_current_captcha_code/',
				type: 'POST',
				datatype: 'json',
				data: {'<?php echo $this->security->get_csrf_token_name(); ?>':cval,'inputCapCode': inputCapCode},
				success: function(response){
					obj = JSON.parse(response);
					cval = obj.key;
					if(obj.status=='true'){
						if( obj.session_captcha != inputCapCode ){
							$("input[name='csrf_token_name']").val(cval);
							$('#capErrorReg').show().html('<span style="color:#f00">Incorrect code !!</span>'); 
							return false;
						}else{
							$("input[name='csrf_token_name']").val(cval);
							$('#capErrorReg').hide();
							
						}
						
						
					}
				},
				
			});
			
			
			
		});
	});
	
	// check if correct captcha code entered or not 
	
</script>

</html>
