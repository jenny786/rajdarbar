<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller {
	
	public function _construct(){
		parent:: _construct();
		
		
		
		/*
		if(getSessionData() =='true'){
			return true;
		}else{
			redirect('admin');
		}
		*/
	}


	public function index()
	{	
		$this->load->helper('cookie');
		$this->load->view('login');
	}
	
	public function authAuthentication(){	
	   	
	
		$this->load->model('admin/App_admin_model');
		
		if($this->input->post()){
			$username = $this->input->post('username');
			$password = $this->input->post('password');
	    if($username !='' && $password !=''){
	        $pass = md5($password);
	        $result = $this->App_admin_model->userAutheticate($username, $pass);
				if($result['status']=='okay'){
					$user_data = array();
					$user_data['homzz_user_id'] = $result['userDetail']->id;
					$user_data['homzz_user_fname'] = $result['userDetail']->first_name;
					$user_data['homzz_user_lname'] = $result['userDetail']->last_name;
					
					$this->session->set_userdata($user_data);

                    // set cookies //  86400 * 30 * 12 = 1 Year // -------------------
					if( $this->input->post('rememberme') == 'yes' ){
						setcookie('hploginuser', encode_id_url($this->input->post('username')), time() + (86400 * 30 * 12), "/",'','',TRUE);
						setcookie('hploginpass', encode_id_url($this->input->post('password')), time() + (86400 * 30 * 12), "/",'','',TRUE);
					}else{
						setcookie('hploginuser', '', time() - 3600, "/");
						setcookie('hploginpass', '', time() - 3600, "/");
					}

					setSuccessFlashData('successfully Logged in');
					redirect('dashboard');				
				}else{
					setErrorFlashData('Entered wrong email Or password ');
					redirect('admin');
				}
				
			}else{
				setWarningFlashData('Please enter valid email-id and password');
				redirect('admin');
			}
			
		}else{
			setWarningFlashData('Trying to an unauthorized access.');
			redirect('admin');
		}
	
	}
	
	
	public function refresh(){
	$return = array();
		$return['key'] = $this->security->get_csrf_hash(); 
		$this->load->helper('captcha');
		$random_number = substr(number_format(time() * rand(),0,'',''),0,4);
        $config = array(
            //'word'          => $random_number,           
		    'word'          => '',
			'img_path'      => './captcha/',
			'img_url'       => base_url().'captcha/',
			'font_path'     => FCPATH.'public/front/font/hind-regular-webfont.ttf',
			'img_width'     => '150',
			'img_height'    => 40,
			'expiration'    => 7200,
			'word_length'   => 5,
			'font_size'     => 20,
			'img_id'        => 'Imageid',
			'pool'          => '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ',
			'colors'        => array(
                'background' => array(255, 255, 255),
                'border' => array(255, 255, 255),
                'text' => array(0, 0, 0),
                'grid' => array(255, 40, 40)
            )
        );
        $captcha = create_captcha($config);
		$data['captchaImg'] = $captcha['image'];
        
        // Unset previous captcha and store new captcha word
        $this->session->unset_userdata('captchaWord');
        $this->session->set_userdata('captchaWord',$captcha['word']);
        
        // Display captcha image
		$csrf_value = $this->security->get_csrf_hash();
		
		$return['status'] = 'true'; 		
		$return['data'] =$captcha['image'];
		echo json_encode($return); die;	
       //echo $captcha['image'].'|'.$csrf_value;
    }
	
	function get_current_captcha_code(){
		$return = array();
		$return['status'] = 'true';
		$return['key'] = $this->security->get_csrf_hash(); 
		$return['session_captcha'] =$this->session->userdata('captchaWord');
		echo json_encode($return); die;	
	}
	
	public function logout(){
		$message = '';
		if( getSessionData('user_id') != '')
		{
			$message = 'Logged out successfully';
		}
        $items = array('homzz_user_id', 'homzz_user_fname', 'homzz_user_lname');
        $this->session->unset_userdata($items);
     
        setSuccessFlashdata($message);
		redirect('admin');
	}
	
	public function changepassword() {  
			if(checkSessionData() =='true'){
	    
			$this->load->library('form_validation');
			
			$this->form_validation->set_rules('old_password', 'Old password ', 'trim|required');
			$this->form_validation->set_rules('password', 'New password', 'trim|required|min_length[8]');
			$this->form_validation->set_rules('cnfrm_password', 'confirm password', 'trim|required|matches[password]');
			
			if ($this->form_validation->run() == FALSE){
				$data['page_title'] = 'Change Password';
				$data['main_content'] = 'admin/changepassword';
				$this->load->view('admin/layout', $data);
			} else {
				 if ($this->input->post('old_password')) {
				$data = $this->input->post();
				$password = $data['password'];
				$confirm_password = $data['cnfrm_password'];
				if($password == $confirm_password){
					$uid = $this->session->userdata("homzz_user_id");
					$old_password = $data['old_password'];
					$password = $data['password'];
					$this->db->select('id');
					$this->db->from('tbl_users');
					$this->db->where('user_password', md5($old_password));
					$this->db->where('id', $uid);
					$query = $this->db->get();
					if ($query->num_rows() > 0) {
						
					} else {
						setErrorFlashData('Old password doesnot match!!');
						redirect('/admin/changepassword');
					}
						$this->db->select('id');
						$this->db->from('tbl_users');
						$this->db->where('user_password', md5($password));
						$this->db->where('id', $uid);
						$query = $this->db->get();
						if ($query->num_rows() > 0) {
							setErrorFlashData('Please enter new password not old one!!');
							redirect('/admin/changepassword');
						}
						$Data['user_password'] = md5($password);
						$this->db->where('id', $uid);
						$this->db->update('tbl_users', $Data);
						setSuccessFlashData('Password changed Successfully..!!');
						redirect('admin/changepassword');
					}else{
						setErrorFlashData('New password and confirm password not match, Pleae try again.');
						redirect('/admin/changepassword');
					}
				
				
			   
				}else{
					setErrorFlashData('Please Enter Password.');
						redirect('/admin/changepassword');
				}
			}	
			}else{ 
	      redirect('admin'); 
	    }
		
       
		
			
		
    }
	
	
	public function forgot_password(){
		
		if($this->input->post()){
			$this->load->model('admin/App_admin_model');
		    $this->load->library('email');
	        $usesrname = $this->input->post('username');
			$admin_user = $this->App_admin_model->get_user_email($usesrname);
			if($admin_user['status'] == 'false'){
			    setErrorFlashData('Username is not valid, Please try again!');
        		redirect('admin/forgot_password');
			}
		    $id = $admin_user['list']->id;
			$time = time();
		    $email = $admin_user['list']->user_email;
		    $name = $admin_user['list']->first_name;
	
	
		    $combine_token = $id.'|'.$time.'|10101';
			$link= strtr(base64_encode($combine_token), '+/=', '._-');
			$url = base_url('admin/resetpassword/'.$link);
			$link2  = "<a href='$url'>click Here</a>";
			$message = "Hi,<p> Please $link2 to reset password.<br><p>This link would be expire after 30 minute <p><p>Regards<br> Mahindra Equipment";
			
			//$message = 'Project Name : '.$property_name->project_name.',<br><br>Dealer Name : '.$dealer_name->dealer_name.',<br><br>project Type : '.$price_name->projecttype.',<br><br>Super Built Area : '.$price_name->built_area.',<br><br>Price Type : '.$price_name->price_type.',<br><br>Start Price : '.$price_name->startprice.',<br><br>End Price : '.$price_name->endprice.',<br><br>Name : '.$post_value['name'].',<br><br>Mobile : '.$post_value['mobile'].',<br><br>Email : '.$post_value['email'].',<br><br>Message : '.$post_value['message'].'';
			$dealer_email = $dealer_name->email;
			$this->load->library('email');
			$config['mailtype'] = 'html';
			$this->email->initialize($config);
			$this->email->from('noreply@staging.csipl.net', 'Package');
			$this->email->to('abhishek.srivastava@csipl.net');
			$this->email->subject('Forgot password');
			$this->email->message($message);
			if($this->email->send()){
			setSuccessFlashData('We have sent an link in your registerd mail id, Click here change your password.');
			redirect('admin');
			} else {
			setErrorFlashData('Mail not sent');
			redirect('admin');
			}
			
			
			
			/*require_once "Mail.php";
			require_once 'Mail/mime.php';

            $from = "Mahindra Equipment<noreply@mahindrafarmequipment.com>";
            
            $to = "$name <$email>";
            $subject = "Forgot Password";
            
            $link2  = "<a href='$url'>click Here</a>";
            $body = "Hi,<p> Please $link2 to reset password.<br><p>This link would be expire after 30 minute <p><p>Regards<br> Mahindra Equipment";
            $crlf = "\n";
        //  $body = "Hi,\n\n Please click here to reset password.\n\nThis link would be expire after 30 minute";
            
            $host = "localhost";
            $port = "25";
            $username = "smtp@mahindrafarmequipment.com";
            $password = "KvBIerT4VD5K";
            
            $headers = array ('From' => $from,
              'To' => $to,
              'Subject' => $subject);
            $smtp = Mail::factory('smtp',
              array ('host' => $host,
                'port' => $port,
                'auth' => false,
                'username' => $username,
                'password' => $password));
                
                
            $mime = new Mail_mime(array('eol' => $crlf));
            $mime->setHTMLBody($body);
            $body = $mime->get();
            $headers = $mime->headers($headers);    
            
            $mail = $smtp->send($to, $headers, $body);
            
            if (PEAR::isError($mail)) {
              echo("<p>" . $mail->getMessage() . "</p>");die;
             } else {
                     $this->session->set_flashdata('success_message', 'We have sent an link in your registerd mail id, Click here change your password.');
            		redirect('admin');
             }*/

		
			
		}
		$this->load->view('forgot_password');
	}
	
	
	public function resetpassword($id){
		$this->load->model('admin/App_admin_model');
		$this->load->library('form_validation');
			
		$decode_url = base64_decode(strtr($id, '._-', '+/='));
	    $exploastoken = explode('|', $decode_url);
	    $id = $exploastoken[0];
		$user_id = $exploastoken[0];
		$user_token = $exploastoken[2];
	  
		$last_activity_time = date('d-m-Y H:i:s', $exploastoken[1]);
		$endTime = date('d-m-Y H:i:s', strtotime("+25 minutes", strtotime($last_activity_time)));
		$current_time = date('d-m-Y H:i:s');
		
			
		
		$this->form_validation->set_rules('password', 'New password', 'trim|required|min_length[8]');
		$this->form_validation->set_rules('cnfrm_password', 'confirm password', 'trim|required|matches[password]');
		
		if($current_time >= $endTime){
	        $this->session->set_flashdata('error_message', 'This Link has been Expired! Please Try again.');
			redirect('admin/forgot_password');
		}
				
		if ($this->form_validation->run() == FALSE){
			 $this->load->view('set_new_password');
		}else if($this->input->post('password')!=''&& $this->input->post('cnfrm_password')!=''){
			
			$user_token_exist = $this->App_admin_model->user_token_exist($user_id, $user_token);
			if($user_token_exist['status'] == 'true'){
				$this->session->set_flashdata('error_message', 'Your password already has been update.');
				redirect('admin');
			}else{
				$password = md5($this->input->post('password'));
				$admin_user = $this->App_admin_model->chage_forgot_password($user_id, $password);
				if($admin_user){
					$this->session->set_flashdata('success_message', 'Your password has been successfully reset! Please try to login.');
					redirect('admin');
				}
			}
		}
	}
	
}
