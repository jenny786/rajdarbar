<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class App_career_model extends CI_Model{
	
	function __construct() {
        parent::__construct();
        
    }
	
	protected $tbl_career = 'tbl_career';
	
	public function insertCareer($post_value){
		
		if(!empty($post_value)){
			
			$static_pageData = array(
			'dept_name'        => $post_value['dept_name'],
			'slug'             => $post_value['slug'],
			'position_name'    => $post_value['position_name'],
			'experience'    => $post_value['experience'],
			'qualification'    => $post_value['qualification'],
            'description'  	   => $post_value['description'],
            'sequence'  	   => $post_value['sequence'],
			'publish_date'     => $post_value['publish_date'],
            'start_date'  	   => $post_value['start_date'],
            'end_date'  	   => $post_value['end_date'],
            'meta_title'       => $post_value['meta_title'],
			'meta_keyword'     => $post_value['meta_keyword'],
			'meta_description' => $post_value['meta_description']
        );
			$this->db->insert('tbl_career', $static_pageData);
			
		$insert_id = $this->db->insert_id();
		if($insert_id){			
				$return['status'] = 'true';
				$return['message'] = "Career added successfully.";				
			}else{
				$return['status'] = 'false';
				$return['message'] = "Somthing went wrong.";
			}
			return $return;
		}
	}
	
	public function updatecareer_page($sub_ad, $id) {
        $sub_admin['dept_name']  = $sub_ad['dept_name'];
		$sub_admin['slug']  = $sub_ad['slug'];
		$sub_admin['position_name']  = $sub_ad['position_name'];
		$sub_admin['experience']  = $sub_ad['experience'];
		$sub_admin['qualification']  = $sub_ad['qualification'];
		$sub_admin['description']  = $sub_ad['description'];
		$sub_admin['sequence']  = $sub_ad['sequence'];
		$sub_admin['publish_date']  = $sub_ad['publish_date'];
		$sub_admin['start_date']  = $sub_ad['start_date'];
		$sub_admin['end_date']  = $sub_ad['end_date'];
		$sub_admin['meta_title']  = $sub_ad['meta_title'];
        $sub_admin['meta_keyword']  = $sub_ad['meta_keyword'];
        $sub_admin['meta_description']  = $sub_ad['meta_description'];
        $this->db->where('id', $id);
		$update_status = $this->db->update('tbl_career', $sub_admin);
		if ($update_status) {

            return $update_status;

        } else {

            return FALSE;

        }

    }
	
	public function getCareerList(){
    $this->db->select('*');
    $this->db->from('tbl_career');
    $query = $this->db->get();
	if($query->num_rows()>0)
	    return $query->result_array($query);
	  else
	    return array(); 
	}
	
	public function getCareerEnquiry($id=false)
	{
	  $this->db->select('*');
      $this->db->from('tbl_career_enquiry');
	  if($id)
	  {
		$this->db->where('id', $id);  
	  }
		  
      $query = $this->db->get();
	     if($query->num_rows()>0)
	    return $query->result_array($query);
	      else
	    return array();	
	}
	
	
	public function checkcareerExist($editId="") {
		$this->db->where('id', $editId);
		$this->db->from('tbl_career');
		$userStatus = $this->db->count_all_results();        
		if ($userStatus > 0) {
		return 1;
		} else {
		return 0;
		}
	}
	
	public function deletecareer($id){
		$this->db->where('id', $id);
		if ($this->db->delete('tbl_career')) {
		return 1;
		} else {
		return 0;
		}
	}
	
	public function deletecareerenquiry($id){
		$this->db->where('id', $id);
		if ($this->db->delete('tbl_career_enquiry')) {
		return 1;
		} else {
		return 0;
		}
	}
	
	
	
	public function status_active_inacive($post_value) {

        if(!empty($post_value)){
			$id = $post_value['record_id'];
			$data = array();
			if($post_value['status']==1)
			{
				$status =0;
			}
			else if($post_value['status']==0)
			{
				$status =1;
			}
			$data['is_active'] = $status;
			$this->db->where('id', $id);
			$updated = $this->db->update($this->tbl_career, $data);
			if($updated){			
				$return['status'] = $status;
				$return['message'] = "Record updated successfully.";				
			}else{
				$return['status'] = 'false';
				$return['message'] = "Somthing went wrong, please try again.";
			}
			return $return;
		}

    }
	
	public function fetchEditCareer($sid) {
		$this->db->select('*');
		$this->db->from('tbl_career');
		$this->db->where('id', $sid);
		$query = $this->db->get();
	    if ($query->num_rows() > 0) {
            $data = $query->result_array();
			return $data; 
	     }
        return false;
    }
	
	public function getCountryDetail($sid) {
	$this->db->select('country_id');
    $this->db->from('tbl_news');
   	$this->db->where('news_id', $sid);
		$query = $this->db->get();
	    if ($query->num_rows() > 0) {
            $data = $query->result_array();
			$res_exp=array();
				foreach ($data as $val)
				 {
					$res_exp[]=$val['country_id'] ;
				 }
			return $res_exp; 
	     }
        return false;
    }
	
	public function check_slugExist($c_name){
        $slug = trim($c_name);
		$this->db->select('*');
		$this->db->where('slug', $slug);
		$query = $this->db->get('tbl_news_management');        
		return $query->num_rows();
	}
	
	public function getCountryDetail1($array) {
		//pr($array);DIE;
		$this->db->select('tbl_news.news_id,tbl_news.country_id,tbl_country.country_id,tbl_country.country_name,tbl_language.language_id,tbl_language.language_name');
    $this->db->from('tbl_news');
    $this->db->join('tbl_country', 'tbl_country.country_id = tbl_news.country_id');
    $this->db->join('tbl_language', 'tbl_language.language_id = tbl_news.language_id');
	$this->db->where('news_id', $array);	
		$query = $this->db->get();
		//echo $this->db->last_query(); exit;
	    if ($query->num_rows() > 0) {
            $data = $query->result_array();
			return $data; 
	     }
        return false;
    }
}
