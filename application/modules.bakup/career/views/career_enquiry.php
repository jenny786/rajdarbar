<style>
.bgnone {
	background-color: transparent;
}
#example1 td span:last-child {
    display: none;
}
</style>
<div class="content-wrapper">
<?php if($this->session->flashdata('success_message')){ ?>
	<div class="alert alert-success alert-dismissable">
		<a href="#" class="close" data-dismiss="alert" aria-label="close">�</a>
		<strong>Success!</strong> <?php echo $this->session->flashdata('success_message'); ?>
	</div>
<?php }else if($this->session->flashdata('error_message')){ ?>
	<div class="alert alert-danger alert-dismissable">
		<a href="#" class="close" data-dismiss="alert" aria-label="close">�</a>
		<strong>Oops!</strong> <?php echo $this->session->flashdata('error_message'); ?>
	</div>
<?php } ?>

    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Career list
      </h1>
	 </section>
      
    

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
            <div class="box">
            <div class="box-header" >
			<ol class="breadcrumb bgnone">
					<li><a href="<?php echo base_url('dashboard');?>"><i class="fa fa-dashboard"></i> <?php echo 'home' ;?></a></li>
					<li class="active">Career Enquiry list</li>
					
					
				</ol>
				
				
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
				  <th> User Name </th>
                  <th> Email </th>
                  <th> Mobile </th>
                  <th><?php echo 'action';?></th>
                </tr>
                </thead>
                <tbody>
				<?php if(!empty($career_list)){ 
					$i=1;
					foreach($career_list as $value){?>
					<tr>
						<td><?php echo $value['name'];?></td>
						<td><?php echo $value['email'];?></td>
						<td><?php echo $value['mobile'];?></td>
						<td>
						<a class="" data-toggle="tooltip" data-placement="top" title="" href="<?php echo base_url() . "career/showcareer/" . $value['id']; ?>" style="height:20px;"> Show</a>
						
					<a class="btn btn-danger" data-toggle="tooltip" data-placement="top" title="" href="<?php echo base_url() . "career/deleteCareerenquiry/" . $value['id']; ?>" style="padding: 0px 4px;" data-original-title="Delete Career" onclick="return confirm('Are you sure? you want to delete.')"><i class="fa fa-trash"></i></a> 
						
												
						</td>
					</tr>
                <?php $i++; } }?>
                </tbody>
               
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->

         
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <script>
  $(document).ready(function() {
        $(document).on('click', '.activeDeactive', function() {
			record_id = $(this).attr('record_id');
            status = $(this).attr('record_status');
			var cval = $(this).data('csrf');
			if (status == '1') {
                var r = confirm("Are you sure you want to De-activate?");
            } else {
                var r = confirm("Are you sure you want to Activate?");
            }
			
            if (r == true) {
				$.post('<?php echo base_url() ?>career/ajax_careerstatus/', {'record_id': record_id, 'status': status, '<?php echo $this->security->get_csrf_token_name(); ?>':cval}, function(data) {
                        var obj = jQuery.parseJSON(data);
						var csrfvalue = obj.csrfvalue;
						$('.recordDataDIV' + record_id).html(obj.html);
						$('.Changecsrf').attr('data-csrf', csrfvalue);
				});
            }
        });
    });
  

  $(function () {
   $("#example1").DataTable( {
		aaSorting: [],
		pageLength: 25,
		paging: true,
		bFilter: true,
		bInfo: false,
		bSortable: true,
		bRetrieve: true,
		aoColumnDefs: [
                {"aTargets": [0], "bSortable": true},
                {"aTargets": [1], "bSortable": true},
                {"aTargets": [2], "bSortable": true},
                {"aTargets": [3], "bSortable": false},
                
            ]
	
	});
  });

</script>
