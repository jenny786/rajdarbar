<style>
.bgnone {
	background-color: transparent;
}
#example1 td span:last-child {
    display: none;
}
</style>
<div class="content-wrapper">
<?php if($this->session->flashdata('success_message')){ ?>
	<div class="alert alert-success alert-dismissable">
		<a href="#" class="close" data-dismiss="alert" aria-label="close">�</a>
		<strong>Success!</strong> <?php echo $this->session->flashdata('success_message'); ?>
	</div>
<?php }else if($this->session->flashdata('error_message')){ ?>
	<div class="alert alert-danger alert-dismissable">
		<a href="#" class="close" data-dismiss="alert" aria-label="close">�</a>
		<strong>Oops!</strong> <?php echo $this->session->flashdata('error_message'); ?>
	</div>
<?php } ?>

    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Career list
      </h1>
	 </section>
      
    

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
            <div class="box">
            <div class="box-header" >
			<ol class="breadcrumb bgnone">
					<li><a href="<?php echo base_url('dashboard');?>"><i class="fa fa-dashboard"></i> <?php echo 'home' ;?></a></li>
					<li class="active">Career Enquiry list</li>
						
				</ol>
				
				
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="" class="table table-bordered table-striped" align="center">
                <thead>
				  <?php //pr($career_list); ?>
                <tr>
				<td> &nbsp; </td>
				 <td> First Name </td>
				 
				 <td> : </td>
                   <td> <?php echo $career_list[0]['name']?> </td>
                </tr>
				 
				 <tr>
				<td> &nbsp; </td>
				 <td> Email </td>
			 
				 <td> : </td>
                   <td> <?php echo $career_list[0]['email']?> </td>
                </tr>
				
				 <tr>
				<td> &nbsp; </td>
				 <td> Mobile </td>
			 
				 <td> : </td>
                   <td> <?php echo $career_list[0]['mobile']?> </td>
                </tr>
				
				<tr>
				<td> &nbsp; </td>
				 <td> Date of birth </td>
			 
				 <td> : </td>
                   <td> <?php echo $career_list[0]['dob'];?> </td>
                </tr>
				
				<tr>
				<td> &nbsp; </td>
				 <td> Gender </td>
			 
				 <td> : </td>
                   <td> <?php echo $career_list[0]['gender'];?> </td>
                </tr>
				
				<tr>
				<td> &nbsp; </td>
				 <td> Subject </td>
			 
				 <td> : </td>
                   <td> <?php echo $career_list[0]['subject'];?> </td>
                </tr>
				<tr>
				<td> &nbsp; </td>
				 <td> Position </td>
			 
				 <td> : </td>
                   <td> <?php echo $career_list[0]['position'];?> </td>
                </tr>
				<tr>
				<tr>
				<td> &nbsp; </td>
				 <td> Comment </td>
			 
				 <td> : </td>
                   <td> <?php echo $career_list[0]['comment'];?> </td>
                </tr>
				<?php if($career_list[0]['cv']!=''){?>
				<tr>
				<td> &nbsp; </td>
				 <td> Cv </td>
			 
				 <td> : </td>
                   <td>  <a href="<?php echo base_url();?>/images/cv/<?php echo $career_list[0]['cv'];?>" target="_blank"> Download </a>  </td>
                </tr>
				<?php } ?>
				<tr>
				<td> &nbsp; </td>
				 <td> Send Date </td>
			 
				 <td> : </td>
                   <td> <?php echo $career_list[0]['sent_date'];?> </td>
                </tr>
				
				<tr>
				<td colspan="4" align="right"> 
				<a href="<?php echo base_url();?>/career/enquiry/">
				back </a>  </td>
				 
                </tr>
				
				
                </thead>
               </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->

         
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <script>
 
  

</script>
