<style>
.bgnone {
	background-color: transparent;
}
</style>
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <?php echo 'Update News';?>
        
      </h1>
      
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <!-- left column -->
        <div class="col-md-12">
          <!-- general form elements -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <ol class="breadcrumb bgnone">
        <li><a href="<?php echo base_url('dashboard');?>"><i class="fa fa-dashboard"></i> <?php echo 'home';?></a></li>
        <li ><a href="<?php echo base_url('career');?>">Opening</a></li>
        <li class="active"> Update Opening </li>
      </ol>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
			<?= validation_errors(); ?>
           <?php $attributes = array('id' => 'frm_subadmin', 'name'=>'frm_subadmin');
	echo form_open_multipart('career/editCareer/'.$fetchmodule[0]['id'], $attributes); ?>	
			
			
				<?php if($fetchmodule[0]){?>
				<div class="box-body">
				<div class="form-group">
					  <label for=""> Department Name</label>
					  <input type="text" class="form-control" id="dept_name" name="dept_name" placeholder="Department Name" value="<?php echo $fetchmodule[0]['dept_name']; ?>">
					</div>
					
					<div class="form-group">
					  <label for=""><?php echo 'slug';?></label>
					  <input type="text" class="form-control" id="slug" name="slug" placeholder="Slug" value="<?php echo $fetchmodule[0]['slug']; ?>">
					</div>
					
					<div class="form-group">
					  <label for="">Position Name </label>
					  <input type="text" class="form-control" id="position_name" name="position_name" placeholder="Position Name " value="<?php echo $fetchmodule[0]['position_name']; ?>">
					</div>
					
				<div class="form-group">
					  <label for="">Experience  </label>
					  <input type="text" class="form-control" id="experience" name="experience" placeholder="Experience " value="<?php echo $fetchmodule[0]['experience']; ?>">
					</div>
				<div class="form-group">
					  <label for="">Qualification  </label>
					  <input type="text" class="form-control" id="qualification" name="qualification" placeholder="Qualification " value="<?php echo $fetchmodule[0]['qualification']; ?>">
					</div>
					
				<div class="form-group">
					  <label for=""><?php echo 'Description';?></label>
					  <textarea id="description" name="description" class="form-control ckeditor">
					     <?php echo $fetchmodule[0]['description']; ?>
						</textarea>
					</div>	
					
					<div class="form-group">
					  <label for=""><?php echo 'publish_date';?></label>
					  <input type="text" class="form-control datepicker" id="publish" name="publish_date" value="<?php echo $fetchmodule[0]['publish_date']; ?>" />
					</div>
					<div class="form-group">
					  <label for=""><?php echo 'sequence';?></label>
					  <input type="text" maxlength="2" class="form-control" id="sequence" name="sequence" placeholder="News Sequence" value="<?php echo $fetchmodule[0]['sequence']; ?>">
					</div>
					<div class="form-group">
					  <label for=""><?php echo 'start_date';?></label>
					  <input type="text" class="form-control datepicker" id="datefrom" name="start_date" value="<?php echo $fetchmodule[0]['start_date']; ?>" />
					</div>
					<div class="form-group">
					  <label for=""><?php echo 'end_date';?></label>
					  <input type="text" class="form-control datepicker" id="dateend" name="end_date" value="<?php echo $fetchmodule[0]['end_date']; ?>" />
					</div>
					
					<div class="form-group">
					  <label for=""><?php echo 'Meta title';?></label>
					  <input type="text" maxlength="30" class="form-control" id="meta_title" name="meta_title" placeholder="Meta Title" value="<?php echo $fetchmodule[0]['meta_title']; ?>">
					</div>
					<div class="form-group">
					  <label for=""><?php echo 'Meta keyword';?></label>
					  <input type="text" maxlength="50" class="form-control" id="meta_keyword" name="meta_keyword" placeholder="Meta Keyword" value="<?php echo $fetchmodule[0]['meta_keyword']; ?>">
					</div>
					<div class="form-group">
					  <label for=""><?php echo 'Meta description';?></label>
					  <input type="text" maxlength="165" class="form-control" id="meta_description" name="meta_description" placeholder="Meta Description" value="<?php echo $fetchmodule[0]['meta_description']; ?>">
					 
					</div>
					
				</div>
				
              <!-- /.box-body -->

              <div class="box-footer">
                <button type="submit" class="btn btn-primary">Update</button>
				<a href="<?php echo base_url('news');?>" class="btn btn-danger">Cancel</a>
              </div>
			  <?php }else{
					echo $fetchmodule['message'];
				}?>
            </form>
          </div>
          <!-- /.box -->

        

        </div>
        <!--/.col (left) -->
        <!-- right column -->
        
        <!--/.col (right) -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->

  </div>
  <link rel="stylesheet" href="<?php echo base_url();?>public/admin/css/selectize.default.css">
  <script src="<?php echo base_url();?>public/admin/js/selectize.js"></script>
  <script src="<?php echo base_url();?>public/admin/js/index.js"></script>
  <script>
				var eventHandler = function(name) {
					return function() {
						console.log(name, arguments);
						$('#log').append('<div><span class="name">' + name + '</span></div>');
					};
				};
				var $select = $('#select-state').selectize({
					plugins: ['remove_button'],
					create          : true,
					onChange        : eventHandler('onChange'),
					onItemAdd       : eventHandler('onItemAdd'),
					onItemRemove    : eventHandler('onItemRemove'),
					onOptionAdd     : eventHandler('onOptionAdd'),
					onOptionRemove  : eventHandler('onOptionRemove'),
					onDropdownOpen  : eventHandler('onDropdownOpen'),
					onDropdownClose : eventHandler('onDropdownClose'),
					onFocus         : eventHandler('onFocus'),
					onBlur          : eventHandler('onBlur'),
					onInitialize    : eventHandler('onInitialize'),
				});
	</script>
  <script type="text/javascript">
  $(document).ready(function() {
  $("#frm_subadmin").validate({
            rules: {
				'title': {required: true},
			    'slug': {required: true},
			    'news_type': {required: true},
			    'sequence': {required: true},
			    'start_date': {required: true},
			    'end_date': {required: true},
			    'meta_title': {required: true},
			    'meta_keyword': {required: true},
			    'meta_description': {required: true},
			    'video': {required: true},
			    'link': {required: true}
			},
	submitHandler: function(form) {
			  form.submit();
			}
			
		});
	$('#datefrom').datepicker({ format: 'yyyy-mm-dd' });
    $('#dateend').datepicker({ format: 'yyyy-mm-dd' });
	$('#publish').datepicker({ format: 'yyyy-mm-dd' });
	});
	  
  </script>
  <script src="<?php echo base_url() . "public/admin/css/datepicker/bootstrap-datepicker.js" ?>"></script>
<link rel="stylesheet" href="<?php echo base_url() . "public/admin/css/datepicker/datepicker3.css" ?>">