<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class App_subscribe_newsletter_model extends CI_Model{
	
	function __construct() {
        parent::__construct();
        
    }
	
	protected $tbl_newsletter = 'subscribe_newsletter';
	public function getCareerEnquiry($id=false)
	{
	  $this->db->select('*');
      $this->db->from('subscribe_newsletter');
	  if($id)
	  {
		$this->db->where('id', $id);  
	  }
		  
      $query = $this->db->get();
	     if($query->num_rows()>0)
	    return $query->result_array($query);
	      else
	    return array();	
	}
	
	public function status_active_inacive($post_value) {

        if(!empty($post_value)){
			$id = $post_value['record_id'];
			$data = array();
			if($post_value['status']==1)
			{
				$status =0;
			}
			else if($post_value['status']==0)
			{
				$status =1;
			}
			$data['is_active'] = $status;
			$this->db->where('id', $id);
			$updated = $this->db->update($this->tbl_newsletter, $data);
			if($updated){			
				$return['status'] = $status;
				$return['message'] = "Record updated successfully.";				
			}else{
				$return['status'] = 'false';
				$return['message'] = "Somthing went wrong, please try again.";
			}
			return $return;
		}

    }
	public function updatecareer_page($sub_ad, $id) {
        $sub_admin['dept_name']  = $sub_ad['dept_name'];
		$sub_admin['slug']  = $sub_ad['slug'];
		$sub_admin['position_name']  = $sub_ad['position_name'];
		$sub_admin['experience']  = $sub_ad['experience'];
		$sub_admin['qualification']  = $sub_ad['qualification'];
		$sub_admin['description']  = $sub_ad['description'];
		$sub_admin['sequence']  = $sub_ad['sequence'];
		$sub_admin['publish_date']  = $sub_ad['publish_date'];
		$sub_admin['start_date']  = $sub_ad['start_date'];
		$sub_admin['end_date']  = $sub_ad['end_date'];
		$sub_admin['meta_title']  = $sub_ad['meta_title'];
        $sub_admin['meta_keyword']  = $sub_ad['meta_keyword'];
        $sub_admin['meta_description']  = $sub_ad['meta_description'];
        $this->db->where('id', $id);
		$update_status = $this->db->update('subscribe_newsletter', $sub_admin);
		if ($update_status) {

            return $update_status;

        } else {

            return FALSE;

        }

    }
	public function fetchEditCareer($sid) {
		$this->db->select('*');
		$this->db->from('subscribe_newsletter');
		$this->db->where('id', $sid);
		$query = $this->db->get();
	    if ($query->num_rows() > 0) {
            $data = $query->result_array();
			return $data; 
	     }
        return false;
    }
	
	public function deletecareerenquiry($id){
		$this->db->where('id', $id);
		if ($this->db->delete('subscribe_newsletter')) {
		return 1;
		} else {
		return 0;
		}
	}
	public function getUsersData($id=false){
		if($id){ 
			 
		}else{
			$to_date = $this->input->post('todate').' 23:59:59';
			$from_date = $this->input->post('fromdate').' 00:00:00';
			$this->db->select('*');
			$this->db->from("subscribe_newsletter"); 
			if(!empty($_POST)){
				$this->db->where('created_date >= ',$from_date);
				$this->db->where('created_date <= ',$to_date);
				$this->db->order_by('created_date','DESC');
			}
			else{
				$this->db->order_by('id','DESC');
			}
			$query = $this->db->get();
			if($query->num_rows() > 0){
				$return['status'] = 'true';
				$return['resultSet'] = $query->result();
			}else{
				$return['status'] = 'false';
			} 
		}
		 return $return;
	}
}
