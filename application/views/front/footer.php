	
	<div class="footer">
		<div class="container">
			<div class="col-lg-12 col-md-12 col-sm-12 footer-main">
				<div class="footer-one wow fadeInDown">
					<img src="<?php echo base_url();?>public/front/images/logo1.png" alt=""/>
					<h3>For Any Enquiries:</h3>
					<p><a href="mailto:customercare@rajdarbarrealty.com">customercare@rajdarbarrealty.com</a></p>
					<p><a href="mailto:info@rajdarbarrealty.com">info@rajdarbarrealty.com</a></p>
					<p><a href="mailto:sales@rajdarbarrealty.com">sales@rajdarbarrealty.com</a></p>
					<h3>Call Us: <a href="callto:919991888950">+91 9991888950</a></h3>
					<h2>Rajdarbar Limited</h2>
					<h4>303, 3rd Floor, Global Foyer, </br>
					Golf Course Road, Sector-43, </br>
					Gurgaon -122002, (Haryana) India</h4>
				</div>
				<div class="footer-two wow fadeInDown">
					<h2>Company</h2>
					<ul>
						<li><a href="<?php echo base_url()?>home/about">About Us</a></li>
						<li><a href="<?php echo base_url()?>home/mission_vision">Mission &amp; Vision</a></li>
						<li><a href="<?php echo base_url()?>home/chairmen_message">Chairman&rsquo;s Message</a></li>
						<li><a target="_blank" href="http://rajdarbarrealty.com/PublicNotice.pdf">Public Notice</a></li>
					</ul>
				</div>
				<div class="footer-three wow fadeInDown">
					<h2>Projects</h2>
					<ul>
						<li><a href="<?php echo base_url()?>home/business#commercial-m">Commercial</a></li>
						<li><a href="<?php echo base_url()?>home/business#residential-m">Residential</a></li>
					</ul>
				</div>
				<div class="footer-four wow fadeInDown">
					<h2>Other Links</h2>
					<ul>
						<li><a href="<?php echo base_url()?>home/csr">CSR</a></li>
						<li><a href="<?php echo base_url()?>home/media">Media</a></li>
						<li><a href="<?php echo base_url()?>home/commercial_leasing">Leasing</a></li>
						<li><a href="<?php echo base_url()?>home/career">Careers</a></li>
						<!--<li><a href="<?php //echo base_url()?>home/online_payment">Payment Online</a></li>-->
						<li><a href="<?php echo base_url()?>home/contact_us">Contact Us</a></li>
						<li><a href="javascript:;">Privacy Policy</a></li>
					</ul>
				</div>
				<div class="footer-five wow fadeInDown">
					<h2>Subscribe Newsletter</h2>
					<p>Stay updated about latest happenings, events and campus news</p>
					<div id="succMsg"></div>
					<form id="newsletter" class="sub1" method="post">
					<input type="email" name="email_newsletter" id="email_newsletter" required placeholder="Your email address here">
					<input type="hidden" id="<?php echo $this->security->get_csrf_token_name(); ?>" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>">
					<button class="sub2" id="newsletter_btn" type="submit"><img src="<?php echo base_url();?>public/front/images/msg.png" alt=""></button>
					</form>
					<h2>Rajdarbar Limited@Social</h2>
					<!--https://fontawesome.com/v4.7.0/icons/-->
					<ul>
						<li><a href="https://www.facebook.com/rajdarbarrealty/" target="_blank">
							<i class="fa fa-facebook" aria-hidden="true"></i>
						</a></li>
						<li><a href="https://twitter.com/RajdarbarLtd" target="_blank">
							<i class="fa fa-twitter" aria-hidden="true"></i>
						</a></li>
						<li><a href="https://www.youtube.com/channel/UCqfHGz5TjBvDjHAd-yGWkvQ" target="_blank">
							<i class="fa fa-youtube" aria-hidden="true"></i>
						</a></li>
					
						<li><a href="https://www.linkedin.com/company/rajdarbarltd/" target="_blank">
							<i class="fa fa-linkedin" aria-hidden="true"></i>
						</a></li>
						<li><a href="https://www.instagram.com/rajdarbarltd/" target="_blank">
							<i class="fa fa-instagram" aria-hidden="true"></i>
						</a></li>
						
						<li><a href="http://blog.rajdarbarrealty.com" target="_blank">
							<img src="<?php echo base_url();?>public/front/images/icon-blog.png" alt="" />
							<img class="h" style="display:none;" src="<?php echo base_url();?>public/front/images/icon-blog-h.png" alt="" />
						</a></li>
						
						
					</ul>
				</div>
				<div class="clearfix"></div>
				<div class="copywright">
					<p>© Copyright 2018, all rights reserved with <span>Rajdarbar Limited</span></p>
					<p>Design &amp; Developed By: <a href="http://www.csipl.net/" target="_blank">CSIPL</a></p>
				</div>
			</div>
		</div>
	</div>

	<div class="sdfsd">
	    <div class="popup-wrapper" id="wrpr">
			<div class="view-area">
				<div class="view-block"> 
				<!--<img src="isfcpop.jpg" />-->
					<div class="view-cont mCustomScrollbar" id="pop_cnt"> 
					<!--<img src="images/icn_alert_error.png" class="closepop"/>--> 
						<img src="<?php echo base_url();?>public/front/images/logo.png">
						<div class="info">
							<h4>Thank you for visiting our website.</h4>
							<p><b>Note :</b> This disclaimer ("Disclaimer") will be applicable to the Website. By using or accessing the Website you agree with the Disclaimer without any qualification or limitation. The Company reserves the right to add, alter or delete material from the Website at any time and may, at any time, revise these Terms without notifying you. You are bound by any such amendments and the Company therefore advise that you periodically visit this page to review the current Terms.</p>

							<p>The Websites and all its content are provided with all faults on an "as is" and "as available" basis. No information given under this Website creates a warranty or expand the scope of any warranty that cannot be disclaimed under applicable law. Your use of the Website is solely at your own risk. This website is for guidance only. It does not constitute part of an offer or contract. Design &amp; specifications are subject to change without prior notice. Computer generated images are the artist's impression and are an indicative of the actual designs.</p>

							<p>The particulars contained on the mentions details of the Projects/developments undertaken by the Company including depicting banners/posters of the Project. The contents are being modified in terms of the stipulations / recommendations under the Real Estate Regulation Act, 2016 and Rules made thereunder ("RERA") and accordingly may not be fully in line thereof as of date. You are therefore required to verify all the details, including area, amenities, services, terms of sales and payments and other relevant terms independently with the sales team/ company prior to concluding any decision for buying any unit(s) in any of the said projects. Till such time the details are fully updated, the said information will not be construed as an advertisement. To find out more about a project / development, please telephone our sales centres or visit our sales office during opening hours and speak to one of our sales staff.</p>

							<p>In no event will the Company be liable for claim made by the users including seeking any cancellation for any of the inaccuracies in the information provided in this Website, though all efforts have to be made to ensure accuracy. The Company will no circumstance will be liable for any expense, loss or damage including, without limitation, indirect or consequential loss or damage, or any expense, loss or damage whatsoever arising from use, or loss of use, of data, arising out of or in connection with the use of this website.
							</p>

							<p><strong>We Thank you for your patience and understanding</strong></p>
						</div>
						<div class="buttons">
			  				<button id="clck_agr">I agree</button>
			  				<a href="https://www.google.com/" >Exit Website</a>
						</div>
					</div>
				</div>
	    	</div>
		</div>
	</div>

	
	

<script src="<?php echo base_url();?>public/front/js/jquery-ui.min.js"></script>
<script src="<?php echo base_url();?>public/front/js/bootstrap.min.js"></script>
<script src="<?php echo base_url();?>public/front/js/owl.carousel.min.js"></script>

<script src="<?php echo base_url();?>public/front/js/wow.js"></script>
<script src="<?php echo base_url();?>public/front/js/jquery.swipebox.js"></script>
<script src="<?php echo base_url();?>public/front/js/main-new.js"></script>
<script src="<?php echo base_url();?>public/front/js/viewer.js"></script>

<script src="<?php echo base_url();?>public/front/js/custom.js"></script>
<script type="text/javascript">

	
$(document).ready(function(){
	$(".more-vv").click(function(){
	$(".black11").show();
	$(".popbox").fadeIn("fast");
	});
	$(".popclose").click(function(){
	$(".black11").fadeOut("fast");
	$(".popbox").fadeOut("fast");
	});
});
</script>
	<script type="text/javascript">
	
	$(function(){
        $("#newsletter").on("submit", function(event) {
		var email = $("#email_newsletter").val();
		   var error = 0;
            event.preventDefault();
            var formData = {
                'email': $('input[name=email_newsletter]').val(), 
                'csrf_token_name': $('input[name=csrf_token_name]').val()  
            };
            //console.log(formData);
				$.ajax({
					url: "<?php echo base_url()?>home/newsletter",
					type: "post",
					data: formData,
					dataType: "json",
					success: function(d) {
						$("#succMsg").html(d.msg);
						$("#csrf_token_name").val(d.csrfName);
						return false;
						//alert(d.msg);
					}
				});
			 
        });
    }) 
	
	
	   $(".enquiry").click(function(){
		   var project = '<?php echo $this->uri->segment(2);?>'
		    var newStr = project.replace(/_/g, " ");
		   sessionStorage.setItem('session_pro', newStr);  
		 var select_proj =  sessionStorage.getItem('session_pro');
		
	});
	   $(".addClass").click(function(){
		   sessionStorage.setItem('add_class','rc-top');
	});
	 
		$(document).ready(function(){
			$('#rc').addClass(sessionStorage.getItem('add_class'));
			var select_proj =  sessionStorage.getItem('session_pro');
              $("#project").val(select_proj);
			//sessionStorage.setItem('firstVisit', '1');  
// 			if (sessionStorage.getItem('firstVisit') !=1)
// 				{
// 					$('#wrpr').fadeIn();	
// 					$('#pop_cnt').fadeIn();	
// 					$('body').addClass('fixedPage');
// 				}else{
// 					// alert(sessionStorage.getItem('firstVisit'));
// 				}
			  $('#clck_agr').click(function(){
				sessionStorage.setItem('firstVisit', '1');  
				$('#wrpr').fadeOut();	
				$('#pop_cnt').fadeOut();	
				$('body').removeClass('fixedPage');
			  });  	
			 // $(".content").mCustomScrollbar({
			   // axis:"y" // vertical and horizontal scrollbar
			//});
		});
	</script>

	<!--loader-->
	<div id="loading">
	  <img id="loading-image" src="<?php echo base_url();?>public/front/images/ajax-loader.gif" alt="Loading..." />
	</div>
	<script>
	
	</script>
</body>
</html>
