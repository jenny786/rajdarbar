<!DOCTYPE html>
<html>
<head>

<title>Rajdarbar</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link rel="shortcut icon" type="image/png" href="<?php echo base_url();?>public/front/images/favicon1.ico"/>
<link href="https://fonts.googleapis.com/css?family=Hind:300,400,500,600,700" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>public/front/css/bootstrap.css">
<link href="<?php echo base_url();?>public/front/css/drop/bootstrap-dropdownhover.css" rel="stylesheet">
<link href="<?php echo base_url();?>public/front/css/drop/bootstrap-dropdownhover.min.css" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>public/front/css/font.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>public/front/css/owl.carousel.min.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>public/front/css/owl.theme.default.min.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>public/front/css/jquery-ui.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>public/front/css/animate.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>public/front/css/swipebox.css"/>
<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css" rel="stylesheet"> 
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>public/front/css/style.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>public/front/css/responsive.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>public/front/css/viewer.css">
<script src="<?php echo base_url();?>public/front/js/jquery.min.js"></script>


 <!--[if lte IE 9]>
<script src="bower_components/html5shiv/dist/html5shiv.js"></script>
<![endif]-->
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-128432660-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-128432660-1');
</script>




<!-- Facebook Pixel Code -->
<script>
  !function(f,b,e,v,n,t,s)
  {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
  n.callMethod.apply(n,arguments):n.queue.push(arguments)};
  if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
  n.queue=[];t=b.createElement(e);t.async=!0;
  t.src=v;s=b.getElementsByTagName(e)[0];
  s.parentNode.insertBefore(t,s)}(window, document,'script',
  'https://connect.facebook.net/en_US/fbevents.js');
  fbq('init', '767995420007835');
  fbq('track', 'PageView');
</script>
<noscript><img height="1" width="1" style="display:none"
  src="https://www.facebook.com/tr?id=767995420007835&ev=PageView&noscript=1"
/></noscript>
<!-- End Facebook Pixel Code -->

<!-- Global site tag (gtag.js) - Google Ads: 780762880 -->
<script async src="https://www.googletagmanager.com/gtag/js?id=AW-780762880"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'AW-780762880');
</script>




</head>
<body>
	<script>
 	var imgpath = '<?php echo base_url();?>public/front/images';
</script>

	<div class="header">
		<div class="container">
			<nav class="navbar navbar-default navbar-default-new">
			  <div class="container-fluid">
				<div class="navbar-header">
				  <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				  </button>
				  <a href="<?php echo base_url()?>" class="navbar-brand"><img src="<?php echo base_url();?>public/front/images/logo.png" alt="#"/></a>
				</div>
				<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1"
				 data-hover="dropdown" data-animations="fadeInDown fadeInRight fadeInUp fadeInLeft">
				  <ul class="nav navbar-nav navbar-right">
					 <li class="dropdown">
					  <a href="<?php echo base_url()?>home/about" class="c-link dropdown-toggle">Corporate </a>
					  <ul class="dropdown-menu dropdown-menu-part logo-img-part">
						<div class="row">
							<div class="drop-one1">
								<img src="<?php echo base_url();?>public/front/images/menu-img.jpg" alt=""/>
							</div>
							<div class="drop-one two">
								<h2>Overview</h2>
								<p>Rajdarbar Limited is one of the largest and only backward integrated real estate players in the country. <a href="<?php echo base_url()?>home/about">Read More</a></p>
							</div>
							<div class="drop-one three">
								<h2>Chairman&rsquo;s Message</h2>
								<p>India is one of the fastest 
								growing economies of the world with a billion people that spell a trillion opportunities. <a href="<?php echo base_url()?>home/chairman_message">Read More</a></p>
							</div>
							<div class="drop-one four">
								<ul>
									<li><a href="<?php echo base_url()?>home/about">About Rajdarbar Limited</a></li>
									<li><a href="<?php echo base_url()?>home/mission_vision">Vision &amp; Mission</a></li>
									<li><a href="<?php echo base_url()?>home/chairman_message">Chairman&rsquo;s Message</a></li>
								</ul>
							</div>
						</div>		
					  </ul>
					</li>
					 <li class="dropdown">
					  <a href="<?php echo base_url()?>home/business" class="busi-link c-link1 dropdown-toggle">Projects</a>
					  <ul class="dropdown-menu dropdown-menu-part1 logo-img-part">
						<div class="row">
							<div class="b-menu">
							  <div class="b-menu-left">
							  <!-- Nav tabs -->
							  <ul class="nav nav-tabs nav-tabs-m" role="tablist">
								<li role="presentation" class="active"><a href="<?php echo base_url()?>home/business#commercial-m" role="tab" data-toggle="tab">Commercial</a></li>
								<li role="presentation"><a href="<?php echo base_url()?>home/business#residential-m" role="tab" data-toggle="tab">Residential</a></li>
							  </ul>
							  </div>
								
							  <div class="b-menu-right">	
							  <!-- Tab panes -->
							  <div class="tab-content">
								<div role="tabpanel" class="tab-pane active" id="commercial-m">
									<div class="com-main">
										<div class="com-left">
											<ul>
												<li class="active"><a href="<?php echo base_url()?>home/global_foyer_gurgaon">Global Foyer Gurgaon <span>Delivered &amp; Operational</span></a></li>
												<li><a href="<?php echo base_url()?>home/global_foyer_palam_vihar">Global Foyer Palam Vihar <span>Retail Delivered &amp; operational</span></a></li>
											</ul>
										</div>
										<div class="com-right">
											<div class="img-corner">
												<img src="<?php echo base_url();?>public/front/images/menu-img1.jpg" alt=""/>
												<div class="img-corner-l">
													<a href="javascript:;">
														<img src="<?php echo base_url();?>public/front/images/nk.png" alt=""/>
													</a>
												</div>
												<div class="pull-left">
													<img style="width: 120px;" src="<?php echo base_url();?>public/front/images/commercial-foyer.jpg" alt=""/>
												</div>
											</div>
											<!--<div class="pull-right">
												<p>Gurgaon</p>
											</div>-->
										</div>
									</div>
								</div>
								<div role="tabpanel" class="tab-pane" id="residential-m">
									<div class="com-main">
										<div class="com-left">
											<ul>
												<li class="active"><a href="<?php echo base_url();?>home/global_spaces_hisar">Rajdarbar Spaces Hisar</a></li>
												<li><a href="<?php echo base_url();?>home/global_spaces_sirsa">Rajdarbar Spaces Sirsa</a></li>
												<li><a href="<?php echo base_url();?>home/global_spaces_agra">Rajdarbar Spaces Agra </a></li>
												<li><a href="<?php echo base_url();?>home/global_spaces_karnal_phase1">Global Spaces Karnal Phase I <span>Delivered</span></a></li>
												<li><a href="<?php echo base_url();?>home/global_spaces_karnal_phase2">Global Spaces Karnal Phase II <span>Delivered</span></a></li>
											</ul>
										</div>
										<div class="com-right">
											<div class="img-corner">
												<!--<img class="resi" src="<?php echo base_url();?>public/front/images/resi-menu-right-img.jpg" alt=""/>-->
												<img class="resi" src="<?php echo base_url();?>public/front/images/menu-img2.jpg" alt=""/>
												<div class="img-corner-l">
													<a href="javascript:;">
														<img src="<?php echo base_url();?>public/front/images/nk.png" alt=""/>
													</a>
												</div>
												<div class="pull-left">
													<img style="width: 120px;" src="<?php echo base_url();?>public/front/images/residential-spaces.jpg" alt=""/>
												</div>
											</div>
											<!--<div class="pull-right">
												<p>Hisar</p>
											</div>-->
										</div>
									</div>
								</div>
							  </div>
							  </div>
							</div>
						</div>		
					  </ul>
					</li>
					<li><a href="<?php echo base_url()?>home/csr">CSR</a></li>      
					<li><a href="<?php echo base_url()?>home/media">Media</a></li> 
					<li><a href="<?php echo base_url()?>home/commercial_leasing">Leasing</a></li>     
					<li><a href="<?php echo base_url()?>home/career">Careers</a></li> 
					<li><a href="<?php echo base_url()?>home/contact_us">Contact Us</a></li>      
					<!--<li><a href="<?php //echo base_url()?>home/online_payment">Online Payment </a></li>-->
				  </ul>
				</div>
			  </div>
			</nav>
		</div>
	</div>

