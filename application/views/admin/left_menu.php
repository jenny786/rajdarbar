<script>
 $(document).ready(function()
{
	var url = window.location.href;
	var link = url.split('/').slice(-1);
	$('.treeview-menu li').find('a[href$="'+link+'"]').parent('li').addClass('current');
});
 $(function(){
	$('.treeview-menu').find('.current').parent().css('display','block');
 })
 </script>
 
<style>
.skin-blue .treeview-menu>li.current>a{
    color: #fff;
}
</style>
 
 
 
 
 <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">

      <div class="user-panel">

      </div>

      <ul class="sidebar-menu">
        <li class="">
          <a href="<?php echo base_url('dashboard');?>">
            <i class="fa fa-dashboard"></i> <span>Dashboard</span>
          </a>
        </li>
         <li class="">
          <a href="<?php echo base_url('banner_management');?>">
            <i class="fa fa-list"></i> <span>Banner Management</span>
          </a>
        </li> 
      
		<li class="">
          <a href="<?php echo base_url('enquiry_management');?>">
            <i class="fa fa-list"></i> <span>Enquiry Management</span>
          </a>
        </li>
		<li class="">
          <a href="<?php echo base_url('career/enquiry/');?>">
            <i class="fa fa-list"></i> <span>Career Management</span>
          </a>
        </li>
	    <li class="">
          <a href="<?php echo base_url('subscribe_newsletter');?>">
            <i class="fa fa-list"></i> <span>Subscribe Newsletter</span>
          </a>
        </li> 

    <li class="">
          <a href="<?php echo base_url('project_management/gallery');?>">
            <i class="fa fa-list"></i> <span>Project Gallery</span>
          </a>
        </li> 

         <li class="">
          <a href="<?php echo base_url('project_management/leasing');?>">
            <i class="fa fa-list"></i> <span>Leasing</span>
          </a>
        </li> 


          <li class="">
          <a href="<?php echo base_url('media_management');?>">
            <i class="fa fa-list"></i> <span>Media</span>
          </a>
        </li> 


      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>