$(document).ready(function() {
	$(function(){
		$(".dropdown").hover(            
				function() {
					$('.dropdown-menu', this).stop( true, true ).fadeIn("fast");
					$(this).toggleClass('open');
					$('b', this).toggleClass("caret caret-up");                
				},
				function() {
					$('.dropdown-menu', this).stop( true, true ).fadeOut("fast");
					$(this).toggleClass('open');
					$('b', this).toggleClass("caret caret-up");                
				});
		}); 
		
	$('.nav-tabs-m > li > a').hover(function() {
	  $(this).tab('show');
	});						   
});

$(document).ready(function() {
	//$(window).scroll(function() {    
		//var scroll = $(window).scrollTop();
	
		//if (scroll >= 0) {
		//	$(".header").addClass("headerfix");
		//} else {
		//	$(".header").removeClass("headerfix");
		//}
	//});
	$('.header').addClass("headerfix");		   
});

$(document).ready(function() {
	$(function() {
	$('.com-main .com-left ul li a[href*="#"]:not([href="#"])').click(function() {
	  if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
		var target = $(this.hash);
		target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
		if (target.length) {
		  $('html, body').animate({
			scrollTop: target.offset().top - 65
		  }, 1000);
		  return false;
		}
	  }
	});
  });					   
});

$(document).ready(function() {
	  $(".banner.home .banner-slider").owlCarousel({
			items:1,
			navigation: true,
			nav: true,	
			autoplay: true,
			loop:true,
			autoPlaySpeed: 1000,
			autoPlayTimeout: 1000,
			autoplayHoverPause: true,	
			slideSpeed : 300,
			paginationSpeed : 400,
			pagination:true,
			rewindSpeed: 500,
			navText: ["<img src='"+imgpath+"/arrow-left.png'>","<img src='"+imgpath+"/arrow-right.png'>"]
		});

	  $(".banner-slider").owlCarousel({
			items:1,
			navigation: true,
			nav: true,	
			autoplay: true,
			loop:false,
			autoPlaySpeed: 1000,
			autoPlayTimeout: 1000,
			autoplayHoverPause: true,	
			slideSpeed : 300,
			paginationSpeed : 400,
			pagination:true,
			rewindSpeed: 500,
			//animateIn: 'fadeIn',
			//animateOut: 'fadeOut',
			//transitionStyle: "fadeUp",
			navText: ["<img src='"+imgpath+"/arrow-left.png'>","<img src='"+imgpath+"/arrow-right.png'>"]
		});

	  /*$(".header-c-slider").owlCarousel({
			items:1,
			navigation: false,
			nav: true,	
			autoplay: true,
			loop:true,
			autoPlaySpeed: 1000,
			autoPlayTimeout: 1000,
			autoplayHoverPause: true,	
			slideSpeed : 300,
			paginationSpeed : 400,
			pagination:true,
			rewindSpeed: 500,
			animateIn: 'fadeIn',
			animateOut: 'fadeOut',
			transitionStyle: "fadeUp",
			navText: ["<img src='images/arrow-left.png'>","<img src='images/arrow-right.png'>"]
		});

	  $(".header-r-slider").owlCarousel({
			items:1,
			navigation: false,
			nav: true,	
			autoplay: true,
			loop:true,
			slideSpeed : 300,
			paginationSpeed : 400,
			pagination:true,
			rewindSpeed: 500,
			animateIn: 'fadeIn',
			animateOut: 'fadeOut',
			transitionStyle: "fadeUp",
			navText: ["<img src='images/arrow-left.png'>","<img src='images/arrow-right.png'>"]
		});*/

	  	$(".elev-slider").owlCarousel({
			items:1,
			navigation: true,
			nav: true,	
			autoplay: true,
			loop:true,
			autoPlaySpeed: 2000,
			autoPlayTimeout: 2000,
			autoplayHoverPause: true,	
			slideSpeed : 2000,
			paginationSpeed : 2000,
			pagination:true,
			rewindSpeed: 2000,
			animateIn: 'fadeIn',
			animateOut: 'fadeOut',
			transitionStyle: "fadeUp",
			navText: ["<img src='"+imgpath+"/arrow-left.png'>","<img src='"+imgpath+"/arrow-right.png'>"]
		});
	
		$('.featured-slider').owlCarousel({
			loop:false,
			margin:25,
			nav:true,
			autoplay: true,
			autoPlaySpeed: 1000,
			autoPlayTimeout: 1000,
			autoplayHoverPause: true,
			responsive:{
				0:{
					items:1
				},
				600:{
					items:2
				},
				1000:{
					items:3
				}
			}
		})
		
		$('.shops-slider').owlCarousel({
			loop:true,
			margin:35,
			nav:true,
			autoplay: true,
			autoPlaySpeed: 1000,
			autoPlayTimeout: 1000,
			autoplayHoverPause: true,
			responsive:{
				0:{
					items:1
				},
				600:{
					items:2
				},
				1000:{
					items:4
				}
			}
		})
		
		$('.loans-slider').owlCarousel({
			loop:true,
			margin:18,
			nav:true,
			autoplay: true,
			autoPlaySpeed: 1000,
			autoPlayTimeout: 1000,
			autoplayHoverPause: true,
			responsive:{
				0:{
					items:1
				},
				350:{
					items:2
				},
				1000:{
					items:4
				}
			}
		})
		
		
		
		$('.brands-slider').owlCarousel({
			loop:true,
			margin:60,
			nav:true,
			autoplay: true,
			autoPlaySpeed: 1000,
			autoPlayTimeout: 1000,
			autoplayHoverPause: true,
			navText: ["<img src='"+imgpath+"/cr2.png'>","<img src='"+imgpath+"/cr1.png'>"],
			responsive:{
				0:{
					items:2
				},
				400:{
					items:3
				},
				1000:{
					items:4
				}
			}
		})


		$('#spec-slider').owlCarousel({
			loop:false,
			margin:60,
			nav:false,
			autoplay: true,
			autoPlaySpeed: 1000,
			autoPlayTimeout: 1000,
			autoplayHoverPause: true,
			navText: ["<img src='"+imgpath+"/cr2.png'>","<img src='"+imgpath+"/cr1.png'>"],
			responsive:{
				0:{
					items:1
				},
				768:{
					items:2
				},
				1000:{
					items:3
				}
			}
		})

		$('#floorplan-slider').owlCarousel({
			loop:false,
			margin:60,
			nav:false,
			autoplay: true,
			autoPlaySpeed: 1000,
			autoPlayTimeout: 1000,
			autoplayHoverPause: true,
			navText: ["<img src='"+imgpath+"/cr2.png'>","<img src='"+imgpath+"/cr1.png'>"],
			responsive:{
				0:{
					items:1
				},
				479:{
					items:2
				},
				1000:{
					items:3
				}
			}
		})		

		$(".cust-leasing-slider").owlCarousel({
			loop:true,
			margin:60,
			nav:false,
			autoplay: true,
			autoPlaySpeed: 2000,
			autoplayTimeout:2000,
			autoplayHoverPause: false,	
			pagination:false,
			navText: ["<img src='"+imgpath+"/cr2.png'>","<img src='"+imgpath+"/cr1.png'>"],
			responsive:{
				0:{
					items:1
				},
				400:{
					items:1
				},
				1000:{
					items:1
				}
			}
		})		   
});

$(document).ready(function() {
	$(function(){
		$(".social").animate({"right":"30"},1000,'easeOutExpo');
		$( window).scroll(function(){
			//var scrolltop = $(window).scrollTop();
			//var windowheight1 = $(window).height();
			if( $(this).scrollTop() > 200 ){
			$('.floating_menu').stop(true,true).css({top :'0'});
			//$('.social').stop(true,true).css({bottom :'30px'});	
			}
			else{
			$('.floating_menu').stop(true,true).css({top :'-150px'});	
			//$('.social').stop(true,true).css({bottom :'-300px'});	
			
			}
			
		});
	});				   
});


$(document).ready(function() {
	$("#datepicker").datepicker({
		dateFormat: 'dd-mm-yy'
		
	});
});
/*
$(document).ready(function() {
	function PopUp(hideOrshow) {
	    if (hideOrshow == 'hide') document.getElementById('ac-wrapper').style.display = "none";
	    else document.getElementById('ac-wrapper').removeAttribute('style');
	}
	window.onload = function () {
	    setTimeout(function () {
	        PopUp('show');
	    }, 5000);
	}
});*/

$(document).ready(function() {
    if(localStorage.getItem('popState') != 'shown'){
        $("#ac-wrapper").delay(2000).fadeIn();
        localStorage.setItem('popState','shown')
    }

    $('#ac-wrapper input[type="submit"]').click(function(e) // You are clicking the close button
    {
    $('#popup').fadeOut(); // Now the pop up is hiden.
    });
    $('#popup').click(function(e) 
    {
    $('#popup').fadeOut(); 
    });
});


$(document).ready(function() {
	//$(".rc-right .global-foyer h3").text(function(index, currentText) {
		//return currentText.substr(0, 247);
	//});
});

$(document).ready(function() {
	$("#popup input").click(function(){
	    $('#ac-wrapper').addClass("close");
	});
});

$(window).load(function() {
     $('#loading').hide();
  });

$( document ).ready(function() {

	/* Basic Gallery */
	$( '.swipebox' ).swipebox();

});

$(document).ready(function() {
	//$('.banner.home .banner-slider.owl-carousel').height(
	   // $(window).height() - $('.header').height(60)
	//);

	$('.banner.home').css("height", $(window).height());

	//$('.banner').css("height", $(window).height());
});


$(document).ready(function(){
	var storUrl = window.location.href.substring(window.location.href.lastIndexOf('/') + 1);
	if(storUrl == 'contact-us.php'){
		$('html, body').animate({
        scrollTop: $('#contactus').offset().top-80
    }, 1000);
	}
	
});


$(document).ready(function(){
	if($(window).width() <= 767) {
	$(function () {
	  $(".plan-feature-main .tab-system .navbar-toggle").click(function () {
		$(".plan-feature-main .tab-system #product-tabs-area").slideToggle(500);
		$(this).toggleClass("open");
	  });

	  $(".plan-feature-main .tab-system #product-tabs-area li a").click(function () {
		$(".plan-feature-main .tab-system #product-tabs-area").slideToggle(500);
		$(".plan-feature-main .tab-system .navbar-toggle").toggleClass("open");
	  });
	});
	}
});

$(document).ready(function(){
	if($(window).width() <= 991) {
		$(".header a.c-link").attr("href", "javascript:;");	
		$(".header a.c-link1").attr("href", "javascript:;");	
	}
});


$( document ).ready(function() {
//grabs the hash tag from the url
var hash = window.location.hash;
//checks whether or not the hash tag is set
if (hash != "") {
//removes all active classes from tabs
$('.rc-left .nav-tabs2 li').each(function() {
$(this).removeClass('active');
});
$('.rc-right .tab-content div').each(function() {
  $(this).removeClass('in active');
});
//this will add the active class on the hashtagged value
var link = "";
$('.rc-left .nav-tabs2 li').each(function() {
  link = $(this).find('a').attr('href');
  if (link == hash) {
    $(this).addClass('active');
  }
});
$('.rc-right .tab-content div').each(function() {

  link = $(this).attr('id');
  if ('#'+link == hash) {

    $(this).addClass('in active');
    $('#collapseOne').addClass('in');
    $('#collapseTwo').addClass('in');
  }
});
}
});



/*$(document).ready(function(){
	$('.header-c-slider.owl-carousel .owl-nav').removeClass('disabled');
	$('.header-r-slider.owl-carousel .owl-nav').removeClass('disabled');
});*/